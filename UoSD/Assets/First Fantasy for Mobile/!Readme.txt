﻿First Fantasy for Mobile v.1.0

Construct your own colorful fantasy world easily.

Features:

- 12 ground textures.
- 11 grass brushes.
- 100+ 3d models ready to use bushes, trees, mushrooms, bridge, bricks, 
pillars, walls, crystals.
- 19 particles including rain, fog, whirlwind, sand storm, light shaft and 
magic blast!
- 4 demo scenes.
- Mobile friendly with prepared texture atlas.
- New assets are added every update.
- Worth every penny.

------------------------------------------------------------------

Tips for mobile project.

1. There are SD/HD/UD textures for Asset_01. Switch them according to 
target device capability.
2. If you use Unity terrain paint tool these parameters should be set to 
white color.
	- Terrain's grass tint
	- Grass brush's Healthy & Dry 
3. More grass on terrain takes more runtime draw call. For better 
performance paint it less as you can.
4. Beast Lightmapping is powerful tool. Use it after finished your scene to 
reduce draw calls.
5. Beast Lightmap parameters guide for good result and save your time.
	- Intensity: 0.25 - 1
	- Bounce Intensity: 1
	- Shadow Samples: 100
	- Shadow Angle: 20
	- Bake parameter
		- Mode: Single Lightmaps
		- Quality: High
		- Bounces: 4
		- Sky Light Intensity: 0.25
		- Bounce Boost: 4
		- Bounce Intensity: 0.5
		- Final Gather Rays: 1500
		- Contrast Threshold: 0.012
		- Interpolation: 0
		- Interpolation Points: 30
		- Ambient Occlusion: 0
		- Resolution: 32

------------------------------------------------------------------

Thank you for your support. Enjoy your creation.