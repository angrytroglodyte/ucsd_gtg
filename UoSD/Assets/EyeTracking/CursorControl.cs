using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CursorControl : MonoBehaviour
{
	public GUITexture cursor;
	public Color highlightTint;
	Color unhighlightedTint;
	public bool debug;
	public bool useTracker;
	public bool useRays;
	static GUIElement[] guiElements;
	static GUIElement stCursor;
	static public float cursorRadius;
	static Vector2 _cursorPosition;
	static Vector2 _oldCursorPosition;
	static bool cursorMoved;

	static public Vector2 CursorPosition {
		get { return _cursorPosition; }
		set {
			_oldCursorPosition = _cursorPosition;
			_cursorPosition = value;
			cursorMoved = !(_cursorPosition == _oldCursorPosition);
		}
	}

	static public List<GUIElement> hoveredElements;

	static public bool Highlighted{ get; set; }

	static public bool Show{ get { return Config.ShowCrosshairs; } }

	static public CursorControl instance;

	void Start ()
	{
		hoveredElements = new List<GUIElement> ();
		cursorRadius = cursor.pixelInset.width * 0.5f;
		unhighlightedTint = cursor.color; 
		stCursor = cursor;
		instance = this;
		RefreshElements (false);
	}

	void Update ()
	{
		cursorMoved = false;

		Rect cursorPos = cursor.pixelInset;
		cursorPos.center = CursorPosition;
		cursor.pixelInset = cursorPos;
		cursor.enabled = Show;
		cursor.color = Highlighted ? highlightTint : unhighlightedTint;
		
		if (Input.GetButtonDown ("Click"))
			CursorDown ();
		if (Input.GetButtonUp ("Click"))
			CursorUp ();
		Highlighted = Input.GetButton ("Click");
		
		
		if (Config.UseMouse) {
			CursorPosition = Input.mousePosition;
		} else {
			EyeTribeClient trackerScript = (EyeTribeClient)FindObjectOfType (typeof(EyeTribeClient ));
			Vector3 currentPosition = trackerScript.gazePosInvertY;
			
			if (currentPosition.z == 1) {
				CursorPosition = new Vector2 (currentPosition.x, currentPosition.y);			
			} else {
				// off screen somehwere
				CursorPosition = new Vector2 (-100, -100);
			}
		}
		
		if (cursorMoved)
			CursorMoved ();
	}

	void OnGUI ()
	{
		if (debug) {
			if (hoveredElements != null) 
				foreach (var current in hoveredElements)
					GUILayout.Label (current.gameObject.name);
		}
	}

	void ScanForCursorCollisions ()
	{
		var newHoveredElements = FindHoveredElements ();
		foreach (var guiElem in newHoveredElements) {
			if (!hoveredElements.Contains (guiElem)) {
				guiElem.SendMessage ("OnCursorEnter", SendMessageOptions.DontRequireReceiver);
			}
		}

		var unhovered = hoveredElements.Where (x => !newHoveredElements.Contains(x));//find all the elements in the hovered array that no longer have the cursor over them
		hoveredElements = newHoveredElements.ToList ();

		foreach (var cur in unhovered) {
			if (cur == null)
				continue;
			cur.SendMessage ("OnCursorExit", SendMessageOptions.DontRequireReceiver);
		}
	}

	void CursorMoved ()
	{
		ScanForCursorCollisions ();
	}

	void CursorDown ()
	{
		foreach (var curObj in hoveredElements)
			curObj.SendMessage ("OnCursorDown", SendMessageOptions.DontRequireReceiver);
	}

	void CursorUp ()
	{
		foreach (var curObj in hoveredElements)
			curObj.SendMessage ("OnCursorUp", SendMessageOptions.DontRequireReceiver);
	}

	GUIElement[] FindHoveredElements ()
	{
		return guiElements.Where (x => UnityFix.Intersect(cursor.GetScreenRect(), x.GetScreenRect())).ToArray ();
	}

	Collider FindHoveredCollider ()
	{
		RaycastHit hit = new RaycastHit ();
		Physics.Raycast (Camera.main.ScreenPointToRay(CursorPosition), Mathf.Infinity);
		return hit.collider;
	}

	static public void ElementMoved ()
	{
		if (instance != null)
			instance.ScanForCursorCollisions ();
	}

	static public void RefreshElements (bool scanCollisions = true)
	{
		guiElements = FindObjectsOfType (typeof(GUIElement)).Cast<GUIElement> ().Where (x => x != stCursor && x.enabled).ToArray ();
		hoveredElements = new List<GUIElement> ();
		instance.ScanForCursorCollisions ();
	}
}

public static class UnityFix
{
	static public Rect LerpRect (Rect r1, Rect r2, float lt)
	{
		return new Rect (Mathf.Lerp(r1.x, r2.x, lt),
		                   Mathf.Lerp (r1.y, r2.y, lt),
		                   Mathf.Lerp (r1.width, r2.width, lt),
		                   Mathf.Lerp (r1.height, r2.height, lt));
	}

	public static bool Intersect (Rect rA, Rect r2)
	{
		return !(rA.x > r2.x + r2.width || rA.x + rA.width < r2.x || rA.y > r2.y + r2.height || rA.y + rA.height < r2.y);
	}

	public static IEnumerator PerformAndYield (System.Action x)
	{
		x ();
		yield return null;
	}
}