﻿using UnityEngine;
using System.Collections;

public class EyeTrackerMarkers : MonoBehaviour {
	public Texture2D marker;

	void Start () {
		if(FindObjectsOfType(typeof(EyeTrackerMarkers)).Length > 1){
			Destroy (gameObject);
			return;
		}

		DontDestroyOnLoad(this);
	}

	void OnGUI(){
		GUI.Box(new Rect(0, 0, marker.width, marker.height), marker);
		GUI.Box(new Rect(Screen.width - marker.width, 0, marker.width, marker.height), marker);
		GUI.Box(new Rect(Screen.width - marker.width, Screen.height - marker.height, marker.width, marker.height), marker);
		GUI.Box(new Rect(0, Screen.height - marker.height, marker.width, marker.height), marker);
	}
}
