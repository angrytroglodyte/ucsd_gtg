﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

public class TimeTracker : MonoBehaviour
{
		[System.Serializable]
	public class TrackedDay
		{
				public string dateString;
				public float secondsPlayed;

				public DateTime representedDate {
						get {
								return DateTime.Parse (dateString);
						}
				}

				public TrackedDay (DateTime date)
				{
						dateString = date.ToString ();
				}
		}

		public string savePath {
				get {
						return Config.AnalyticsPath + "/" + "timetracking.json";
				}
		}

		public string[] levelsToTrack;
		float loadedTime;
		bool shouldTrack;
		List<TrackedDay> days;

		TrackedDay CurrentDay {
				get {
						var day = days.FirstOrDefault (x => x.dateString == DateTime.Today.ToString());
						if (day == null) {
								day = new TrackedDay (DateTime.Today);
								days.Add (day);
						}
						return day;
				}
		}

		static public TimeTracker instance;

		void Awake ()
		{
				if (instance != null) {
						Destroy (gameObject);
						return;
				}

				instance = this;
		}

		void Start ()
		{
				DontDestroyOnLoad (gameObject);
				if (File.Exists (savePath)) {
						days = JsonConvert.DeserializeObject<List<TrackedDay>> (File.ReadAllText(savePath)) as List<TrackedDay>;
						days.Sort ((x, y) => DateTime.Compare(x.representedDate, y.representedDate));
				} else {
						days = new List<TrackedDay> ();
				}
		}

		void OnLevelWasLoaded (int level)
		{
				if (shouldTrack) {
						AddTime (Time.time - loadedTime);
						SaveData ();
				}

				shouldTrack = levelsToTrack.Contains (Application.loadedLevelName) && PrefsX.GetBool ("AnalyticsEnabled");

				if (shouldTrack) {
						loadedTime = Time.time;
				}
		}

		void AddTime (float time)
		{
				CurrentDay.secondsPlayed += time;
		}

		void SaveData ()
		{
				File.WriteAllText (savePath, JsonConvert.SerializeObject (days));
		}

		public TrackedDay FirstDayPlayed ()
		{
				return days [0];
		}

		public int DollarsEarned ()
		{
				var weekStartDay = days [0].representedDate;
				var weekEndDay = weekStartDay.AddDays (6);
			
				int totalCentsEarned = 0;
				int centsEarnedThisWeek = 0;
				foreach (var curDay in days) {
						if (curDay.representedDate > weekEndDay) {		//the day is part of next week - reset the counter!
								weekStartDay = weekStartDay.AddDays (7);
								weekEndDay = weekStartDay.AddDays (6);
		
								totalCentsEarned += centsEarnedThisWeek;
								centsEarnedThisWeek = 0;
						}
				
						var centsEarnedForDay = CentsEarnedForDay (curDay);
						Debug.Log ("Cents earned for day: " + centsEarnedForDay);
						if (centsEarnedForDay >= 800)
								centsEarnedForDay = 800;		//truncate cents to $8, $4, or $0
						else if (centsEarnedForDay >= 400)
								centsEarnedForDay = 400;
						else
								centsEarnedForDay = 0;

						centsEarnedThisWeek += centsEarnedForDay;

						if (centsEarnedThisWeek > 4000)
								centsEarnedThisWeek = 4000;	//cut players off at $40 for a week of play
				}
				
				totalCentsEarned += centsEarnedThisWeek;
				
				Debug.Log ("Total cents earned: " + totalCentsEarned);
				return totalCentsEarned / 100;
		}

		public int CentsEarnedToday ()
		{
				return CentsEarnedForDay (CurrentDay);
		}

		int CentsEarnedForDay (TrackedDay day)
		{
				//30 minutes a day
				//$8/day
				//$40/wk
				var centsPerMinute = 26.7f; //cents/minute
				var maxMinutesPerDay = 30.0f; //how many minutes can you play

				var minutesPlayed = day.secondsPlayed / 60.0f;
				var centsEarned = centsPerMinute * Mathf.Clamp (minutesPlayed, 0.0f, maxMinutesPerDay);
				return Mathf.RoundToInt (centsEarned);
		}
}
