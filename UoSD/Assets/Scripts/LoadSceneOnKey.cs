using UnityEngine;
using System.Collections;

public class LoadSceneOnKey : MonoBehaviour {
	public string key;
	public string levelName;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(key)) Application.LoadLevel(levelName);
	}
}
