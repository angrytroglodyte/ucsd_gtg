using UnityEngine;
using System.Collections;

public class ScaleGUITextureOnHover : MonoBehaviour {
    public float scaleAmount = 1.0f;
	public float scaleSpeed = 1.0f;

	Rect originalRect;
	Rect scaledRect;
	Rect wantedRect;

	float scaleStartTime = 0.0f;

	void Start(){
		scaledRect = originalRect = wantedRect = guiTexture.pixelInset;
		scaledRect.width *= scaleAmount;
		scaledRect.x = -scaledRect.width / 2;
		scaledRect.height *= scaleAmount;
		scaledRect.y = -scaledRect.height / 2 ;
	}

	// Update is called once per frame
	void OnCursorEnter () {
		wantedRect = scaledRect;
	}

	void OnCursorExit(){
		wantedRect = originalRect;
	}

	void Update(){
		guiTexture.pixelInset = UnityFix.LerpRect(guiTexture.pixelInset, wantedRect, Time.deltaTime * scaleSpeed);
	}
}
