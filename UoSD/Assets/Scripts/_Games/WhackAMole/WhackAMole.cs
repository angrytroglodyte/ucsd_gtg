using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WhackAMole : MonoBehaviour
{
		public Mole[] moles;
		public Transform[] holes;
		public GameObject professorMoleWarning;
		public GameObject statsPanel;
		public UILabel accuracyLabel;
		public UILabel molesWhackedLabel;
		public UILabel averageTimeLabel;
	#region UI
		public UISprite[] missIndicators;
		public UILabel whackCountLabel;
		public GameObject instructionsPanel;
	#endregion

	#region difficultyScaling
		public float professorMoleWarningTime = 1.0f;
		public int easyHoleCount = 14;
		public float easyDelayBetweenTargets = 0.5f;
		public float hardDelayBetweenTargets = 0.0f;
		public float easyTargetDisplayTime = 4.0f;
		public float hardTargetDisplayTime = 1.0f;
		public float easyRiseSpeed = 3.0f;
		public float hardRiseSpeed = 12.0f;
		public Vector2 easyTargetSize;
		public Vector2 hardTargetSize;
		public Vector3 easyTargetScale;
		public Vector3 hardTargetScale;

		public float targetRiseSpeed {
				get {
						return Mathf.Lerp (easyRiseSpeed, hardRiseSpeed, difficulty);
				}
		}

		public float delayBetweenTargets {
				get {
						return Mathf.Lerp (easyDelayBetweenTargets, hardDelayBetweenTargets, difficulty);
				}
		}

		public float maxTargetDisplayTime {
				get {
						return Mathf.Lerp (easyTargetDisplayTime, hardTargetDisplayTime, difficulty);
				}
		}

		public Vector2 targetSize {
				get {
						return Vector2.Lerp (easyTargetSize, hardTargetSize, difficulty);
				}
		}

		public Vector3 targetScale {
				get {
						return Vector3.Lerp (easyTargetScale, hardTargetScale, difficulty);
				}
		}

		float difficulty;
		public float difficultyAdd = 0.02f;
	#endregion

	#region gameplay
		public float minPerfectAccuracy = 0.9f;
		public float minNiceAccuracy = 0.5f;
	#endregion

	#region popuplabels
		public UILabel popupLabel;
		public string perfectText = "Great!";
		public string okText = "OK";
		public string missText = "Miss";
		public Color perfectColor;
		public Color okColor;
		public Color missColor;
	#endregion

	#region stats
		int molesWhacked;
		int missesLeft = 3;
		List<float> times = new List<float> ();
		List<float> accuracys = new List<float> ();
		Brain.SessionData analytics;
	#endregion

	#region state
		Transform lastHole;
		bool gameOver;
	#endregion

		void Start ()
		{
				instructionsPanel.SetActive (true);
				statsPanel.SetActive (false);

				analytics = new Brain.SessionData ();

				Mole.MoleWhackedEvent += MoleWhacked;
				Mole.MoleMissedEvent += MoleMissed;

				professorMoleWarning.SetActive (false);
		}

		void OnDestroy ()
		{
				Mole.MoleWhackedEvent -= MoleWhacked;
				Mole.MoleMissedEvent -= MoleMissed;
		}

		void MoleWhacked (Mole mole, float time, float accuracy)
		{
				StartCoroutine (PopupText(accuracy, mole));
				if (!mole.evil) {
						accuracys.Add (accuracy);
						times.Add (time);
						difficulty += difficultyAdd;
						whackCountLabel.text = molesWhackedLabel.text = "" + ++molesWhacked;
				} else {
						LoseLife ();
				}
				analytics.RecordHit ("MoleHit", new{
			MoleType = mole.GetType ().ToString (),
			TimeToTarget = time,
			Accuracy = accuracy,
			Evil = mole.evil,
			ScreenPositionX = Camera.main.WorldToScreenPoint (mole.transform.position).x,
			ScreenPositionY = Camera.main.WorldToScreenPoint (mole.transform.position).y
		});
		}

		void MoleMissed (Mole mole)
		{
				if (!mole.evil)
						LoseLife ();
				analytics.RecordHit ("MoleMissed", new{
			MoleType = mole.GetType ().ToString (),
			ScreenPositionX = Camera.main.ScreenToWorldPoint (mole.transform.position).x,
			ScreenPositionY = Camera.main.ScreenToWorldPoint (mole.transform.position).y,
			Evil = mole.evil
		});
		}

		void StartGame ()
		{
				instructionsPanel.SetActive (false);
				StartCoroutine (GameLoop());
		}

		void Update ()
		{
				Time.timeScale = 1.0f + difficulty;
		}

		IEnumerator GameLoop ()
		{
				yield return new WaitForSeconds (2.0f);
				analytics.BeginRecording ();

				while (!gameOver) {
						var popupMole = moles.Where (x => x.difficultyReq <= difficulty).OrderBy (x => Random.Range (0, 100)).First ();
						if (!popupMole.evil)
								yield return new WaitForSeconds (delayBetweenTargets);
						else {
								yield return new WaitForSeconds (delayBetweenTargets - professorMoleWarningTime);
								professorMoleWarning.SetActive (true);
								yield return new WaitForSeconds (professorMoleWarningTime);
								professorMoleWarning.SetActive (false);
						}
						Transform hole = lastHole;
						while (hole == lastHole) {
								hole = holes [Random.Range(0, molesWhacked > 10 ? holes.Length : easyHoleCount)];
						}
		
						yield return popupMole.StartCoroutine (popupMole.Show (holes[Random.Range (0, holes.Length)]));
						lastHole = hole;
						yield return null;
				}
		}

		IEnumerator PopupText (float accuracy, Mole mole)
		{
				popupLabel.transform.localPosition = Camera.main.WorldToScreenPoint (mole.transform.position);
				popupLabel.transform.localPosition -= Vector3.up * Screen.height * 0.5f;
				popupLabel.transform.localPosition -= Vector3.right * Screen.width * 0.5f;

				if (mole.evil) {
						popupLabel.text = "Oops!";
						popupLabel.color = Color.red;
				} else {
						if (accuracy > minPerfectAccuracy) {
								popupLabel.color = perfectColor;
								popupLabel.text = perfectText;
						} else if (accuracy > 0) {
								popupLabel.color = okColor;
								popupLabel.text = okText;
						} else {
								popupLabel.color = missColor;
								popupLabel.text = missText;
						}
				}

				popupLabel.gameObject.SetActive (true);
				yield return new WaitForSeconds (1.0f);
				popupLabel.gameObject.SetActive (false);
		}

		void LoseLife ()
		{
				missIndicators [--missesLeft].enabled = false;
				if (missesLeft == 0)
						GameOver ();
		}

		void GameOver ()
		{
				gameOver = true;
				statsPanel.SetActive (true);
				molesWhackedLabel.text = molesWhacked + " moles whacked";
				accuracyLabel.text = "Accuracy: " + Mathf.RoundToInt (accuracys.Sum () / accuracys.Count * 100) + "%";
				averageTimeLabel.text = "Average Time: " + Mathf.RoundToInt (times.Sum () / times.Count * 10) / 10.0f;
				analytics.RecordMeta ("AverageAccuracy", accuracys.Sum () / accuracys.Count);
				analytics.RecordMeta ("AverageTimes", times.Sum () / times.Count);
				analytics.EndRecording ();
		}
}

