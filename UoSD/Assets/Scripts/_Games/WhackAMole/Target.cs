using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
	public event System.Action CursorEntered;

	void OnCursorEnter(){
		if (CursorEntered != null)
			CursorEntered ();
	}
}
