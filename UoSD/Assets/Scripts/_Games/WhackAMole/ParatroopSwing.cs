﻿using UnityEngine;
using System.Collections;

public class ParatroopSwing : MonoBehaviour {
	public Vector3 forward;
	public Vector3 swing;
	public float speed = 4.0f;

	void Update(){
		transform.rotation = Quaternion.Euler (swing * Mathf.Sin (Time.time * speed)) * Quaternion.Euler (forward);
	}
}
