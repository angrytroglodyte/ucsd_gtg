﻿using UnityEngine;
using System;
using System.Collections;

public class PopupMole : Mole {
	public float downHeight = -2.0f;
	public float upHeight = 2.0f;
	public float riseSpeed;
	public float showTime = 1.0f;

	void Start(){
		renderer.enabled = false;
	}

	public override IEnumerator Show(Transform hole){
		renderer.material.SetColor("_Emission", Color.white);
		renderer.material.color = Color.white;
		yield return StartCoroutine(base.Show (hole));
		yield return StartCoroutine(Popup (hole));
	}

	IEnumerator Popup(Transform hole){
		var startPos = hole.position + Vector3.up * downHeight;
		var endPos = hole.position + Vector3.up * upHeight;
		renderer.enabled = true;
		for(float t = 0.0f; t < 1.0f; t += Time.deltaTime * riseSpeed){
			transform.position = Vector3.Lerp (startPos, endPos, t);
			yield return null;
		}
		targetTexture.enabled = true;
		visible = true;
		CursorControl.RefreshElements();
		var upTime = Time.time;
		while(Time.time - upTime < showTime  && !whacked) yield return null;
		targetTexture.enabled = false;
		CursorControl.RefreshElements();
		yield return StartCoroutine(Hide (hole));
	}

	public override IEnumerator Hide(Transform hole){
		var startPos = hole.position + Vector3.up * downHeight;
		var endPos = hole.position + Vector3.up * upHeight;

		yield return StartCoroutine(base.Hide (hole));

		for(float t = 0.0f; t < 1.0f; t += Time.deltaTime * riseSpeed){
			transform.position = Vector3.Lerp(endPos, startPos, t);
			yield return null; 
		}
		visible = false;
	}

	public override void Whacked(float time, float accuracy){
		if(whacked) return;

		renderer.material.SetColor("_Emission", new Color(0.5f, (1.0f - accuracy) * 0.5f, (1.0f - accuracy) * 0.5f));
		renderer.material.color = new Color(0.5f, (1.0f - accuracy) * 0.5f, (1.0f - accuracy) * 0.5f);
		base.Whacked(time, accuracy);
	}
}
