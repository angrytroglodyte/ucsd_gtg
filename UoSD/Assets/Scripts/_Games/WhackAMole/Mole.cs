﻿using UnityEngine;
using System;
using System.Collections;

public class Mole : MonoBehaviour {
	static public event Action<Mole, float, float> MoleWhackedEvent;
	static public event Action<Mole> MoleMissedEvent;
	public float difficultyReq = 0f;
	public float wiggleTime = 0.5f;
	public GUITexture targetTexture;
	public ParticleSystem whackParticles;
	public AudioSource whackSound;
	public AudioSource missedSound;
	public bool evil = false;

	protected float shownTime;
	protected bool whacked;
	protected bool visible;

	void Start(){
		
	}

	public virtual IEnumerator Show(Transform hole){
		whacked = false;
		shownTime = Time.time;
		yield break;
	}

	public virtual IEnumerator Hide(Transform hole){
		if(!whacked){
			if(MoleMissedEvent != null) MoleMissedEvent(this);
			if(missedSound != null)
				missedSound.Play ();
		}
		yield return new WaitForSeconds(wiggleTime);
	}

	public virtual void Whacked(float time, float accuracy){
		if(whacked) return;

		if(MoleWhackedEvent != null) MoleWhackedEvent(this, time, accuracy);
		if(whackParticles != null) whackParticles.Play ();
		if(accuracy > 0 && whackSound != null) whackSound.Play();
		whacked = true;
	}

	void CursorEnteredTarget(GameObject hitTarget){
		if (!visible || whacked) return;
		float timeToTarget = Time.time - shownTime;
		StartCoroutine (CheckAccuracy(acc => {
			Whacked (timeToTarget, acc);
		}));
	}

	IEnumerator CheckAccuracy(Action<float> callback){
		if(evil) {
			yield return new WaitForSeconds (0.5f);
		}
		yield return new WaitForSeconds(0.1f);
		//accuracy calculation
		float distance = (targetTexture.GetScreenRect().center - CursorControl.CursorPosition).magnitude;
		float targetRadius = targetTexture.GetScreenRect().width * 0.5f;
		float minDistance = CursorControl.cursorRadius;
		float maxDistance = minDistance + targetRadius;
		float accuracy = Mathf.Clamp((maxDistance - distance) / maxDistance, 0.0f, 1.0f);
		if (evil) if (accuracy <= 0.0f) yield break; //don't kill the evil moles IF the user looked away 
		callback(accuracy);
	}

}
