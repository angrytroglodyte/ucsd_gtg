﻿using UnityEngine;
using System;
using System.Collections;

public class ParaMole : Mole {
	public Renderer chuteMole;
	public Renderer groundMole;
	public ParatroopSwing swinger;
	public float fallHeight = 5.0f;
	public float groundHeight = 2.0f;
	public float hideHeight = -2.0f;
	public float whackedFallSpeed = 3.0f;
	public float fallSpeed = 2.0f;
	public float hideSpeed = 4.0f;

	void Start(){
		chuteMole.enabled = groundMole.enabled = false;
	}

	public override IEnumerator Show(Transform hole){
		groundMole.enabled = false;
		chuteMole.enabled = true;
		yield return StartCoroutine(base.Show (hole));
		yield return StartCoroutine(Animate(hole));
	}

	public IEnumerator Animate(Transform hole){
		visible = true;
		transform.position = hole.position + Vector3.up * fallHeight;
		swinger.enabled = true;
		while(transform.position.y > hole.position.y + groundHeight &! whacked){
			transform.position -= Vector3.up * fallSpeed * Time.deltaTime;
			yield return null;
		}

		chuteMole.enabled = !(groundMole.enabled = true);
		swinger.enabled = false;

		yield return StartCoroutine(Hide (hole));
	}

	public override IEnumerator Hide(Transform hole){
		yield return StartCoroutine(base.Hide (hole));

		while(transform.position.y > hole.position.y + groundHeight){
			transform.position -= Vector3.up * Time.deltaTime * whackedFallSpeed;
			yield return null;
		}
		yield return new WaitForSeconds(0.1f);
		while(transform.position.y > hole.position.y + hideHeight){
			visible = false;
			transform.position -= Vector3.up * Time.deltaTime * hideSpeed;
			yield return null;
		}
	}

	public override void Whacked(float time, float accuracy){
		base.Whacked (time, accuracy);
	}
}


/*
 * 	public override IEnumerator Show(Transform hole){
		renderer.enabled = true;
		renderer.material.SetColor("_Emission", Color.white);
		renderer.material.color = Color.white;
		yield return StartCoroutine(base.Show (hole));
		yield return StartCoroutine(Popup (hole));
	}

	IEnumerator Popup(Transform hole){
		var startPos = hole.position + Vector3.up * downHeight;
		var endPos = hole.position + Vector3.up * upHeight;
		for(float t = 0.0f; t < 1.0f; t += Time.deltaTime * riseSpeed){
			transform.position = Vector3.Lerp (startPos, endPos, t);
			yield return null;
		}

		visible = true;
		var upTime = Time.time;
		while(Time.time - upTime < showTime  && !whacked) yield return null;

		yield return StartCoroutine(Hide (hole));
	}

	public override IEnumerator Hide(Transform hole){
		var startPos = hole.position + Vector3.up * downHeight;
		var endPos = hole.position + Vector3.up * upHeight;

		yield return StartCoroutine(base.Hide (hole));

		for(float t = 0.0f; t < 1.0f; t += Time.deltaTime * riseSpeed){
			transform.position = Vector3.Lerp(endPos, startPos, t);
			yield return null; 
		}
		visible = false;
	}

	public override void Whacked(float time, float accuracy){
		if(whacked) return;

		renderer.material.SetColor("_Emission", new Color(0.5f, (1.0f - accuracy) * 0.5f, (1.0f - accuracy) * 0.5f));
		renderer.material.color = new Color(0.5f, (1.0f - accuracy) * 0.5f, (1.0f - accuracy) * 0.5f);
		base.Whacked(time, accuracy);
	}
(*/