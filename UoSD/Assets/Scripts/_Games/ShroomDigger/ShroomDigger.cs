﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ShroomDigger : MonoBehaviour {
	public GameObject[] biggerItems; //these need to go in back
	public GameObject[] smallerItems;
	public GameObject[] specialShrooms; //we can pick from this pool to do special shrooms


	public AudioSource explosionSound;
	public ParticleSystem explosion;

	public float spawnRadius;
	public float scaleStartDelay = 0.5f;

	#region DifficultyScaling
	public float easiestSpecialScale = 1.0f;
	public float hardestSpecialScale = 0.5f;
	public int easiestSpecialCount = 4;
	public int hardestSpecialCount = 8;
	public float easiestAngleSpacing = 900;
	public float hardestAngleSpacing = 600;
	public float easiestSpacing = 4;
	public float hardestSpacing = 1;

	public float moneyLossRate = 1;
	public float moneyAddPerShroom = 10;

	float specialScale{
		get{
			return Mathf.Lerp (easiestSpecialScale, hardestSpecialScale, difficulty);
		}
	}

	int specialCount{
		get{
			return Mathf.RoundToInt (Mathf.Lerp (easiestSpecialCount, hardestSpecialCount, difficulty));
		}
	}

	public float difficulty = 0.0f;
	float difficultyAdd = 0.1f;
	#endregion
	
	#region highlighting
	public float highlightedScale = 2.0f;
	public float scaleSpeed = 0.1f;
	#endregion

	#region state
	GameObject hoveredShroom;
	int shroomsLeft;
	int wave;
	float hoverStartTime;
	float levelStartTime;
	public float money;
	bool inGame;
	int totalShroomsFound;

	Brain.SessionData analyticData;
	#endregion

	#region UI
	public GameObject levelCompleteWindow;
	public GameObject instructionsWindow;
	public GameObject gameOverWindow;
	public UILabel shroomsFoundLabel;
	public UILabel totalShroomsFoundLabel;
	public UILabel moneyLeftLabel;
	public UILabel moneyGainedLabel;
	#endregion

	public GameObject shroomParent;
	public GameObject foliageParent;
	public CursorWorldTarget shroomTarget;


	List<GameObject> shrooms = new List<GameObject>();
	List<GameObject> specials = new List<GameObject> ();
	List<GameObject> shroomTargets = new List<GameObject>();

	// Use this for initialization
	void Start () {
		analyticData = new Brain.SessionData();
		instructionsWindow.SetActive(true);
		levelCompleteWindow.SetActive(false);
		gameOverWindow.SetActive(false);
	}

	void StartGame(){
		instructionsWindow.SetActive(false);
		analyticData.BeginRecording();
		Generate ();
	}

	void Generate(){
		inGame = true;
		levelStartTime = Time.time;
		wave++;
		foreach(var curShroom in shrooms) Destroy (curShroom);
		foreach(var curTarget in shroomTargets) Destroy(curTarget);
		shrooms.Clear ();
		shroomTargets.Clear ();
		specials.Clear ();

		float spacing = Mathf.Lerp (easiestSpacing, hardestSpacing, difficulty);
		float angleSpacing = Mathf.Lerp (easiestAngleSpacing, hardestAngleSpacing, difficulty);

		//start a new ring
		for(float currentRadius = spawnRadius; currentRadius > 0f; currentRadius -= spacing){
			var recentIndices = new List<int> ();
			//var currentAngleSpacing = angleSpacing / currentRadius;
			//var spokeCount = 360f / currentAngleSpacing;
			for(float currentAngle = 0; currentAngle < 360; currentAngle += angleSpacing / currentRadius){
				int randIndex = Random.Range (0, smallerItems.Length);
				do randIndex = Random.Range(0, smallerItems.Length);
				while (recentIndices.Contains (randIndex));

				var shroom = Instantiate (smallerItems[randIndex], Vector3.zero, Quaternion.identity) as GameObject;
				shroom.transform.parent = shroomParent.transform;
				shroom.transform.localPosition = Quaternion.Euler(Vector3.up * currentAngle) * Vector3.right * currentRadius;

				shrooms.Add (shroom);
			}
		}

		GameObject specialShroom = specialShrooms[Random.Range (0, specialShrooms.Length)];
		List<int> replaceIndices = new List<int>();
		for(int i = 0; i < specialCount; i++){
			int replace;
			do replace = Random.Range(shrooms.Count / 2, shrooms.Count);
			while(replaceIndices.Contains(replace));

			replaceIndices.Add (replace);

			var newShroom = Instantiate (specialShroom, shrooms [replace].transform.position + Vector3.up * specialScale, Quaternion.identity) as GameObject;
			newShroom.transform.localScale = Vector3.one * specialScale;
			Destroy (shrooms[replace]);
			shrooms [replace] = newShroom;
			specials.Add (newShroom);

			var newTarget = Instantiate(shroomTarget, Vector3.zero, Quaternion.identity) as CursorWorldTarget;
			newTarget.trackObject = newShroom;
			newTarget.notifyObject = gameObject;
			shroomTargets.Add (newTarget.gameObject);
		}

		shroomsFoundLabel.text = specials.Count.ToString();
		CursorControl.RefreshElements();
	}

	void CursorEnteredTarget(GameObject theTarget){
		if(hoveredShroom == null){
			hoverStartTime = Time.time;
			hoveredShroom = theTarget;
			var point = Camera.main.WorldToScreenPoint(theTarget.transform.position);
			analyticData.RecordHit ("EyeHitShroom", new{PositionX = point.x, PositionY = point.y});
		}
	}

	void CursorExitedTarget(GameObject theTarget){
		if(theTarget == hoveredShroom) {
			analyticData.RecordHit ("EyeLeftShroom");
			hoveredShroom = null;
		}
	}

	// Update is called once per frame
	void Update () {
		if(inGame){
			money -= moneyLossRate * Time.deltaTime;
			if(money <= 0){
				GameOver();
			}
		}

		moneyLeftLabel.text = "" + Mathf.RoundToInt(money);

		foreach (var curShroom in specials)
			if(curShroom != hoveredShroom)
				curShroom.transform.localScale = Vector3.Lerp (curShroom.transform.localScale, Vector3.one * specialScale, Time.deltaTime);

		if(hoveredShroom != null) {
			hoveredShroom.transform.localScale = Vector3.Lerp(Vector3.one * specialScale, Vector3.one * highlightedScale, (Time.time - hoverStartTime - scaleStartDelay) * scaleSpeed);
			if(Vector3.Distance (hoveredShroom.transform.localScale, Vector3.one * highlightedScale) < 0.1f){
				PullShroom();
			}
		}
	}

	void PullShroom(){
		var shroomPos = Camera.main.WorldToScreenPoint(hoveredShroom.transform.position);
		analyticData.RecordHit ("ShroomExploded", new{PositionX = shroomPos.x, PositionY = shroomPos.y});
		explosion.transform.position = hoveredShroom.transform.position + Vector3.up * 28;
		explosion.Play ();
		explosionSound.Play ();
		specials.Remove (hoveredShroom);
		var pulledTarget = shroomTargets.First (x => x.GetComponent<CursorWorldTarget>().trackObject == hoveredShroom);
		shroomTargets.Remove (pulledTarget);
		Destroy(pulledTarget.gameObject);
		Destroy (hoveredShroom);
		hoveredShroom = null;
		CursorControl.RefreshElements();

		shroomsFoundLabel.text = specials.Count.ToString();
		totalShroomsFound++;

		if(specials.Count == 0) {
			LevelComplete();
		} else {
			if(CursorControl.hoveredElements.Count > 0){
				var shTg = CursorControl.hoveredElements.FirstOrDefault(x => shroomTargets.Contains(x.gameObject));
				if(shTg != null) CursorEnteredTarget(shTg.gameObject);
			}
		}
	}

	void LevelComplete(){
		analyticData.RecordHit("LevelComplete", wave);
		levelCompleteWindow.SetActive(true);
		moneyGainedLabel.text = "You gained " + Mathf.RoundToInt (moneyAddPerShroom * specialCount) + " coins";
		money += moneyAddPerShroom * specialCount;
		inGame = false;
	}

	void ContinueToNextLevel(){
		difficulty += difficultyAdd;
		Generate ();
		levelCompleteWindow.SetActive(false);
	}

	void GameOver(){
		analyticData.RecordMeta ("TotalShrooms", totalShroomsFound);
		analyticData.EndRecording();

		inGame = false;
		gameOverWindow.SetActive(true);
		totalShroomsFoundLabel.text = "" + totalShroomsFound;
	}
}
