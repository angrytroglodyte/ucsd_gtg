﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ShipSelectButton : MonoBehaviour {
	public int unlockLevel = 1; //how many seconds does the player have to play to unlock this ship
	public string unlockKey = "SpaceRaceLevel";
	public UITexture buttonTex;
	public Texture2D shipGraphic;

	static List<ShipSelectButton> allButtons = new List<ShipSelectButton>();
	static public Texture2D CurrentShipGraphic{
		get{
			return allButtons[PlayerPrefs.GetInt("SpaceRaceSelectedShip", 1)].shipGraphic;
		}
	}

	void Start(){
		if(!allButtons.Contains (this)){
			allButtons.Add (this);
			allButtons = allButtons.OrderBy(x => unlockLevel).ToList();
			buttonTex.color = Unlocked ? Color.white : new Color(0.0f,0.0f,0.0f,1.0f);
			GetComponent<UIButtonScale>().enabled = Unlocked;
		}
	}

	bool Unlocked{
		get{
			return PlayerPrefs.GetInt(unlockKey, 0) > unlockLevel;
		}
	}

	public void OnClick(){
		PlayerPrefs.SetInt("SpaceRaceSelectedShip", allButtons.IndexOf(this));
	}
}
