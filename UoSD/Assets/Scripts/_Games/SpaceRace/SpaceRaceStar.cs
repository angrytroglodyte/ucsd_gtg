﻿using UnityEngine;
using System.Collections;

public class SpaceRaceStar : MonoBehaviour {
	public ParticleSystem starSplosion;
	public AudioSource starAudio;

	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Ship")){
			starSplosion.Play ();
			starAudio.Play ();
			renderer.enabled = false;
		}
	}
}
