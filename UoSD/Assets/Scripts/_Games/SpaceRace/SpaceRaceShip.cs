﻿using UnityEngine;
using System.Collections;

public class SpaceRaceShip : MonoBehaviour {
	public GameObject colNotify;

	void OnTriggerEnter(Collider col){
		colNotify.SendMessage ("ShipHitItem", col.gameObject);
	}

	void GameStarted(){
		renderer.material.mainTexture = ShipSelectButton.CurrentShipGraphic;
	}
}
