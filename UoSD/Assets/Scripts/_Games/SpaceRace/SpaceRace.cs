﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceRace : MonoBehaviour {
	#region shipparams
	public float screenRange = 6;
	public Transform ship;
	public ParticleSystem shipSplosion;
	public AudioSource shipSplosionSound;
	public float shipYRange = 5;
	public float easyShipSpeed = 1.0f;
	public float hardShipSpeed = 8.0f;
	public float scrollBackSpeedFactor = 0.5f;
	public float shipSpeed {
		get{
			return Mathf.Lerp(easyShipSpeed, hardShipSpeed, difficulty);
		}
	}
	public float easyShipSideSpeed = 2.0f;
	public float hardShipSideSpeed = 8.0f;
	public float shipSideSpeed {
		get{
			return Mathf.Lerp (easyShipSideSpeed, hardShipSideSpeed, difficulty);
		}
	}
	#endregion

	#region gates
	public float minGateX = 0f;
	public float maxGateX = 12f;
	public float gateY;

	int quadsPerGate{
		get{
			return difficulty > 0.66f ? 1 : difficulty > 0.33f ? 2 : 4;
		}
	}
	
	public Renderer[] gates1;
	public Renderer[] gates2;
	Renderer[] gates;

	public Material greenGateMaterial;
	public Material redGateMaterial;
	public AudioSource greenGateSound;
	public AudioSource redGateSound;
	#endregion

	#region UI
	public GameObject instructionsPanel;
	public GameObject hudPanel;
	public GameObject gameOverPanel;
	public GameObject selectShipPanel;
	public UILabel gameOverTimeLabel;

	public UILabel timerLabel;
	public UILabel startPromptLabel;

	public UISprite[] redGateIndicators;
	#endregion

	public Transform environment;
	public GameObject[] bgPlanes;
	public GameObject[] stars;
	public float bgSpacing = 12.8f;
	float bgPosition = 0.0f;
	int curBG = 0;
	public float starPoints = 2000;
	public float timeScoreFactor = 100.0f;

	public float scrollDuration = 1.0f;

	public int scorePerLevel = 10000;

	public int GameLevel{
		get{ return PlayerPrefs.GetInt("SpaceRaceLevel", 1); }
		set{ PlayerPrefs.SetInt ("SpaceRaceLevel", value); }
	}

	public int NeededScoreForNextLevel{
		get{
			return NeededScoreForLevel(GameLevel + 1);
		}
	}

	public int NeededScoreForLevel(int level){
		if(level > 1) return NeededScoreForLevel(level - 1) + (level - 1) * scorePerLevel;
		return 0;
	}

	public float TotalScore{
		get{ return PlayerPrefs.GetFloat("SpaceRaceScore"); }
		set{ PlayerPrefs.SetFloat ("SpaceRaceScore", value); }
	}

	#region gamestate
	bool gameStarted;
	bool gameOver;
	float targetX;
	float startTime;
	float gateShownTime;
	float score;
	int redGatesHit;
	bool hit = false;

	public float difficulty = 0.0f;
	public float difficultyAdd = 0.1f;
	public float difficultySubtract = 0.2f;

	#endregion

	class SpaceRaceAnalytics{
		public List<bool> hitGates;
		public List<float> times;
	}

	Brain.SessionData analyticData;

	void Start(){
		analyticData = new Brain.SessionData();
		gameOverPanel.SetActive (false);
		hudPanel.SetActive(false);
		instructionsPanel.SetActive (true);
		selectShipPanel.SetActive(false);
	}

	void SelectShip(){
		selectShipPanel.SetActive(true);
		instructionsPanel.SetActive(false);
	}

	void StartGame(){
		selectShipPanel.SetActive(false);
		foreach(GameObject obj in FindObjectsOfType(typeof(GameObject))) obj.SendMessage("GameStarted", SendMessageOptions.DontRequireReceiver);
		StartCoroutine (StartPrompt ());
	}

	IEnumerator StartPrompt(){
		hudPanel.SetActive (true);
		for(float t = 3.0f; t > 0.0f; t -= Time.deltaTime){
			startPromptLabel.text = Mathf.Ceil (t).ToString ();
			yield return null;
		}
		startPromptLabel.text = "GO!";
		yield return new WaitForSeconds(1);
		analyticData.BeginRecording();
		startPromptLabel.enabled = false;
		StartCoroutine(GameLoop());
	}

	void Update(){
		if(gameStarted &! gameOver) score += Time.deltaTime * timeScoreFactor;
		timerLabel.text = Mathf.Round(score).ToString();
	}

	IEnumerator GameLoop(){
		gameStarted = true;
		ship.position = new Vector3(ship.position.x, -shipYRange, ship.position.z);
		targetX = ship.position.x;
		startTime = Time.time;

		while(!gameOver){
			yield return StartCoroutine (MoveShipUp());
			SetupNextScreen();
			yield return StartCoroutine (ScrollScreenUp());
		}
		GameOver();
	}

	IEnumerator MoveShipUp(){
		while(ship.transform.position.y < shipYRange){
			ship.position += Vector3.up * shipSpeed * Time.deltaTime;
			var diff = targetX - ship.position.x;
			if(Mathf.Abs (diff) < 0.1) diff = 0;
			ship.position += Vector3.right * Mathf.Clamp (diff * 1000, -1.0f, 1.0f) * shipSideSpeed * Time.deltaTime;
			yield return null;
		}
		ship.position = new Vector3 (ship.position.x, shipYRange, ship.position.z);
	}

	void SetupNextScreen(){
		gates = (gates == gates1 ? gates2 : gates1);
		for(int i = 0; i < gates.Length; i++) {
			gates[i].transform.position = new Vector3(Mathf.Lerp (minGateX, maxGateX, (float)i / (float)gates.Length), screenRange * 2, gates[i].transform.position.z);
			gates[i].renderer.material = redGateMaterial;
			gates[i].GetComponent<BoxCollider>().size = new Vector3(1, 0.5f, 1);
			gates [i].tag = "RedGate";
		}

		for(int i = 0; i < stars.Length; i++){
			stars[i].renderer.enabled = false;
			if(Random.Range (0, stars.Length) == i){
				var randomPos = Random.onUnitSphere * shipYRange;
				randomPos.z = 10;
				randomPos.x = Mathf.Clamp (randomPos.x, minGateX, maxGateX);
				stars[i].renderer.enabled = true;
				stars[i].transform.position = Vector3.up * screenRange * 2 + randomPos;
				analyticData.RecordHit("StarsShown", new{
					PositionX = stars[i].transform.position.x, 
					PositionY = stars[i].transform.position.y
				});
			}
		}

		var max = gates.Length - quadsPerGate;
		var start = Random.Range (0, max);
		for(var i = start; i < start + quadsPerGate; i++){
			var greenGate = gates[i];
			greenGate.renderer.material = greenGateMaterial;
			gates[i].GetComponent<BoxCollider>().size = Vector3.one;
			greenGate.gameObject.tag = "GreenGate";
		}
		hit = false;
	}

	IEnumerator ScrollScreenUp(){
		var oldGates = gates == gates1 ? gates2 : gates1;
		var gateDistFromShip = shipYRange + gateY;
		var oldGateDistFromShip = oldGates[0].transform.position.y - ship.position.y;
		while(ship.transform.position.y > -shipYRange){
			ship.transform.position -= Vector3.up * Time.deltaTime * scrollBackSpeedFactor * shipSpeed;
			foreach(var curGate in gates) {
				Vector3 pos = curGate.transform.position;
				pos.y = ship.transform.position.y + gateDistFromShip;
				curGate.transform.position = pos;
			}
			oldGateDistFromShip -= shipSpeed * Time.deltaTime;
			var oldGatePos = oldGates[0].transform.position.y;
			foreach(var curGate in oldGates){
				var pos = curGate.transform.position;
				pos.y = ship.transform.position.y + oldGateDistFromShip;
				curGate.transform.position = pos;
			}
			var delta = oldGates[0].transform.position.y - oldGatePos;
			ScrollBackground(-delta);
			yield return null;
		}
		gateShownTime = Time.time;
	}

	void ScrollBackground(float distance){
		var minY = -screenRange * 2;

		foreach(var curPlane in bgPlanes){
			curPlane.transform.position -= Vector3.up * distance;
			if(curPlane.transform.position.y < minY) curPlane.transform.position += Vector3.up * screenRange * 2 * bgPlanes.Length;
		}

		foreach(var curStar in stars){
			curStar.transform.position -= Vector3.up * distance;
		}
	}

	void CursorEnteredTarget(GameObject target){
		if(analyticData == null || !analyticData.recording) return;

		targetX = target.transform.position.x;

		var screenPoint = Camera.main.WorldToScreenPoint(target.transform.position);
		analyticData.RecordHit("EyeHitTarget", new {Tag = target.tag, RelativeTime = Time.time - gateShownTime, PositionX = screenPoint.x, PositionY = screenPoint.y});
	}

	void CursorExitedTarget(GameObject target){
		if(analyticData == null || !analyticData.recording) return;

		var screenPoint = Camera.main.WorldToScreenPoint(target.transform.position);
		analyticData.RecordHit("EyeLeftTarget", new {Tag = target.tag, RelativeTime = Time.time - gateShownTime, PositionX = screenPoint.x, PositionY = screenPoint.y});
	}

	void ShipHitItem(GameObject item){
		if(item.CompareTag("GreenGate")){
			HitGreenGate (item);
		}
		if(item.CompareTag("RedGate")){
			HitRedGate (item);
		}
		if(item.CompareTag("Star")){
			HitStar (item);
		}
	}

	void HitRedGate(GameObject item){
		if(hit) return;
		var screenPoint = Camera.main.WorldToScreenPoint(item.transform.position);
		analyticData.RecordHit("RedGateHit", new {RelativeTime = Time.time - gateShownTime, PositionX = screenPoint.x, PositionY = screenPoint.y});

		redGateSound.Play ();
		redGateIndicators[2 - redGatesHit].spriteName = "live_red";
		difficulty = Mathf.Max (difficulty - difficultySubtract, 0);
		if(++redGatesHit >= 3){
			gameOver= true;
			shipSplosion.Play ();
			shipSplosionSound.Play ();
			ship.renderer.enabled = false;
		}
		hit = true;
	}

	void HitGreenGate(GameObject item){
		if(hit) return;
		var screenPoint = Camera.main.WorldToScreenPoint(item.transform.position);
		analyticData.RecordHit("GreenGateHit", new {RelativeTime = Time.time - gateShownTime, PositionX = screenPoint.x, PositionY = screenPoint.y});

		difficulty = Mathf.Min(difficulty + difficultyAdd, 1);
		greenGateSound.Play ();
		hit = true;
	}

	void HitStar(GameObject item){
		var screenPoint = Camera.main.WorldToScreenPoint(item.transform.position);
		analyticData.RecordHit("StarHit", new {RelativeTime = Time.time - gateShownTime, PositionX = screenPoint.x, PositionY = screenPoint.y});
		score += starPoints;
	}

	void GameOver(){
		analyticData.RecordMeta("GameTimeSeconds", Time.time - startTime);
		analyticData.RecordMeta("GameScore", score);
		analyticData.RecordMeta("DifficultyReached", difficulty);

		analyticData.EndRecording();

		var oldScore = TotalScore;
		TotalScore += score;

		while(TotalScore > NeededScoreForNextLevel){
			Debug.Log ("Level up! " + NeededScoreForNextLevel);
			GameLevel++;
		}

		PlayerPrefs.Save ();

		hudPanel.SetActive(false);
		gameOverPanel.SetActive(true);
		gameOverTimeLabel.text = "You scored " + score + " points.";
	}
}
