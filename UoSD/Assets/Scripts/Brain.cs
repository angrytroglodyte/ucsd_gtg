﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public class Brain : MonoBehaviour
{
	public class SessionData
	{
		static AudioClip recordTone;

		static AudioClip RecordTone {
			get {
				if (recordTone == null)
					recordTone = Resources.Load ("SessionTone") as AudioClip;
				return recordTone;
			}
		}

		public Dictionary<string, object> metadata;
		public Dictionary<string, List<object>> hits;
		public bool recording;
		float recordStartTime;

		public SessionData ()
		{
			metadata = new Dictionary<string, object> ();
			hits = new Dictionary<string, List<object>> ();
		}

		public void BeginRecording ()
		{
			recording = true;
			recordStartTime = Time.time;
			metadata.Add ("date", System.DateTime.Now.ToString ());
			if (RecordTone != null)
				AudioSource.PlayClipAtPoint (RecordTone, Camera.main.transform.position);
		}

		public void EndRecording ()
		{
			recording = false;
			Brain.WriteSession (this);
		}

		public void RecordMeta (string key, object value)
		{
			if (!recording) {
				Debug.LogWarning ("Tried to record metadata for non-recording Session.");
				return;
			}

			metadata [key] = new {AbsoluteTime = Time.time - recordStartTime, Value = value};
		}

		public void RecordHit (string key, object value = null)
		{
			if (!recording) {
				Debug.LogWarning ("Tried to record hit for non-recording Session.");
				return;
			}

			if (!hits.ContainsKey (key))
				hits [key] = new List<object> ();
			hits [key].Add (new {AbsoluteTime = Time.time - recordStartTime, Value = value});
		}
	}

	static string UniqueSessionPath ()
	{
		string path = Config.AnalyticsPath + "/" + Config.Username + "/" + Application.loadedLevelName + "/";
		int i = 0;
		for (; File.Exists(path + "session" + i + ".txt"); i++)
			;

		return path + "session" + i + ".txt";
	}

	static public void WriteSession (SessionData data)
	{
		if (!PrefsX.GetBool ("AnalyticsEnabled"))
			return;

		var writePath = UniqueSessionPath ();
		
		Directory.CreateDirectory (Path.GetDirectoryName(writePath));
		File.WriteAllText (writePath, JsonConvert.SerializeObject (data));
	}
}

static public class Config
{
	static string configPath { get { return Application.dataPath + "/StreamingAssets/config.txt"; } }

	static Dictionary<string, object> configData;

	static Dictionary<string, object> ConfigData {
		get {
			if (!File.Exists (configPath))
				Debug.LogError ("Missing config file at path: " + configPath);

			if (configData == null) {
				configData = JsonConvert.DeserializeObject <Dictionary<string, object>> (File.ReadAllText(configPath));
			}
					
			return configData;
		}
	}

	static public bool UseMouse {
		get {
			return (bool)ConfigData ["UseMouse"];
		}
	}

	static public string AnalyticsPath {
		get {
			return (string)ConfigData ["AnalyticsPath"];
		}
	}

	static public string Username {
		get {
			return (string)ConfigData ["Username"];
		}
	}

	static public bool ShowCrosshairs {
		get {
			return (bool)ConfigData ["ShowCrosshairs"];
		}
	}
}

static public class PrefsX
{
	static public void SetBool (string key, bool val)
	{
		PlayerPrefs.SetInt (key, val ? 1 : 0);
	}

	static public bool GetBool (string key, bool defaultVal = false)
	{
		return PlayerPrefs.GetInt (key, defaultVal ? 1 : 0) == 1;
	}
}