﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{
	public float speed = 10.0f;
	public float magnitude = 10.0f;
	Vector3 startPosition;

	void Start ()
	{
		startPosition = transform.localPosition;
	}
	// Update is called once per frame
	void Update ()
	{
		transform.localPosition = startPosition + Mathf.PerlinNoise (Time.time * speed, 0.1f) * Vector3.one * magnitude;
	}
}
