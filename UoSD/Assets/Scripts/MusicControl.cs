﻿using UnityEngine;
using System.Collections;

public class MusicControl : MonoBehaviour {
	public bool MusicOn{
		get{
			return PrefsX.GetBool("MusicOn", true);
		}
		set{
			PrefsX.SetBool("MusicOn", value);
		}
	}

	public UISprite buttonSprite;
	public string onSpriteName, offSpriteName;

	// Use this for initialization
	void OnClick(){ 
		MusicOn = !MusicOn;
		audio.volume = MusicOn ? 1 : 0;
		if(buttonSprite != null) buttonSprite.spriteName = MusicOn ? onSpriteName : offSpriteName;
	}

	void OnEnable(){
		audio.volume = MusicOn ? 1 : 0;
		if(buttonSprite != null) buttonSprite.spriteName = MusicOn ? onSpriteName : offSpriteName;
	}
}
