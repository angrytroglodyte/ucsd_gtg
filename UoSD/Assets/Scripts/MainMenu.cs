﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	#region UI
	public GameObject usernameWindow;
	public GameObject profileSelectWindow;
	public UILabel usernameButtonLabel;
	public UILabel usernameEntry;
	public UILabel moneyLabel;
	#endregion

	static bool profileSelected;

	IEnumerator Start ()
	{
		usernameWindow.SetActive (false);
		profileSelectWindow.SetActive (false);
		if (!profileSelected) {
			profileSelectWindow.SetActive (true);
			usernameButtonLabel.text = Config.Username;
		}

		yield return null;
		moneyLabel.text = "$" + TimeTracker.instance.DollarsEarned ();
	}

	void ChangeProfile ()
	{
		profileSelectWindow.SetActive (true);
	}

	void LoginUser ()
	{
		PrefsX.SetBool ("AnalyticsEnabled", true);
		profileSelectWindow.SetActive (false);
	}

	void LoginGuest ()
	{
		PrefsX.SetBool ("AnalyticsEnabled", false);
		profileSelectWindow.SetActive (false);
	}
}
