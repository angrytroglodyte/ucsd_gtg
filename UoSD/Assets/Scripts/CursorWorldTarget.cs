﻿using UnityEngine;
using System.Collections;

public class CursorWorldTarget : MonoBehaviour {
	public GameObject trackObject;
	public GameObject notifyObject;
	public Vector2 trackOffset = Vector2.zero;

	void OnCursorEnter(){
		if (!guiTexture.enabled)
			return;
		if(trackObject == null) return;
		trackObject.SendMessage ("OnCursorEnter", SendMessageOptions.DontRequireReceiver);
		if(notifyObject)
			notifyObject.SendMessage ("CursorEnteredTarget", trackObject, SendMessageOptions.DontRequireReceiver);
	}

	void OnCursorExit(){
		if (!guiTexture.enabled)
			return;
		if(trackObject == null) return;


		trackObject.SendMessage ("OnCursorExit", SendMessageOptions.DontRequireReceiver);
		if(notifyObject)
			notifyObject.SendMessage ("CursorExitedTarget", trackObject, SendMessageOptions.DontRequireReceiver);
	}

	void OnClick(){
		if (!guiTexture.enabled)
			return;
		if(trackObject == null) return;


		trackObject.SendMessage ("OnClick", SendMessageOptions.DontRequireReceiver);
		if(notifyObject)
			notifyObject.SendMessage ("CursorClickedTarget", trackObject, SendMessageOptions.DontRequireReceiver);
	}

	void Update(){
		if(trackObject == null) return;

		var trackPoint = Camera.main.WorldToScreenPoint (trackObject.transform.position);
		guiTexture.pixelInset = new Rect (trackPoint.x - guiTexture.pixelInset.width / 2 + trackOffset.x, trackPoint.y - guiTexture.pixelInset.height / 2 + trackOffset.y, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
	}
}
