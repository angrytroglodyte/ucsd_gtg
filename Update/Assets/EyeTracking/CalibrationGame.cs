﻿
using System;
using UnityEngine;
using System.Collections;

using TETCSharpClient;
using TETCSharpClient.Data;

public class CalibrationGame : MonoBehaviour, IGazeListener, ICalibrationResultListener, ICalibrationProcessHandler, ITrackerStateListener, IConnectionStateListener
{
		public bool Complete { get; private set; }

		public bool Success { get; private set; }

		public double Error { get; private set; }

		[SerializeField] private Transform circle = null;
		[SerializeField] private Transform[] points = null;
		[SerializeField] private float sampleTime = 1.0f;

		bool calibrationStarted;
		bool calibrationProgressed;
    

		public void Setup (Action onComplete)
		{
				NMN.Job.Make (Initialize ()).OnComplete += ( cancelled) => {
						if (onComplete != null) {
								onComplete ();
						}
				};
		}

		public void Run (Action onComplete)
		{
				Complete = false;
				NMN.Job.Make (CalibrationRoutine ()).OnComplete += ( cancelled) => {
						if (onComplete != null) {
								onComplete ();
						}
				};
		}

		private IEnumerator Initialize ()
		{
				yield return null;

				while (!EyeTribeClient.IsInitialised) {
						yield return null;
				}
				Debug.Log ("ETC initialized.");

				GazeManager.Instance.AddGazeListener (this);

				GazeManager.Instance.AddCalibrationResultListener (this);
				GazeManager.Instance.AddTrackerStateListener (this);
				GazeManager.Instance.AddConnectionStateListener (this);
				yield break;
		}

		private IEnumerator CalibrationRoutine ()
		{
				yield return new WaitForSeconds (1f);

				GazeManager.Instance.CalibrationStart ((short)points.Length, this);

				while (!calibrationStarted)
						yield return null;

				for (int i = 0; i < points.Length; i++) {
						//	Shuffle points
						var curBananaIndex = UnityEngine.Random.Range (i, points.Length);
						var banana = points [curBananaIndex];
						points [curBananaIndex] = points [i];
						points [i] = banana;

						circle.localPosition = banana.localPosition;
						circle.gameObject.SetActive (true);
						yield return null;

						yield return new WaitForSeconds (0.25f);
						//	Debug.Log("Sending point to tracker: " + (int)banana.localPosition.x + ", " + ((int)Screen.height - (int)banana.localPosition.y));
						calibrationProgressed = false;
						Debug.Log ("Start!");
						GazeManager.Instance.CalibrationPointStart ((int)banana.localPosition.x, (int)Screen.height - (int)banana.localPosition.y);
						yield return new WaitForSeconds (sampleTime);
						Debug.Log ("End!");
						GazeManager.Instance.CalibrationPointEnd ();
						circle.gameObject.SetActive (false);
						while (!calibrationProgressed)
								yield return null;
						yield return new WaitForSeconds (0.3f);
				}

				//	Testing
				if (!GazeManager.Instance.IsActivated) {
						Complete = true;
						Success = false;
				}

				while (!Complete) {
						yield return null;
				}

				yield break;
		}


		#region IGazeListener implementation

		public void OnGazeUpdate (GazeData gazeData)
		{
				//var x = gazeData.SmoothedCoordinates.X;
				//var y = gazeData.SmoothedCoordinates.Y;
				//Debug.Log("Position: " + x + ", " + y);
		}

		#endregion

		#region ICalibrationProcessHandler implementation

		public void OnCalibrationStarted ()
		{
				Invoker.InvokeOnMainThread(() => Debug.Log("Calibration started."));
				calibrationStarted = true;
		}

		public void OnCalibrationProgress (double progress)
		{
            Invoker.InvokeOnMainThread(() => Debug.Log("Calibration progress: " + progress.ToString ("n4")));
				calibrationProgressed = true;
		}

		public void OnCalibrationProcessing ()
		{

        Invoker.InvokeOnMainThread(() => Debug.Log("Calibration Processing..."));
		}

		public void OnCalibrationResult (CalibrationResult calibResult)
		{
        Invoker.InvokeOnMainThread(() => Debug.Log("Calibration result"));
      //  Invoker.InvokeOnMainThread(() => Debug.Log("Calibration Result: " + calibResult.Result + " error: " + calibResult.AverageErrorDegree));

				Complete = true;
				Success = calibResult.Result;
				Error = calibResult.AverageErrorDegree;
		}

		#endregion

		#region ICalibrationResultListener implementation

		public void OnCalibrationChanged (bool isCalibrated, CalibrationResult calibResult)
		{
        Invoker.InvokeOnMainThread(() => Debug.Log("Calibration changed"));
				//Debug.Log ("Is Calibrated: " + isCalibrated + " result: " + calibResult.ToJson ());
		}

		#endregion

		#region IConnectionStateListener implementation

		public void OnConnectionStateChanged (bool isConnected)
		{
        Invoker.InvokeOnMainThread(() => Debug.Log("Connection state changed"));
		}

		#endregion

		#region ITrackerStateListener implementation

		public void OnTrackerStateChanged (GazeManager.TrackerState trackerState)
		{
        Invoker.InvokeOnMainThread(() => Debug.Log("State change"));
		}

		public void OnScreenStatesChanged (int screenIndex, int screenResolutionWidth, int screenResolutionHeight, float screenPhysicalWidth, float screenPhysicalHeight)
		{
        Invoker.InvokeOnMainThread(() => Debug.Log("Screen states chnaged"));
    }

		#endregion
}
