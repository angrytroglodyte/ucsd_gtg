﻿
using UnityEngine;

public class ETCamera : NMN.Behaviour
{
	private static ETCamera instance = null;

	public static bool Available { get { return instance != null; } }
	public static GameObject Target { get { return instance.target; } }
	public static Vector3 LastHitPosition { get { return instance.lastHit.point; } }
	public static Vector3 LastInputPosition { get { return instance.lastPosition; } }
	public static Vector3 LastWorldPosition { get { return instance.lastWorldPosition; } }
	public static bool LastPositionValid { get { return instance.lastPositionValid; } }

	private float Radius { get { return radius * GTG.Config.CursorScale; } }

	[SerializeField] private LayerMask layers;
	[SerializeField] private float radius = 1.0f;
	[SerializeField] private float gizmosDistance = 10.0f;

	private Camera cachedCamera = null;
	private GameObject target = null;
	private RaycastHit lastHit;
	private Vector3 lastPosition = Vector3.zero;
	private Vector3 lastWorldPosition = Vector3.zero;
	private bool lastPositionValid = false;

	private bool PositionValid
	{
		get
		{
			return ( GTG.Config.Emulate || User.UseMouse ) ? ( Input.GetMouseButton( 0 ) || User.UseMouse ) : EyeTribeClient.Valid;
		}
	}

	private Vector3 Position
	{
		get
		{
			return ( GTG.Config.Emulate || User.UseMouse ) ? Input.mousePosition : EyeTribeClient.Position;
		}
	}

	private void Awake( )
	{
		cachedCamera = camera;
		instance = this;
	}

	private void OnDestroy( )
	{
		instance = null;
	}

	private void LateUpdate( )
	{
		var valid = PositionValid && !UICamera.isOverUI;
		lastPositionValid = valid;

		if ( !valid )
		{
			if ( target != null )
			{
				target.SendMessage( "OnHover", false, SendMessageOptions.DontRequireReceiver );
				target = null;
			}
		}
		else
		{
			var newTarget = Raycast( Position );

			if ( target != newTarget )
			{
				if ( target != null )
				{
					target.SendMessage( "OnHover", false, SendMessageOptions.DontRequireReceiver );
				}

				target = newTarget;

				if ( target != null )
				{
					target.SendMessage( "OnHover", true, SendMessageOptions.DontRequireReceiver );
				}
			}
		}
	}

	private GameObject Raycast( Vector3 position )
	{
		var viewport = cachedCamera.ScreenToViewportPoint( position );

		if ( viewport.x < 0.0f || viewport.y < 0.0f || viewport.x > 1.0f || viewport.y > 1.0f )
		{
			return null;
		}

		var ray = cachedCamera.ScreenPointToRay( position );
		lastWorldPosition = ray.origin;
		lastPosition = position;

		if ( Physics.SphereCast( ray, Radius, out lastHit, cachedCamera.farClipPlane - cachedCamera.nearClipPlane, layers ) )
		{
			return lastHit.collider.gameObject;
		}

		return null;
	}

	private void OnDrawGizmos( )
	{
		if ( Application.isPlaying )
		{
			if ( GTG.Config.Instance != null )
			{
				var ray = cachedCamera.ScreenPointToRay( lastPosition );

				Gizmos.color = Color.blue;
				//Gizmos.DrawWireSphere( ray.origin + ( ray.direction * radius ), Radius );

				Gizmos.DrawWireSphere( ray.origin + ( ray.direction * gizmosDistance ), Radius );

				if ( Input.GetMouseButton( 1 ) )
				{
					Debug.Break( );
				}
			}
		}
	}
}