﻿
using UnityEngine;

public class EyeTribeClient : NMN.Singleton<EyeTribeClient>
{
	public static bool IsInitialised { get { return Instance.initialised; } }
	public static bool IsActive { get { return Instance.listener != null && Instance.listener.IsActivated; } }
	public static bool Valid { get { return Position.z > 0.5f; } }
	public static Vector3 Position { get { return Instance.gazePosInvertY; } }

	private Vector3 gazePosInvertY = new Vector3( Screen.width / 2, Screen.height / 2, 0 );
	private ETListener listener;
	private bool initialised = false;

	private void Start( )
	{
		if ( !GTG.Config.Emulate )
		{
			try
			{
				listener = new ETListener( );
			}
			catch ( System.Exception e )
			{
				print( e );
			}
		}

		initialised = true;

		enabled = IsActive;
	}

	private void Update( )
	{
		gazePosInvertY = listener.lastGazePoint;
		gazePosInvertY.y = Screen.height - gazePosInvertY.y;
	}
}
