﻿
using UnityEngine;

public class MoreSpaceScenary : NMN.Behaviour
{
	[SerializeField] private tk2dBaseSprite sprite = null;
	[SerializeField] private float speedScaling = 1.0f;

	private SpaceWizardsGame game = null;
	private float min = 0.0f;
	private float max = 0.0f;

	private new Transform transform = null;
	private Vector3 position = Vector3.zero;

	private void OnGameReady( GTG.BaseGame game )
	{
		this.game = game as SpaceWizardsGame;

		min = -sprite.GetUntrimmedBounds( ).size.y;
		max = this.game.ScrollingWorldHeight;
	}

	private void Update( )
	{
		if ( game != null )
		{
			position.y -= game.Speed * speedScaling * Time.deltaTime;
			position.y = NMN.Math.Wrap( position.y, min, max );
			transform.position = position;
		}
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void Start( )
	{
		transform = GetComponent<Transform>( );
		position = transform.position;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
