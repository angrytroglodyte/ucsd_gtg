﻿
using UnityEngine;

public class PlayerRail : NMN.Behaviour
{
	[SerializeField] private Transform start = null;
	[SerializeField] private Transform end = null;

	private Vector3 startToEnd = Vector3.zero;

	public Vector3 PositionOnRail( Vector3 target )
	{
		var startToTarget = target - start.position;
		var projection = Mathf.Clamp01( NMN.Math.ScalarProjection( startToTarget, startToEnd ) / ( startToEnd.magnitude ) );
		return start.position + ( startToEnd * projection );
	}

	private void Awake( )
	{
		startToEnd = end.position - start.position;
	}
}
