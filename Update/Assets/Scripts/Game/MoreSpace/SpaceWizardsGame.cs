﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceWizardsGame : GTG.BaseGame
{
	public event Action<int> OnLivesChanged;
	public event Action<int> OnEnemyDeath;

	[Serializable]
	private class Settings
	{
		public AnimationCurve scoreCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 100.0f );
		public AnimationCurve medicScoreCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 100.0f );

		public AnimationCurve speedCurve = AnimationCurve.Linear( 0.0f, 10.0f, 1.0f, 100.0f );
		public AnimationCurve minDelayCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 1.0f );
		public AnimationCurve maxDelayCurve = AnimationCurve.Linear( 0.0f, 4.0f, 1.0f, 1.5f );
		public AnimationCurve totalTracksCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 10.0f );
		public AnimationCurve medicChanceCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.2f );

		public AnimationCurve livesCurve = AnimationCurve.Linear( 0.0f, 5.0f, 1.0f, 3.0f );
	}

	[SerializeField] private Settings settings = null;
	[SerializeField] private new tk2dCamera camera = null;
	[SerializeField] private GameObject backgroundInput = null;
	[SerializeField] private Transform worldRoot = null;
	[SerializeField] private List<Transform> trackRoots = null;
	[SerializeField] private Transform endPoint = null;
	[SerializeField] private PlayerRail rail = null;
	[SerializeField] private float scrollingWorldHeight = 0.0f;
	[SerializeField] private AudioClip loseLife = null;

	public int Lives
	{
		get { return lives; }
		private set
		{
			lives = value;
			if ( OnLivesChanged != null )
			{
				OnLivesChanged( lives );
			}

			if ( lives <= 0 )
			{
				Victory = false;
				End( );
			}
		}
	}

	public int TotalLives { get; private set; }
	public int RemainingEnemies { get; private set; }
	public int TotalPlanes { get; private set; }

	public float Speed { get { return speed; } }
	public float ScrollingWorldHeight { get { return scrollingWorldHeight; } }
	public tk2dCamera Camera { get { return camera; } }

	private int scorePerShip = 0;
	private int scorePerMedic = 0;
	private float speed = 0.0f;
	private int lives = 0;
	private float minDelay = 0.0f;
	private float maxDelay = 0.0f;
	private int totalTracks = 0;
	private float medicChance = 0.0f;

	private SpaceWizard player = null;

	protected override IEnumerator GenerateLevel( )
	{
		yield return null;

		Victory = true;

		scorePerShip = Mathf.RoundToInt( settings.scoreCurve.Evaluate( Difficulty ) );
		scorePerMedic = Mathf.RoundToInt( settings.medicScoreCurve.Evaluate( Difficulty ) );

		speed = settings.speedCurve.Evaluate( Difficulty );
		lives = TotalLives = Mathf.RoundToInt( settings.livesCurve.Evaluate( Difficulty ) );
		minDelay = settings.minDelayCurve.Evaluate( Difficulty );
		maxDelay = settings.maxDelayCurve.Evaluate( Difficulty );
		totalTracks = Mathf.RoundToInt( settings.totalTracksCurve.Evaluate( Difficulty ) );

		medicChance = settings.medicChanceCurve.Evaluate( Difficulty );

		yield return null;

		var totalBadGuys = 0;
		var totalMedics = 0;
		var distance = Mathf.Abs( endPoint.position.y - trackRoots[0].position.y );
		var visibleTime = distance / speed;
		var stopTime = visibleTime * 1.1f;
		var time = Duration - Mathf.Lerp( 0.0f, minDelay, (float)Generator.NextDouble( ) );	// random offset to start
		while ( time > stopTime )
		{
			bool badGuy = (float)Generator.NextDouble( ) > medicChance;
			SetupSpawn( Duration - time, badGuy );

			if ( badGuy )
			{
				RemainingEnemies = TotalPlanes = TotalPlanes + 1;
				++totalBadGuys;
			}
			else
			{
				++totalMedics;
			}

			var interval = Mathf.Lerp( minDelay, maxDelay, (float)Generator.NextDouble( ) );	// random offset between new moles
			time -= interval;
		}

		yield return null;

		var playerPrefab = worldGenerator.ActivePool.RandomResource( "player", Generator );
		var playerInstance = Instantiate( playerPrefab, rail.transform.position, Quaternion.identity ) as GameObject;
		playerInstance.transform.parent = rail.transform;
		player = playerInstance.GetComponent<SpaceWizard>( );
		player.Setup( rail );

		session.AddMetaData( "TotalBadGuys", totalBadGuys );
		session.AddMetaData( "TotalMedics", totalMedics );

		yield break;
	}

	protected override void GameComplete( )
	{
	}

	private void SetupSpawn( float time, bool badGuy )
	{
		var pool = worldGenerator.ActivePool;
		var prefab = pool.RandomResource( badGuy ? "enemies" : "medic", Generator );
		var track = Generator.Next( totalTracks );
		AddAlert( time, ( t ) =>
			{
				SpawnEnemy( track, badGuy, prefab );
			} );
	}

	private void SpawnEnemy( int track, bool badGuy, GameObject prefab )
	{
		var spawn = trackRoots[track];
		var enemyInstance = Instantiate( prefab, spawn.position, Quaternion.identity ) as GameObject;
		RegisterInterest( enemyInstance );

		var enemy = enemyInstance.GetComponent<SpaghettiEnemy>( );
		enemy.Setup( speed, endPoint.position.y );

		if ( badGuy )
		{
			enemy.OnDeath += EnemyDeath;

			session.AddEvent( "BadGuySpawn" );
		}
		else
		{
			enemy.OnDeath += MedicDeath;
			session.AddEvent( "MedicSpawn" );
		}

		enemy.transform.parent = worldRoot;
	}

	private void EnemyDeath( SpaghettiEnemy enemy, bool killed )
	{
		--RemainingEnemies;

		if ( OnEnemyDeath != null )
		{
			OnEnemyDeath( RemainingEnemies );
		}

		if ( !killed )
		{
			Audio.PlaySFX( loseLife );
			--Lives;

			session.AddEvent( "EnemyEscaped", new
			{
				RelativeTime = enemy.Age
			} );
		}
		else
		{
			Score += scorePerShip;

			session.AddEvent( "EnemyDeath", new
			{
				ScreenPosition = ScreenPosition( enemy.transform.position ).Serialize( ),
				RelativeTime = enemy.Age
			} );
		}
	}

	private void MedicDeath( SpaghettiEnemy enemy, bool killed )
	{
		if ( killed )
		{
			--Lives;

			session.AddEvent( "MedicDeath", new
			{
				ScreenPosition = ScreenPosition( enemy.transform.position ).Serialize( ),
				RelativeTime = enemy.Age
			} );
		}
		else
		{
			Score += scorePerMedic;

			session.AddEvent( "MedicSaved", new
			{
				RelativeTime = enemy.Age
			} );
		}
	}

	private void Update( )
	{
		if ( Running )
		{
			var targetLock = ETCamera.LastPositionValid;
			var target = ( ETCamera.Target != null ) ? ETCamera.Target.transform.position : player.transform.position;

			if ( backgroundInput == ETCamera.Target )
			{
				targetLock = false;
				target = ETCamera.LastHitPosition;
			}

			player.Step( target, targetLock );
		}
		else if ( player != null )
		{
			player.Step( player.transform.position, false );
		}
	}

	private void OnDrawGizmos( )
	{
		Gizmos.DrawLine( Vector3.zero, Vector3.up * ScrollingWorldHeight );

		var collider = backgroundInput.GetComponent<BoxCollider>( );

		Gizmos.matrix = backgroundInput.transform.localToWorldMatrix;
		Gizmos.DrawWireCube( collider.center, collider.size );
	}
}
