﻿
using System;
using UnityEngine;

public class SpaghettiEnemy : NMN.Behaviour
{
	public event Action<SpaghettiEnemy, bool> OnDeath;

	public float SpawnTime { get; private set; }
	public float Age { get { return Time.time - SpawnTime; } }

	[SerializeField] private float life = 1.5f;
	[SerializeField] private Color deathTint = Color.red;
	[SerializeField] private GameObject explosion = null;

	private float endPoint = 0.0f;
	private bool underFire = false;
	private float underFireStart = 0.0f;
	private Renderer[] renderers = null;

	public void Setup( float speed, float endPoint )
	{
		this.endPoint = endPoint;
		SpawnTime = Time.time;
		rigidbody.velocity = Vector3.up * (-speed);

		renderers = GetComponentsInChildren<Renderer>( );
	}

	private void FixedUpdate( )
	{
		if ( transform.position.y < endPoint )
		{
			Die( false );
		}
	}

	private void OnHover( bool hover )
	{
		underFire = hover;
		if ( hover )
		{
			underFireStart = Time.time;
		}
		else
		{
			Tint( Color.white );
		}
	}

	private void Update( )
	{
		if ( underFire )
		{
			var time = Time.time - underFireStart;
			var t = time / life;

			var colour = Color.Lerp( Color.white, deathTint, t );
			Tint( colour );

			if ( time > life )
			{
				Die( true );
			}
		}
	}

	private void Tint( Color colour )
	{
		foreach ( var renderer in renderers )
		{
			renderer.material.color = colour;
		}
	}

	private void Die( bool killed )
	{
		Destroy( gameObject );

		if ( OnDeath != null )
		{
			OnDeath( this, killed );
		}

		if ( killed )
		{
			var explosion = Instantiate( this.explosion, transform.position, transform.rotation ) as GameObject;
			explosion.transform.parent = transform.parent;
		}
	}
}
