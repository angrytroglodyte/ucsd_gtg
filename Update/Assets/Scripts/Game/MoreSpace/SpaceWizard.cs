﻿
using UnityEngine;

public class SpaceWizard : NMN.Behaviour
{
	[SerializeField] private float speed = 1.0f;
	[SerializeField] private float maxPitch = 45.0f;
	[SerializeField] private LineRenderer[] bulletTrails = null;
	[SerializeField] private AudioClip bulletAudio = null;
	[SerializeField] private AudioClip engineAudio = null;

	private PlayerRail rail = null;
	private Vector3 latestTarget = Vector3.zero;
	private Vector3 velocity = Vector3.zero;

	private GTG.AudioPlayer bulletPlayer = null;
	private GTG.AudioPlayer enginePlayer = null;

	public void Setup( PlayerRail rail )
	{
		this.rail = rail;
		transform.position = rail.PositionOnRail( transform.position );
		latestTarget = transform.position;

		foreach ( var trail in bulletTrails )
		{
			trail.SetVertexCount( 2 );
		}
	}

	public void Step( Vector3 target, bool targetLock )
	{
		latestTarget = target;
		latestTarget.z = transform.position.z;

		var railTarget = rail.PositionOnRail( latestTarget );
		transform.position = Vector3.SmoothDamp( transform.position, railTarget, ref velocity, 1.0f, speed, Time.deltaTime );

		transform.rotation = Quaternion.Euler( 0.0f, ( velocity.x / speed ) * maxPitch * -1.0f, 0.0f );

		foreach ( var trail in bulletTrails )
		{
			UpdateTrail( trail, targetLock );
		}

		bulletPlayer.Volume = targetLock ? 1.0f : 0.0f;
	}

	private void UpdateTrail( LineRenderer trail, bool targetLock )
	{
		if ( !targetLock )
		{
			trail.enabled = false;
			return;
		}

		trail.enabled = true;
		trail.SetVertexCount( 2 );
		trail.SetPosition( 0, trail.transform.position );
		trail.SetPosition( 1, latestTarget );
	}

	private void Awake( )
	{
		enginePlayer = GTG.AudioPlayer.Make( Audio.SFX, engineAudio );
		enginePlayer.Play( 1.0f, true );

		bulletPlayer = GTG.AudioPlayer.Make( Audio.SFX, bulletAudio );
		bulletPlayer.Play( 0.0f, true );
	}

	private void OnDestroy( )
	{
		Destroy( enginePlayer.gameObject );
		Destroy( bulletPlayer.gameObject );
	}

	private void OnDrawGizmos( )
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere( latestTarget, 0.5f );
	}
}
