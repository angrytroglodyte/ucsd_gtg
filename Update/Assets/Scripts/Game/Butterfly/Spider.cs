﻿
using UnityEngine;

public class Spider : NMN.Behaviour
{
	public event System.Action<Spider> OnSpawn;
	public event System.Action<Spider> OnDespawn;

	public float Age { get { return Time.time - visibleTime; } }

	[SerializeField] private TweenPosition tween = null;
	[SerializeField] private float dropAmount = 0.0f;
	[SerializeField] private AudioClip decend = null;

	private float decendPoint = float.MinValue;
	private bool down = false;
	private float visibleTime = 0.0f;

	private void Update( )
	{
		if ( enabled )
		{
			if ( !down )
			{
				if ( transform.position.x < decendPoint )
				{
					tween.PlayForward( );
					Audio.PlaySFX( decend );
					down = true;

					visibleTime = Time.time;

					if ( OnSpawn != null )
					{
						OnSpawn( this );
					}
				}
			}
			else
			{
				if ( transform.position.x < 0.0f )
				{
					tween.PlayReverse( );
					enabled = false;

					if ( OnDespawn != null )
					{
						OnDespawn( this );
					}
				}
			}
		}
	}

	private void OnGameReady( GTG.BaseGame baseGame )
	{
		var game = baseGame as ButterflyGame;

		decendPoint = game.ScreenWorldWidth;
		tween.to.y = dropAmount;
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
