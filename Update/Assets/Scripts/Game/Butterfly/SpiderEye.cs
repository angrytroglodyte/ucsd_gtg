﻿
using UnityEngine;

public class SpiderEye : NMN.Behaviour
{
	[SerializeField] private Transform reference = null;

	private float scale = 1.0f;

	private void Start( )
	{
		scale = transform.localPosition.magnitude;
	}

	private void Update( )
	{
		transform.localPosition = reference.localPosition * scale;
	}
}