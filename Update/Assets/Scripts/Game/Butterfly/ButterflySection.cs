﻿
using UnityEngine;
using System.Collections.Generic;

using Random = System.Random;

public class ButterflySection : NMN.Behaviour
{
	public float Width { get { return width; } }
	public string Next { get { return nextCategory; } }

	public GameObject Star { get; private set; }
	public GameObject Spider { get; private set; }

	[SerializeField] private float width = 0.0f;
	[SerializeField] private string nextCategory = string.Empty;
	[SerializeField] private List<Transform> starLocations = null;
	[SerializeField] private Transform spiderRoot = null;

	private ButterflyGame game = null;
	private new Transform transform = null;
	private Vector3 position = Vector3.zero;

	public bool SpawnStar( ResourcePool pool, Random generator )
	{
		if ( starLocations.Count > 0 )
		{
			var index = generator.Next( starLocations.Count );
			var position = starLocations[index];
			starLocations.RemoveAt( index );

			var starPrefab = pool.RandomResource( "star", generator );
			var star = Instantiate( starPrefab, position.position, Quaternion.identity ) as GameObject;
			star.transform.parent = position;

			Star = star;

			return true;
		}

		return false;
	}

	public bool SpawnSpider( ResourcePool pool, Random generator )
	{
		if ( spiderRoot != null )
		{
			var spiderPrefab = pool.RandomResource( "spider", generator );
			var spider = Instantiate( spiderPrefab, spiderRoot.position, Quaternion.identity ) as GameObject;
			spider.transform.parent = spiderRoot;

			Spider = spider;

			return true;
		}

		return false;
	}

	private void OnGameReady( GTG.BaseGame baseGame )
	{
		game = baseGame as ButterflyGame;
		transform = GetComponent<Transform>( );
		position = transform.position;
	}

	private void Update( )
	{
		if ( game != null && game.Running )
		{
			position.x -= game.Speed * Time.deltaTime;
			transform.position = position;
		}
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}

	private void OnDrawGizmos( )
	{
		Gizmos.color = Color.red;
		var t = GetComponent<Transform>( );
		Gizmos.DrawLine( t.position, t.position + ( Vector3.right * width ) );
	}

#if UNITY_EDITOR
	[ContextMenu( "Spawn Star" )]
	private void SpawnStar( )
	{
		if ( starLocations == null )
		{
			starLocations = new List<Transform>( );
		}

		var star = NMN.Tools.AddChild( gameObject );
		star.name = "star";
		starLocations.Add( star.transform );

		UnityEditor.Selection.activeGameObject = star;
	}
#endif
}
