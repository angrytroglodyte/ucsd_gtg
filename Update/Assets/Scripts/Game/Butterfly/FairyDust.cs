﻿
using UnityEngine;

public class FairyDust : NMN.Behaviour
{
	public event System.Action<FairyDust> OnSpawn;
	public event System.Action<FairyDust> OnDespawn;

	public float Age { get { return Time.time - visibleTime; } }

	private float decendPoint = float.MinValue;
	private bool visible = false;
	private float visibleTime = 0.0f;

	private void Update( )
	{
		if ( enabled )
		{
			if ( !visible )
			{
				if ( transform.position.x < decendPoint )
				{
					visible = true;
					visibleTime = Time.time;

					if ( OnSpawn != null )
					{
						OnSpawn( this );
					}
				}
			}
			else
			{
				if ( transform.position.x < 0.0f )
				{
					enabled = false;

					if ( OnDespawn != null )
					{
						OnDespawn( this );
					}
				}
			}
		}
	}

	private void OnGameReady( GTG.BaseGame baseGame )
	{
		var game = baseGame as ButterflyGame;

		decendPoint = game.ScreenWorldWidth;
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
