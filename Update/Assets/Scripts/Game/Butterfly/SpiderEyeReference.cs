﻿
using UnityEngine;

public class SpiderEyeReference : NMN.Behaviour
{
	[SerializeField] private Transform eye = null;

	private Transform bob = null;

	private void Update( )
	{
		if ( bob != null )
		{
			var delta = bob.position - transform.position;
			if ( delta.sqrMagnitude > 1.0f )
			{
				delta.Normalize( );
			}

			eye.localPosition = delta;
		}
	}

	private void OnGameReady( GTG.BaseGame baseGame )
	{
		var game = baseGame as ButterflyGame;

		bob = game.Bob.transform;
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}