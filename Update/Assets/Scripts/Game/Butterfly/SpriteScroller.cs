﻿
using UnityEngine;

public class SpriteScroller : NMN.Behaviour
{
	[SerializeField] private float scaling = 1.0f;

	private ButterflyGame game = null;
	private new Transform transform = null;
	private Vector3 position = Vector3.zero;

	private float width = 0.0f;

	private void OnGameReady( GTG.BaseGame baseGame )
	{
		game = baseGame as ButterflyGame;
	}

	private void Update( )
	{
		if ( game != null && game.Running )
		{
			position.x -= game.Speed * scaling * Time.deltaTime;

			var overshoot = ( position.x + width );
			if ( overshoot < 0.0f )
			{
				position.x += game.ScrollingWorldWidth;
			}

			transform.position = position;
		}
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void Start( )
	{
		transform = GetComponent<Transform>( );
		position = transform.position;

		var maxX = float.MinValue;
		var sprites = GetComponentsInChildren<tk2dBaseSprite>( );

		foreach ( var sprite in sprites )
		{
			var bounds = sprite.GetUntrimmedBounds( );

			var topRight = bounds.max;
			var bottomRight = new Vector3( bounds.max.x, bounds.min.y, 0.0f );
			var topLeft = new Vector3( bounds.min.x, bounds.max.y, 0.0f );
			var bottomLeft = bounds.min;

			maxX = Mathf.Max( maxX, sprite.transform.TransformPoint( topRight ).x );
			maxX = Mathf.Max( maxX, sprite.transform.TransformPoint( bottomRight ).x );
			maxX = Mathf.Max( maxX, sprite.transform.TransformPoint( topLeft ).x );
			maxX = Mathf.Max( maxX, sprite.transform.TransformPoint( bottomLeft ).x );
		}

		width = maxX - transform.position.x;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
