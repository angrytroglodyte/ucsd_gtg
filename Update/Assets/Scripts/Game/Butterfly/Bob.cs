﻿
using System;
using UnityEngine;

using NMN.Extensions;

public class Bob : NMN.Behaviour
{
	public event Action<GameObject> OnHitStar;
	public event Action<GameObject> OnHitSpider;
	public event Action OnDeath;

	public bool AutoPilot { get; private set; }

	[SerializeField] private tk2dBaseSprite sprite = null;

	[SerializeField] private float speedScaler = 1.0f;
	[SerializeField] private Color damageTint = Color.red;
	[SerializeField] private AnimationCurve damageTintCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.0f );
	[SerializeField] private TweenPosition deathTween = null;

	private bool running = false;
	private float autoSpeed = 0.0f;
	private Vector3 target = Vector3.zero;
	private float speed = 0.0f;

	public void Setup( float baseSpeed )
	{
		speed = baseSpeed * speedScaler;
	}

	public void StartAutoPilot( float autoSpeed )
	{
		AutoPilot = true;
		this.autoSpeed = autoSpeed;
	}

	public void StopAutoPilot( )
	{
		AutoPilot = false;
	}

	public void UpdateTarget( Vector3 target )
	{
		this.target = target;
		this.target.z = transform.position.z;
	}

	public void Die( )
	{
		deathTween.from = transform.localPosition;

		deathTween.to.x = transform.localPosition.x;
		deathTween.to.z = transform.localPosition.z;

		deathTween.PlayForward( );
	}

	private void Update( )
	{
		if ( running )
		{
			if ( AutoPilot )
			{
				transform.position += Vector3.right * autoSpeed * Time.deltaTime;
			}
			else
			{
				var meToTarget = target - transform.position;
				if ( meToTarget.sqrMagnitude > 0.01f )
				{
					transform.position += meToTarget.normalized * speed * Time.deltaTime;
				}
			}
		}
	}

	private void Awake( )
	{
		AutoPilot = false;
		CountdownTimer.OnCountdownStart += OnCountdownStart;
	}

	private void OnDestroy( )
	{
		CountdownTimer.OnCountdownStart -= OnCountdownStart;
	}

	private void OnCountdownStart( )
	{
		running = true;
	}

	private void OnTriggerEnter( Collider collider )
	{
		if ( collider.tag == "Star" )
		{
			if ( OnHitStar != null )
			{
				OnHitStar( collider.gameObject );
			}
		}
		else
		{
			NMN.Job.Make( damageTintCurve.Run( ( ratio ) =>
			{
				sprite.color = Color.Lerp( Color.white, damageTint, ratio );
			} ) );

			if ( collider.tag == "Spider" )
			{
				if ( OnHitSpider != null )
				{
					OnHitSpider( collider.gameObject );
				}
			}
			else
			{
				if ( OnDeath != null )
				{
					OnDeath( );
				}

				enabled = false;
			}
		}
	}
}
