﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButterflyGame : GTG.BaseGame
{
		public event Action OnStarCollected;
		public event Action<int> OnLivesChanged;

		[Serializable]
		private class Settings
		{
				public AnimationCurve scoreCurve = AnimationCurve.Linear (0.0f, 1.0f, 1.0f, 2.0f);
				public AnimationCurve speedCurve = AnimationCurve.Linear (0.0f, 1.0f, 1.0f, 2.0f);
				public AnimationCurve lengthCurve = AnimationCurve.Linear (0.0f, 50.0f, 1.0f, 100.0f);

				public AnimationCurve livesCurve = AnimationCurve.Linear (0.0f, 3.0f, 1.0f, 3.0f);

				public AnimationCurve totalStarsCurve = AnimationCurve.Linear (0.0f, 5.0f, 1.0f, 10.0f);
				public AnimationCurve starScoreCurve = AnimationCurve.Linear (0.0f, 1.0f, 1.0f, 100.0f);

				public AnimationCurve totalSpidersCurve = AnimationCurve.Linear (0.0f, 3.0f, 1.0f, 5.0f);
		}

		[SerializeField] private Settings settings = null;
		[SerializeField] private GameObject inputRoot = null;
		[SerializeField] private float scrollingWidth = 0.0f;
		[SerializeField] private Transform worldRoot = null;
		[SerializeField] private Transform playerRoot = null;
		[SerializeField] private float countDownLength = 4.0f;
		[SerializeField] private AudioClip deathSFX = null;
		[SerializeField] private AudioClip collectStarSFX = null;

		public int Lives {
				get { return lives; }
				private set {
						lives = value;
						if (OnLivesChanged != null) {
								OnLivesChanged (lives);
						}

						if (lives <= 0) {
								Audio.PlaySFX (deathSFX);
								Victory = false;
								bob.Die ();
								End ();
						}
				}
		}

		public int TotalLives { get; private set; }

		public float Speed { get; private set; }

		public float ScrollingWorldWidth { get { return scrollingWidth; } }

		public float ScreenWorldWidth { get { return inputRoot.transform.localScale.x; } }

		public override float Progress { get { return Mathf.Clamp01 (distanceTravelled / actualDistance); } }

		public int TotalStars { get; private set; }

		public int RemainingStars { get; private set; }

		public int StarsHit { get { return TotalStars - RemainingStars; } }

		public Bob Bob { get { return bob; } }

		private Bob bob = null;
		private int lives = 0;
		private float scorePerMeter = 0.0f;
		private float minimumLength = 0;
		private float pointCounter = 0.0f;
		private float actualDistance = 0.0f;
		private int starsToSpawn = 0;
		private int scorePerStar = 0;
		private int spidersToSpawn = 0;

		private float distanceTravelled = 0.0f;
		private List<ButterflySection> liveSections = new List<ButterflySection> ();

		protected override IEnumerator GenerateLevel ()
		{
				yield return null;

				Victory = true;

				scorePerMeter = settings.scoreCurve.Evaluate (Difficulty);
				Speed = settings.speedCurve.Evaluate (Difficulty);
				minimumLength = settings.lengthCurve.Evaluate (Difficulty);

				starsToSpawn = Mathf.RoundToInt (settings.totalStarsCurve.Evaluate (Difficulty));
				scorePerStar = Mathf.RoundToInt (settings.starScoreCurve.Evaluate (Difficulty));

				spidersToSpawn = Mathf.RoundToInt (settings.totalSpidersCurve.Evaluate (Difficulty));

				lives = TotalLives = Mathf.RoundToInt (settings.livesCurve.Evaluate (Difficulty));

				yield return null;

				var pool = worldGenerator.ActivePool;
				var startSpacerPrefab = pool.RandomResource ("spacer_00", Generator);
				var startSpacerInstance = Instantiate (startSpacerPrefab) as GameObject;
				startSpacerInstance.transform.parent = worldRoot;
				var startSpacer = startSpacerInstance.GetComponent<ButterflySection> ();
				var startSpacerWidth = startSpacer.Width;
				var nextSectionType = startSpacer.Next;

				var currentX = 0.0f;
				do {
						var x = currentX + startSpacerWidth;

						var sectionPrefab = pool.RandomResource ("section_" + nextSectionType, Generator);
						var sectionInstance = Instantiate (sectionPrefab) as GameObject;
						var section = sectionInstance.GetComponent<ButterflySection> ();

						sectionInstance.transform.parent = worldRoot;
						sectionInstance.transform.position = Vector3.right * x;

						liveSections.Add (section);

						nextSectionType = section.Next;
						currentX += section.Width;

						yield return null;
				} while (currentX < minimumLength);

				var stopSpacerPrefab = pool.RandomResource ("spacer_" + nextSectionType, Generator);
				var stopSpacerInstance = Instantiate (stopSpacerPrefab) as GameObject;
				stopSpacerInstance.transform.parent = worldRoot;
				stopSpacerInstance.transform.position = Vector3.right * (currentX + startSpacerWidth);

				actualDistance = currentX + startSpacerWidth;

				yield return null;

				var bobPrefab = pool.RandomResource ("bob", Generator);
				var bobInstance = Instantiate (bobPrefab, playerRoot.position - (Vector3.right * Speed * countDownLength), Quaternion.identity) as GameObject;
				bobInstance.transform.parent = playerRoot;
				bob = bobInstance.GetComponent<Bob> ();
				bob.Setup (Speed);
				bob.StartAutoPilot (Speed);

				yield return null;

				//	An adaption of Bresenham's line algorithm <http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm> to evenly position stars along our path.
				//	Am I a genius or what?

				var runningStarsError = 0.0f;
				var starsErrorDelta = (float)starsToSpawn / (float)liveSections.Count;

				var runningSpiderError = 0.0f;
				var spiderErrorDelta = (float)spidersToSpawn / (float)liveSections.Count;

				var spiders = 0;
				for (int i = 0; i < liveSections.Count; ++i) {
						runningStarsError += starsErrorDelta;
						runningSpiderError += spiderErrorDelta;

						var spawned = false;
						while (runningStarsError > 0.5f) {
								if (!spawned && liveSections [i].SpawnStar (pool, Generator)) {
										++TotalStars;
										spawned = true;

										RegisterInterest (liveSections [i].Star);

										var star = liveSections [i].Star.GetComponent<FairyDust> ();
										RegisterInterest (star.gameObject);

										star.OnSpawn += ( s) => {
												session.AddEvent ("StarEnter", new
						{
							ScreenPosition = ScreenPosition (s.transform.position).Serialize ()
						});
										};

										star.OnDespawn += ( s) => {
												session.AddEvent ("StarExit", new
						{
							ScreenPosition = ScreenPosition (s.transform.position).Serialize (),
							RelativeTime = s.Age
						});
										};
								}

								runningStarsError -= 1.0f;
						}

						spawned = false;
						while (runningSpiderError > 0.5f) {
								if (!spawned && liveSections [i].SpawnSpider (pool, Generator)) {
										spawned = true;
										++spiders;

										var spider = liveSections [i].Spider.GetComponent<Spider> ();
										RegisterInterest (spider.gameObject);

										spider.OnSpawn += ( s) => {
												session.AddEvent ("SpiderEnter", new
							{
								ScreenPosition = ScreenPosition (s.transform.position).Serialize ()
							});
										};

										spider.OnDespawn += ( s) => {
												session.AddEvent ("SpiderExit", new
							{
								ScreenPosition = ScreenPosition (s.transform.position).Serialize (),
								RelativeTime = s.Age
							});
										};
								}

								runningSpiderError -= 1.0f;
						}
				}

				RemainingStars = TotalStars;

				session.AddMetaData ("TotalSpiders", spiders);
				session.AddMetaData ("TotalStars", TotalStars);
				session.AddMetaData ("TotalSections", liveSections.Count);
				session.AddMetaData ("TotalDistance", actualDistance);

				yield return null;

				OnGameStart += () => {
						bob.StopAutoPilot ();
				};

				bob.OnHitStar += ( star) => {
						Audio.PlaySFX (collectStarSFX);

						Destroy (star);
						User.AwardPoints (scorePerStar);

						--RemainingStars;
						if (OnStarCollected != null) {
								OnStarCollected ();
						}

						Debug.Log (NGUITools.GetHierarchy (star));

						session.AddEvent ("StarCollected", new
				{
					ScreenPosition = ScreenPosition (star.transform.position).Serialize (),
					RelativeTime = star.GetComponent<FairyDust> ().Age
				});
				};

				bob.OnHitSpider += ( spider) => {
						--Lives;

						Debug.Log (NGUITools.GetHierarchy (spider));

						var rigidbody = spider.collider.attachedRigidbody;
						rigidbody.detectCollisions = false;

						session.AddEvent ("SpiderHit", new
				{
					ScreenPosition = ScreenPosition (spider.transform.position).Serialize (),
					RelativeTime = spider.GetComponentInParent<Spider> ().Age
				});
				};

				bob.OnDeath += () => {
						session.AddEvent ("WorldHit", new
				{
					ScreenPosition = ScreenPosition (bob.transform.position).Serialize ()
				});

						Lives = 0;
				};

				yield break;
		}

		private void Update ()
		{
				if (Running) {
						if (ETCamera.Target == inputRoot) {
								bob.UpdateTarget (ETCamera.LastHitPosition);
						}

						var distance = Speed * Time.deltaTime;
						distanceTravelled += distance;

						if (distanceTravelled > actualDistance) {
								End ();
								bob.StartAutoPilot (Speed);
						}

						pointCounter += scorePerMeter * distance;
						while (pointCounter > 1.0f) {
								//TODO: This triggers a SAVE every update, so we're removing it ++Score;
								pointCounter -= 1.0f;
						}
				}
		}

		protected override void GameComplete ()
		{
		}

		private void OnDrawGizmos ()
		{
				Gizmos.DrawLine (Vector3.zero, Vector3.right * ScrollingWorldWidth);

				var collider = inputRoot.GetComponent<BoxCollider> ();

				Gizmos.matrix = inputRoot.transform.localToWorldMatrix;
				Gizmos.DrawWireCube (collider.center, collider.size);
		}
}
