﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

public class SpawnerWorldNode : GTG.BaseWorldNode
{
	[SerializeField] private string category = string.Empty;

	public override void BuildTask( NMN.Job job, ResourcePool pool, Random generator )
	{
		job.AddChild( Spawn( pool, generator ) );
	}

	private IEnumerator Spawn( ResourcePool pool, Random generator )
	{
		yield return null;

		var prefab = pool.RandomResource( category, generator );
		var instance = Instantiate( prefab ) as GameObject;
		
		instance.transform.parent = transform;
		instance.transform.localPosition = Vector3.zero;
		instance.transform.localScale = Vector3.one;
		instance.transform.localRotation = Quaternion.identity;

		yield break;
	}

	private void OnDrawGizmos( )
	{
		Gizmos.color = Color.red;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawSphere( Vector3.zero, 0.5f );
	}
}
