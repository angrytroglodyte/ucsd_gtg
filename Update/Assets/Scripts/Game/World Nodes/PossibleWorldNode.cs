﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

public class PossibleWorldNode : GTG.BaseWorldNode
{
	[SerializeField] private float chance = 0.5f;

	public override void BuildTask( NMN.Job job, ResourcePool pool, Random generator )
	{
		if ( chance >= (float)generator.NextDouble( ) )
		{
			var child = children[0];
			child.BuildTask( job, pool, generator );
		}
	}
}
