﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

public class AllChildrenWorldNode : GTG.BaseWorldNode
{
	public override void BuildTask( NMN.Job job, ResourcePool pool, Random generator )
	{
		foreach ( var child in children )
		{
			child.BuildTask( job, pool, generator );
		}
	}
}
