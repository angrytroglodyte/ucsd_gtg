﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

public class SelectChildWorldNode : GTG.BaseWorldNode
{
	public override void BuildTask( NMN.Job job, ResourcePool pool, Random generator )
	{
		var child = children[ generator.Next( children.Count) ];
		child.BuildTask( job, pool, generator );
	}
}
