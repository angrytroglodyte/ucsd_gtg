﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

namespace GTG
{
	public abstract class BaseWorldNode : NMN.Behaviour
	{
		protected List<GTG.BaseWorldNode> children = new List<GTG.BaseWorldNode>( );

		public abstract void BuildTask( NMN.Job job, ResourcePool pool, Random generator );

		private void Start( )
		{
			foreach ( Transform child in transform )
			{
				if ( child.gameObject.activeSelf )
				{
					var node = child.GetComponent<GTG.BaseWorldNode>( );
					if ( node != null )
					{
						children.Add( node );
					}
				}
			}
		}
	}
}
