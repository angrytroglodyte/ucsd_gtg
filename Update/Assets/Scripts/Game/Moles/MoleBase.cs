﻿
using System;
using UnityEngine;

namespace GTG
{
	public abstract class MoleBase : NMN.Behaviour
	{
		public event Action<MoleBase> OnLookStart;
		public event Action<MoleBase> OnLookStop;
		public event Action<MoleBase, bool> OnDeath;

		public abstract MoleHill.MoleType Type { get; }
		public float Age { get { return Time.time - spawnTime; } }
		public GameObject Focus { get { return sprite.gameObject; } }

		[SerializeField] protected tk2dSpriteAnimator sprite = null;
		[SerializeField] protected Color deathTint = Color.red;

		protected bool whacked = false;
		private float spawnTime = 0.0f;

		public abstract void Run( MoleGame game );

		public virtual void LookStart( )
		{
			if ( OnLookStart != null )
			{
				OnLookStart( this );
			}
		}

		public virtual void LookStop( )
		{
			if ( OnLookStop != null )
			{
				OnLookStop( this );
			}
		}

		public virtual void Whack( )
		{
			sprite.Play( "death" );
			whacked = true;

			if ( OnDeath != null )
			{
				OnDeath( this, whacked );
			}
		}

		protected void CleanUp( )
		{
			LookStop( );
			Destroy( gameObject );

			if ( !whacked )
			{
				if ( OnDeath != null )
				{
					OnDeath( this, whacked );
				}
			}
		}

		private void Awake( )
		{
			spawnTime = Time.time;

			sprite.AnimationEventTriggered = ( animator, clip, frame ) =>
				{
					sprite.Sprite.color = deathTint;
				};
		}
	}
}
