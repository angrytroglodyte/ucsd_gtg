﻿
using UnityEngine;
using System.Collections;

public class WarningPopupMole : PopupMole
{
	[SerializeField] private GameObject warning = null;

	public override MoleHill.MoleType Type { get { return MoleHill.MoleType.Good; } }

	public override void Run( MoleGame game )
	{
		job.AddChild( Warning( game.WarningDuration ) );

		base.Run( game );
	}

	public override void LookStart( )
	{
		Whack( );
	}

	public override void LookStop( )
	{
	}

	private IEnumerator Warning( float duration )
	{
		yield return null;

		warning.SetActive( true );

		for ( var time = 0.0f; time < duration; time += Time.deltaTime )
		{
			yield return null;
		}

		warning.SetActive( false );

		yield break;
	}
}