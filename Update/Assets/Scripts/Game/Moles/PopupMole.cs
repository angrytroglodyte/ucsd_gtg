﻿
using UnityEngine;
using System.Collections;

public class PopupMole : GTG.MoleBase
{
	[SerializeField] protected Vector2 transitionTargets = new Vector2( -1.0f, 0.0f );

	public override MoleHill.MoleType Type { get { return MoleHill.MoleType.Evil; } }

	protected NMN.Job job = NMN.Job.MakeNull( false );

	public override void Run( MoleGame game )
	{
		var transition = game.TransitionDuration;
		var lifetime = game.LifetimeDuration;

		job.AddChild( Transition( transitionTargets.y, transition ) );
		job.AddChild( Wait( lifetime ) );
		job.AddChild( Transition( transitionTargets.x, transition ) );
		job.OnComplete += ( killed ) =>
			{
				if ( !killed )
				{
					CleanUp( );
				}
			};

		job.Start( );
	}

	private void OnDestroy( )
	{
		job.Kill( );
	}

	private IEnumerator Transition( float target, float duration )
	{
		yield return null;

		var current = sprite.transform.localPosition;
		var final = Vector3.up * target;

		for ( var time = 0.0f; time < duration; time += Time.deltaTime )
		{
			var t = NMN.Math.SmoothStep( 0.0f, duration, time );
			sprite.transform.localPosition = Vector3.Lerp( current, final, t );

			yield return null;
		}

		sprite.transform.localPosition = final;

		yield break;
	}

	private IEnumerator Wait( float lifetime )
	{
		yield return null;

		for ( var time = 0.0f; time < lifetime && !whacked; time += Time.deltaTime )
		{
			yield return null;
		}

		yield break;
	}
}
