﻿
using UnityEngine;
using System.Collections;

public class ParachuteMole : GTG.MoleBase
{
	[SerializeField] protected float transitionTarget = -1.0f;

	public override MoleHill.MoleType Type { get { return MoleHill.MoleType.Parachute; } }

	protected NMN.Job job = NMN.Job.MakeNull( false );

	public override void Run( MoleGame game )
	{
		job.AddChild( Transition( game.ParachuteMoleLifetime ) );
		job.OnComplete += ( killed ) =>
		{
			if ( !killed )
			{
				CleanUp( );
			}
		};

		job.Start( );
	}

	private void OnDestroy( )
	{
		job.Kill( );
	}

	private IEnumerator Transition( float duration )
	{
		yield return null;

		var current = sprite.transform.localPosition;
		var final = Vector3.up * transitionTarget;

		for ( var time = 0.0f; time < duration; time += Time.deltaTime )
		{
			var t = NMN.Math.LinearStep( 0.0f, duration, time );
			sprite.transform.localPosition = Vector3.Lerp( current, final, t );

			yield return null;
		}

		sprite.transform.localPosition = final;

		yield break;
	}
}
