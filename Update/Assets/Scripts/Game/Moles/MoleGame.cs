﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoleGame : GTG.BaseGame
{
	public event Action<int> OnLivesChanged;
	public event Action<GTG.MoleBase> OnCountdownMole;
	public event Action<int> OnMoleWhacked;

	[Serializable]
	private class Settings
	{
		public AnimationCurve livesCurve = AnimationCurve.Linear( 0.0f, 5.0f, 1.0f, 3.0f );

		public AnimationCurve scoreCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 100.0f );
		public AnimationCurve transitionDurationCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 0.5f );
		
		public AnimationCurve lifetimeCurve = AnimationCurve.Linear( 0.0f, 2.0f, 1.0f, 0.5f );
		public AnimationCurve parachuteTimeCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 3.0f );
		public AnimationCurve warningCurve = AnimationCurve.Linear( 0.0f, 0.5f, 1.0f, 0.5f );

		public AnimationCurve redHerringCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.25f );
		public AnimationCurve parachuteCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.25f );

		public AnimationCurve minDelayCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 1.0f );
		public AnimationCurve maxDelayCurve = AnimationCurve.Linear( 0.0f, 4.0f, 1.0f, 1.5f );

		public AnimationCurve totalHillsCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 10.0f );
	}

	public int Lives
	{
		get { return lives; }

		private set
		{
			lives = value;
			if ( OnLivesChanged != null )
			{
				OnLivesChanged( lives );
			}

			if ( lives <= 0 )
			{
				Victory = false;
				End( );
			}
		}
	}

	public Camera SceneCamera { get { return gameCamera; } }
	public int TotalLives { get; private set; }

	public int MolesHit { get; private set; }
	public int TotalMoles { get; private set; }
	public float Accuracy { get { return (float)MolesHit / (float)TotalMoles; } }

	public float TransitionDuration { get; private set; }
	public float LifetimeDuration { get; private set; }
	public float WarningDuration { get; private set; }
	public float ParachuteDuration { get; private set; }

	public float EvilMoleLifetime { get { return ( TransitionDuration * 2.0f ) + LifetimeDuration; } }
	public float ProfessorMoleLifetime { get { return WarningDuration + EvilMoleLifetime; } }
	public float ParachuteMoleLifetime { get { return ParachuteDuration; } }

	[SerializeField] private Settings settings = null;
	[SerializeField] private List<Transform> moleHills = null;
	[SerializeField] private AudioClip whacked = null;
	[SerializeField] private AudioClip escaped = null;

	private int scorePerMole = 0;
	
	private float redHerring = 0.0f;
	private float parachuteChance = 0.0f;

	private float minDelay = 0.0f;
	private float maxDelay = 0.0f;

	private int lives = 0;
	private int hills = 0;
	private List<MoleHill> availableHills = new List<MoleHill>( );

	protected override IEnumerator GenerateLevel( )
	{
		yield return null;

		Victory = true;

		lives = TotalLives = Mathf.RoundToInt( settings.livesCurve.Evaluate( Difficulty ) );

		scorePerMole = Mathf.RoundToInt( settings.scoreCurve.Evaluate( Difficulty ) );
		
		TransitionDuration = settings.transitionDurationCurve.Evaluate( Difficulty );
		LifetimeDuration = settings.lifetimeCurve.Evaluate( Difficulty );
		WarningDuration = settings.warningCurve.Evaluate( Difficulty );
		ParachuteDuration = settings.parachuteTimeCurve.Evaluate( Difficulty );

		redHerring = settings.redHerringCurve.Evaluate( Difficulty );
		parachuteChance = settings.parachuteCurve.Evaluate( Difficulty );

		minDelay = settings.minDelayCurve.Evaluate( Difficulty );
		maxDelay = settings.maxDelayCurve.Evaluate( Difficulty );

		hills = Mathf.RoundToInt( settings.totalHillsCurve.Evaluate( Difficulty ) );

		yield return null;

		for ( int i = 0; i < hills; ++i )
		{
			var hillPrefab = worldGenerator.ActivePool.RandomResource( "hole", Generator );
			var hillInstance = Instantiate( hillPrefab ) as GameObject;
			hillInstance.transform.parent = moleHills[i];
			hillInstance.transform.localPosition = Vector3.zero;
			hillInstance.transform.localRotation = Quaternion.identity;
			hillInstance.transform.localScale = Vector3.one;

			var hill = hillInstance.GetComponent<MoleHill>( );
			hill.Setup( this );

			hill.OnMoleFocus += OnMoleFocus;
			hill.OnMoleUnFocus += OnMoleUnFocus;
			hill.OnMoleDeath += OnMoleKilled;

			availableHills.Add( hill );
		}

		yield return null;

		var parachuteThreshold = 1.0f - parachuteChance - redHerring;
		var redHerringThreshold = 1.0f - redHerring;

		var evilMoles = 0;
		var proffessorMoles = 0;
		var parachutingMoles = 0;

		var stopTime = EvilMoleLifetime;
		var time = Duration - Mathf.Lerp( 0.0f, minDelay, (float)Generator.NextDouble( ) );	// random offset to start
		while ( time > stopTime )
		{
			var type = MoleHill.MoleType.Evil;
			var random = Generator.NextDouble( );

			if ( random > redHerringThreshold )
			{
				if ( time > ProfessorMoleLifetime )	//	Only if we have time available
				{
					type = MoleHill.MoleType.Good;

					++proffessorMoles;
				}
			}
			else if ( random > parachuteThreshold )
			{
				if ( time > ParachuteDuration )	//	Only if we have time available
				{
					type = MoleHill.MoleType.Parachute;

					++parachutingMoles;
				}
			}
			else
			{
				++evilMoles;
			}

			SetupSpawn( type, Duration - time );

			var interval = Mathf.Lerp( minDelay, maxDelay, (float)Generator.NextDouble( ) );	// random offset between new moles
			time -= interval;
		}

		session.AddMetaData( "TotalMoles", evilMoles + parachutingMoles + proffessorMoles );
		session.AddMetaData( "TotalEvilMoles", evilMoles );
		session.AddMetaData( "TotalParachutingMoles", parachutingMoles );
		session.AddMetaData( "TotalProfessorMoles", proffessorMoles );

		yield break;
	}

	protected override void GameComplete( )
	{
	}

	private void OnMoleKilled( GTG.MoleBase mole, bool whacked )
	{
		if ( !Over )
		{
			var screenPosition = ScreenPosition( mole.transform.position ).Serialize( );

			if ( whacked )
			{
				Audio.PlaySFX( this.whacked );

				if ( mole.Type == MoleHill.MoleType.Good )
				{
					Victory = false;
					End( );

					session.AddEvent( "ProffessorWhacked", new
					{
						ScreenPosition = screenPosition,
						RelativeTime = mole.Age
					} );
				}
				else
				{
					Score += scorePerMole;
					++MolesHit;

					if ( OnMoleWhacked != null )
					{
						OnMoleWhacked( MolesHit );
					}

					session.AddEvent( "MoleWhacked", new
					{
						Type = mole.Type,
						ScreenPosition = screenPosition,
						RelativeTime = mole.Age
					} );
				}
			}
			else if ( mole.Type != MoleHill.MoleType.Good )
			{
				--Lives;
				Audio.PlaySFX( escaped );

				session.AddEvent( "MoleEscaped", new
				{
					Type = mole.Type,
					ScreenPosition = screenPosition,
					RelativeTime = mole.Age
				} );
			}
			else
			{
				session.AddEvent( "ProffessorSafe", new
				{
					ScreenPosition = screenPosition,
					RelativeTime = mole.Age
				} );
			}
		}
	}

	private void OnMoleFocus( GTG.MoleBase mole )
	{
		if ( OnCountdownMole != null )
		{
			OnCountdownMole( mole );
		}

		session.AddEvent( "MoleFocus", new
		{
			Type = mole.Type,
			ScreenPosition = ScreenPosition( mole.transform.position ).Serialize( ),
			RelativeTime = mole.Age
		} );
	}

	private void OnMoleUnFocus( GTG.MoleBase mole )
	{
		session.AddEvent( "MoleUnFocus", new
		{
			Type = mole.Type,
			ScreenPosition = ScreenPosition( mole.transform.position ).Serialize( ),
			RelativeTime = mole.Age
		} );
	}

	private void SetupSpawn( MoleHill.MoleType type, float time )
	{
		AddAlert( time, ( t ) =>
		{
			SpawnMole( type );
		} );
	}

	private void SpawnMole( MoleHill.MoleType type )
	{
		MoleHill hill = null;
		var offset = Generator.Next( availableHills.Count );	// Move this out to the main generation step?
		for ( int i = 0; i < availableHills.Count; ++i )
		{
			var index = ( i + offset ) % availableHills.Count;
			if ( !availableHills[index].Active )
			{
				hill = availableHills[index];
				hill.Spawn( type );

				if ( type != MoleHill.MoleType.Good )
				{
					++TotalMoles;
				}

				break;
			}
		}

		if ( hill == null )
		{
			Debug.LogError( "NO SPARE HILLS TO SPAWN!" );
		}
		else
		{
			session.AddEvent( "MoleSpawned", new
			{
				Type = type.ToString( ),
				ScreenPosition = ScreenPosition( hill.transform.position ).Serialize( )
			} );
		}
	}

	[ContextMenu( "Sort Hills" )]
	private void SortHills( )
	{
		var center = new Vector3( 0.5f, 0.2f, 0.0f );
		moleHills.Sort( ( lhs, rhs ) =>
			{
				var lhsViewport = gameCamera.WorldToViewportPoint( lhs.position ); lhsViewport.z = 0.0f;
				var rhsViewport = gameCamera.WorldToViewportPoint( rhs.position ); rhsViewport.z = 0.0f;
				var lhsDelta = lhsViewport - center;
				var rhsDelta = rhsViewport - center;
				var lhsSqrDist = lhsDelta.sqrMagnitude;
				var rhsSqrDist = rhsDelta.sqrMagnitude;

				return lhsSqrDist.CompareTo( rhsSqrDist );
			} );

		for ( int i = 0; i < moleHills.Count; ++i )
		{
			moleHills[i].name = i.ToString( );
		}
	}
}
