﻿
using System;
using UnityEngine;
using System.Collections;

public class MoleHill : NMN.Behaviour
{
	public event Action<GTG.MoleBase> OnMoleFocus;
	public event Action<GTG.MoleBase> OnMoleUnFocus;
	public event Action<GTG.MoleBase, bool> OnMoleDeath;

	public enum MoleType
	{
		Good,
		Evil,
		Parachute
	}

	public bool Active { get { return moleInstance != null; } }

	[SerializeField] private GameObject holeRoot = null;
	[SerializeField] private GameObject evilPrefab = null;
	[SerializeField] private GameObject goodPrefab = null;
	[SerializeField] private GameObject parachutePrefab = null;

	private GTG.MoleBase moleInstance = null;
	private MoleGame game = null;

	public void Setup( MoleGame game )
	{
		this.game = game;
	}

	public void Spawn( MoleType moleType )
	{
		GameObject prefab = null;

		switch ( moleType )
		{
			case MoleType.Good:
				{
					prefab = goodPrefab;
				}
				break;

			case MoleType.Evil:
				{
					prefab = evilPrefab;
				}
				break;

			case MoleType.Parachute:
				{
					prefab = parachutePrefab;
				}
				break;
		}

		if ( prefab != null )
		{
			var instance = NGUITools.AddChild( holeRoot, prefab );
			moleInstance = instance.GetComponent<GTG.MoleBase>( );
			moleInstance.Run( game );

			moleInstance.OnLookStart += ( mole ) =>
				{
					if ( OnMoleFocus != null )
					{
						OnMoleFocus( mole );
					}
				};

			moleInstance.OnLookStop += ( mole ) =>
				{
					if ( OnMoleUnFocus != null )
					{
						OnMoleUnFocus( mole );
					}
				};

			moleInstance.OnDeath += ( mole, whacked ) =>
				{
					if ( OnMoleDeath != null )
					{
						OnMoleDeath( mole, whacked );
					}
				};
		}
	}
}
