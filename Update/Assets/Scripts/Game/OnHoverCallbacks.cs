﻿
using System;
using UnityEngine;

namespace GTG
{
	public class OnHoverCallback : NMN.Behaviour
	{
		public event Action<OnHoverCallback> OnHoverEnter;
		public event Action<OnHoverCallback> OnHoverExit;

		public static OnHoverCallback Get( GameObject source )
		{
			return NMN.Tools.GetOrAddComponent<OnHoverCallback>( source );
		}

		private void OnHover( bool hover )
		{
			if ( hover )
			{
				if ( OnHoverEnter != null )
				{
					OnHoverEnter( this );
				}
			}
			else
			{
				if ( OnHoverExit != null )
				{
					OnHoverExit( this );
				}
			}
		}
	}
}
