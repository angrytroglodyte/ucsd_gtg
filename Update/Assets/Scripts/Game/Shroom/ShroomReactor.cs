﻿
using System;
using UnityEngine;

public class ShroomReactor : NMN.Behaviour
{
	public event Action OnPop;

	[SerializeField] private UITweener tween = null;
	[SerializeField] private GameObject explosion = null;

	public void OnTweenComplete( )
	{
		if ( tween.direction == AnimationOrTween.Direction.Forward )
		{
			if ( OnPop != null )
			{
				OnPop( );
			}

			var explosionInstance = Instantiate( explosion, transform.position, transform.rotation ) as GameObject;
			explosionInstance.transform.parent = transform.parent;

			Destroy( gameObject );
		}
	}

	private void OnHover( bool hover )
	{
		if ( hover )
		{
			tween.PlayForward( );
		}
		else
		{
			tween.PlayReverse( );
		}
	}
}
