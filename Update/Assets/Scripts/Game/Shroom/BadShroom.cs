﻿
using System;
using UnityEngine;

public class BadShroom : NMN.Behaviour
{
	public event Action<BadShroom> OnPop;

	[SerializeField]
	private ShroomReactor reactor = null;

	private void Awake( )
	{
		reactor.OnPop += ( ) =>
		{
			if ( OnPop != null )
			{
				OnPop( this );
			}
		};
	}
}

