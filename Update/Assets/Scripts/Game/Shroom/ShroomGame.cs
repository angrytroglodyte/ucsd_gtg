﻿
using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NMN.Extensions;

public class ShroomGame : GTG.BaseGame
{
	public event Action<float> OnBlackout;
	public event Action OnShroomPopped;

	[Serializable]
	private class Settings
	{
		public AnimationCurve scoreCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 100.0f );
		
		public AnimationCurve spawnAreaSizeCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 10.0f );
		public AnimationCurve spawnRingCountCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 5.0f );
		public AnimationCurve spokesPerRingCurve = AnimationCurve.Linear( 0.0f, 6.0f, 1.0f, 10.0f );

		public AnimationCurve blackoutDurationCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 2.0f );

		public AnimationCurve totalShroomsCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 10.0f );
		public AnimationCurve totalPosionousShroomsCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 10.0f );
	}

	public int ShroomsHit { get; private set; }
	public int TotalShrooms { get; private set; }
	public int ShroomsRemaining { get { return TotalShrooms - ShroomsHit; } }

	[SerializeField] private Transform spawnCenter = null;
	[SerializeField] private Settings settings = null;

	private int scorePerShroom = 0;
	private float spawnSize = 0.0f;
	private float blackoutDuration = 0.0f;
	private int totalPoisonousShrooms = 0;
	private int ringCount = 0;
	private int ringSpokeCount = 0;

	protected override IEnumerator GenerateLevel( )
	{
		yield return null;

		Victory = false;

		scorePerShroom = Mathf.RoundToInt( settings.scoreCurve.Evaluate( Difficulty ) );
		spawnSize = settings.spawnAreaSizeCurve.Evaluate( Difficulty );
		blackoutDuration = settings.blackoutDurationCurve.Evaluate( Difficulty );
		TotalShrooms = Mathf.RoundToInt( settings.totalShroomsCurve.Evaluate( Difficulty ) );
		totalPoisonousShrooms = Mathf.RoundToInt( settings.totalPosionousShroomsCurve.Evaluate( Difficulty ) );
		ringCount = Mathf.RoundToInt( settings.spawnRingCountCurve.Evaluate( Difficulty ) );
		ringSpokeCount = Mathf.RoundToInt( settings.spokesPerRingCurve.Evaluate( Difficulty ) );

		yield return null;

		var points = new List<Vector3>( );
		for ( int i = 0; i < ringCount; ++i )
		{
			var radius = (spawnSize * ( i + 1 )) / ringCount;
			var angleOffset = Mathf.Lerp( -180.0f, 180.0f, (float)Generator.NextDouble( ) );
			
			for ( int j = 0; j < ringSpokeCount; ++j )
			{
				var angle = Mathf.Lerp( 0.0f, 360.0f, (float)j / (float)ringSpokeCount );
				var position = ( Quaternion.Euler( 0.0f, angle + angleOffset, 0.0f ) * Vector3.forward ) * radius;

				points.Add( position );
			}
		}

		points = points.Shuffle( Generator ).ToList( );

		if ( points.Count < ( TotalShrooms + totalPoisonousShrooms ) )
		{
			Debug.LogError( "NOT ENOUGH POINTS AVAILABLE" );
		}

		yield return null;

		for ( int i = 0; i < TotalShrooms; ++i )
		{
			var instance = Spawn( "shrooms", points[i] );
			instance.GetComponent<GoodShroom>( ).OnPop += OnGoodPop;

			RegisterInterest( instance );
		}

		yield return null;

		for ( int i = TotalShrooms; i < TotalShrooms + totalPoisonousShrooms; ++i )
		{
			var instance = Spawn( "poisonous_shrooms", points[i] );
			instance.GetComponent<BadShroom>( ).OnPop += OnBadPop;

			RegisterInterest( instance );
		}

		yield return null;

		for ( int i = TotalShrooms + totalPoisonousShrooms; i < points.Count; ++i )
		{
			Spawn( "distractions", points[i] );
		}

		session.AddMetaData( "TotalDistractions", points.Count - ( TotalShrooms + totalPoisonousShrooms ) );

		yield break;
	}

	private GameObject Spawn( string category, Vector3 offset )
	{
		var prefab = worldGenerator.ActivePool.RandomResource( category, Generator );
		var instance = Instantiate( prefab ) as GameObject;
		instance.transform.parent = spawnCenter;
		instance.transform.position = spawnCenter.position + offset;
		instance.transform.localRotation = Quaternion.identity;
		instance.transform.localScale = prefab.transform.localScale;

		return instance;
	}

	private void OnGoodPop( GoodShroom shroom )
	{
		Score += scorePerShroom;
		++ShroomsHit;
		if ( OnShroomPopped != null )
		{
			OnShroomPopped( );
		}

		if ( ShroomsRemaining <= 0 )
		{
			Victory = true;
			End( );
		}

		session.AddEvent( "GoodShroomPopped", new
		{
			ScreenPosition = ScreenPosition( shroom.transform.position ).Serialize( ),
			RelativeTime = ElapsedTime
		} );
	}

	private void OnBadPop( BadShroom shroom )
	{
		if ( OnBlackout != null )
		{
			OnBlackout( blackoutDuration );
		}

		session.AddEvent( "BadShroomPopped", new
		{
			ScreenPosition = ScreenPosition( shroom.transform.position ).Serialize( ),
			RelativeTime = ElapsedTime
		} );
	}

	protected override void GameComplete( )
	{
	}

	private void OnDrawGizmos( )
	{
		if ( spawnCenter != null )
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere( spawnCenter.position, settings.spawnAreaSizeCurve.Evaluate( 0.0f ) );

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere( spawnCenter.position, settings.spawnAreaSizeCurve.Evaluate( 1.0f ) );
		}
	}
}
