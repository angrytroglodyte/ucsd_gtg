﻿
using System;
using UnityEngine;

public class GoodShroom : NMN.Behaviour
{
	public event Action<GoodShroom> OnPop;

	[SerializeField] private ShroomReactor reactor = null;

	private void Awake( )
	{
		reactor.OnPop += ( ) =>
			{
				if ( OnPop != null )
				{
					OnPop( this );
				}
			};
	}
}

