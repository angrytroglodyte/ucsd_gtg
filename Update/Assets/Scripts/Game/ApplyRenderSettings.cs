﻿
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ApplyRenderSettings : NMN.Behaviour
{
	[SerializeField] private Color ambientLight = Color.black;

	private void Awake( )
	{
		RenderSettings.ambientLight = ambientLight;
	}

#if UNITY_EDITOR
	[ContextMenu( "Cache Settings" )]
	private void CacheSettings( )
	{
		ambientLight = RenderSettings.ambientLight;
	}
#endif
}
