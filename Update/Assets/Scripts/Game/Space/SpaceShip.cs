﻿
using System;
using UnityEngine;

using NMN.Extensions;

public class SpaceShip : NMN.Behaviour
{
	public event Action OnGate;
	public event Action<GameObject> OnAsteroid;
	public event Action<GameObject> OnStar;

	[SerializeField] private tk2dBaseSprite sprite = null;
	[SerializeField] private Color damageTint = Color.red;
	[SerializeField] private AnimationCurve tintCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.0f );

	private bool running = false;
	private float speed = 0.0f;
	private float targetX = 0.0f;

	public void Setup( float speed )
	{
		this.speed = speed;
	}

	public void UpdateTarget( float targetX )
	{
		this.targetX = targetX;
	}

	public void Damage( )
	{
		NMN.Job.Make( tintCurve.Run( ( t ) =>
			{
				if ( sprite != null )
				{
					sprite.color = Color.Lerp( Color.white, damageTint, t );
				}
			} ) );
	}

	private void Update( )
	{
		if ( running )
		{
			var velocity = transform.up * speed;
			var targetPosition = transform.position + velocity;
			targetPosition.x = targetX;
			var direction = ( targetPosition - transform.position ).normalized;
			velocity = direction * speed;

			transform.position = transform.position + ( velocity * Time.deltaTime );
		}
	}

	private void Awake( )
	{
		CountdownTimer.OnCountdownStart += OnCountdownStart;
	}

	private void OnDestroy( )
	{
		CountdownTimer.OnCountdownStart -= OnCountdownStart;
	}

	private void OnCountdownStart( )
	{
		running = true;
	}

	private void OnTriggerEnter( Collider collider )
	{
		var tag = collider.tag;
		var gate = ( tag == "Gate" );
		var asteroid = ( tag == "Asteroid" );
		var star = ( tag == "Star" );

		if ( gate )
		{
			OnGate( );
		}
		else if ( star )
		{
			OnStar( collider.gameObject );
		}
		else if ( asteroid )
		{
			OnAsteroid( collider.gameObject );
		}
	}
}
