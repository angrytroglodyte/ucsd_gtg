﻿
using UnityEngine;

public class BackgroundScale : NMN.Behaviour
{
	[SerializeField] private tk2dSprite sprite = null;

	private void Start( )
	{
		var camera = tk2dCamera.Instance;
		var scale = sprite.scale;
		scale.x = camera.ScreenExtents.width / sprite.CurrentSprite.GetUntrimmedBounds( ).size.x;
		sprite.scale = scale;
	}
}
