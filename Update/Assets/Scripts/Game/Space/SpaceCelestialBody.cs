﻿
using UnityEngine;

public class SpaceCelestialBody : NMN.Behaviour
{
	[SerializeField] private tk2dBaseSprite sprite = null;

	private SpaceGame game = null;
	private new Transform transform = null;

	private float travelRange = 0.0f;
	private Vector3 initialPosition = Vector3.zero;
	private Vector3 finalPosition = Vector3.zero;

	private void OnGameReady( GTG.BaseGame game )
	{
		this.game = game as SpaceGame;
		transform = GetComponent<Transform>( );

		travelRange = this.game.ScrollMax;

		initialPosition = Vector3.up * this.game.SectionHeight;
		finalPosition = Vector3.down * sprite.GetUntrimmedBounds( ).size.y;
	}

	private void Update( )
	{
		if ( game != null )
		{
			var t = Mathf.Clamp01( game.ScrollAmount / travelRange );
			transform.localPosition = Vector3.Lerp( initialPosition, finalPosition, t );
		}
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
