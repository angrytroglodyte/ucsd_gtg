﻿
using UnityEngine;

public class SpaceBackground : NMN.Behaviour
{
	[SerializeField] private tk2dBaseSprite sprite = null;

	private SpaceGame game = null;
	private new Transform transform = null;
	private Vector3 initialPosition = Vector3.zero;
	private Vector3 finalPosition = Vector3.zero;

	private void OnGameReady( GTG.BaseGame game )
	{
		this.game = game as SpaceGame;
		transform = GetComponent<Transform>( );
		initialPosition = transform.localPosition;
		finalPosition = initialPosition - Vector3.up * ( sprite.GetUntrimmedBounds( ).size.y - this.game.SectionHeight );
	}

	private void Update( )
	{
		if ( game != null )
		{
			var t = game.ScrollAmount / game.ScrollMax;
			transform.localPosition = Vector3.Lerp( initialPosition, finalPosition, t );
		}
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
