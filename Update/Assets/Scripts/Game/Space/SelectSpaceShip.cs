﻿
using UnityEngine;

public class SelectSpaceShip : NMN.Behaviour
{
	[SerializeField] private GameObject selected = null;
	[SerializeField] private UITweener tween = null;

	public void Highlight( bool active )
	{
		NGUITools.SetActive( selected, active );
		tween.Play( active );
	}
}
