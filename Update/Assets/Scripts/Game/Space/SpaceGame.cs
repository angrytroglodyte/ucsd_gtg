﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceGame : GTG.BaseGame
{
	public event Action<int> OnLivesChanged;

	[Serializable]
	private class Settings
	{
		public AnimationCurve scoreCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 100.0f );
		public AnimationCurve starScoreCurve = AnimationCurve.Linear( 0.0f, 10.0f, 1.0f, 1000.0f );

		public AnimationCurve speedCurve = AnimationCurve.Linear( 0.0f, 10.0f, 1.0f, 100.0f );

		public AnimationCurve starChanceCurve = AnimationCurve.Linear( 0.0f, 0.1f, 1.0f, 0.1f );
		public AnimationCurve asteroidChanceCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 0.2f );

		public AnimationCurve livesCurve = AnimationCurve.Linear( 0.0f, 5.0f, 1.0f, 3.0f );

		public AnimationCurve gatesCurve = AnimationCurve.Linear( 0.0f, 5.0f, 1.0f, 50.0f );
		public AnimationCurve gateSizeCurve = AnimationCurve.Linear( 0.0f, 3.0f, 1.0f, 1.0f );
		public AnimationCurve gateOffsetCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 2.0f );

		public AnimationCurve availableShips = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 4.0f );
	}

	[SerializeField] private Settings settings = null;
	[SerializeField] private GameObject inputRoot = null;

	[SerializeField] private float sectionSeparation = 0.0f;
	[SerializeField] private float sectionExitPoint = 0.0f;
	[SerializeField] private GameObject worldRoot = null;
	[SerializeField] private GameObject playerRoot = null;

	[SerializeField] private float worldWidth = 0.0f;
	[SerializeField] private float scrollDuration = 0.0f;

	[SerializeField] private AudioClip goodGateAudio = null;
	[SerializeField] private AudioClip badGateAudio = null;
	[SerializeField] private GameObject explosion = null;
	[SerializeField] private GameObject collection = null;

	public int Lives
	{
		get { return lives; }
		private set
		{
			lives = value;
			if ( OnLivesChanged != null )
			{
				OnLivesChanged( lives );
			}

			if ( lives <= 0 )
			{
				Victory = false;
				End( );
			}
		}
	}
	public int TotalLives { get; private set; }
	public bool Scrolling { get { return scrollJob != null; } }
	public int Section { get; private set; }
	public int AvailableShips { get; private set; }

	public override float Progress { get { return ship == null ? 0.0f : ( ship.transform.localPosition.y / topOfWorld ); } }

	public float SectionHeight { get { return sectionSeparation; } }
	public float ScrollMax { get { return topOfWorld - SectionHeight; } }
	public float ScrollAmount { get { return -worldRoot.transform.position.y; } }

	private float scorePerMeter = 0.0f;
	private int scorePerStar = 0;
	private float speed = 0.0f;
	private float starChance = 0.0f;
	private float asteroidChance = 0.0f;
	private int gates = 0;
	private int gateSize = 0;
	private int gateOffset = 0;
	private int lives = 0;

	private float targetX = 0.0f;
	private float topOfWorld = 0.0f;
	private int section = 0;
	private List<SpaceSection> sections = new List<SpaceSection>( );

	private SpaceShip ship = null;
	private NMN.Job scrollJob = null;
	private float pointCounter = 0.0f;
	private float lastScrollStopTime = 0.0f;

	public void SpawnShip( int index )
	{
		var shipPrefab = worldGenerator.ActivePool.SpecificResource( "ship", index );
		var shipInstance = Instantiate( shipPrefab, playerRoot.transform.position, Quaternion.identity ) as GameObject;
		shipInstance.transform.parent = worldRoot.transform;

		ship = shipInstance.GetComponent<SpaceShip>( );
		ship.Setup( speed );

		ship.OnGate += OnGate;
		ship.OnAsteroid += OnAsteroid;
		ship.OnStar += OnStar;

		session.AddMetaData( "ShipSelected", index );
	}

	protected override IEnumerator GenerateLevel( )
	{
		yield return null;

		Victory = true;

		scorePerMeter = settings.scoreCurve.Evaluate( Difficulty );
		scorePerStar = Mathf.RoundToInt( settings.starScoreCurve.Evaluate( Difficulty ) );

		speed = settings.speedCurve.Evaluate( Difficulty );
		starChance = settings.starScoreCurve.Evaluate( Difficulty );
		asteroidChance = settings.asteroidChanceCurve.Evaluate( Difficulty );
		gates = Mathf.RoundToInt( settings.gatesCurve.Evaluate( Difficulty ) );
		gateSize = Mathf.RoundToInt( settings.gateSizeCurve.Evaluate( Difficulty ) );
		gateOffset = Mathf.RoundToInt( settings.gateOffsetCurve.Evaluate( Difficulty ) );

		lives = TotalLives = Mathf.RoundToInt( settings.livesCurve.Evaluate( Difficulty ) );

		var currentCompletion = difficultyCurve.Evaluate( (float)save.LastCompleteLevel );
		AvailableShips = Mathf.RoundToInt( settings.availableShips.Evaluate( currentCompletion ) );

		topOfWorld = ( gates + 1 ) * sectionSeparation;

		yield return null;

		var startPrefab = worldGenerator.ActivePool.RandomResource( "start_section", Generator );
		var startInstance = Instantiate( startPrefab ) as GameObject;
		startInstance.transform.parent = worldRoot.transform;
		var startSection = startInstance.GetComponent<SpaceSection>( );
		sections.Add( startSection );

		yield return null;

		var starCount = 0;
		var asteroidCount = 0;

		var starSections = new List<int>( );
		var asteroidSections = new List<int>( );
		for ( int i = 0; i < gates; ++i )
		{
			var offset = Vector3.up * ( ( i + 1 ) * sectionSeparation );

			var sectionPrefab = worldGenerator.ActivePool.RandomResource( "sections", Generator );
			var sectionInstance = Instantiate( sectionPrefab, offset, Quaternion.identity ) as GameObject;
			sectionInstance.transform.parent = worldRoot.transform;

			var star = Generator.NextDouble( ) < starChance;
			var asteroid = Generator.NextDouble( ) < asteroidChance;

			starCount += star ? 1 : 0;
			asteroidCount += asteroid ? 1 : 0;

			var section = sectionInstance.GetComponent<SpaceSection>( );
			section.Setup( worldGenerator.ActivePool, gateSize, gateOffset, star, asteroid, Generator );
			sections.Add( section );

			if ( star )
			{
				starSections.Add( i );
				RegisterInterest( section.Star );
			}

			if ( asteroid )
			{
				asteroidSections.Add( i );
				RegisterInterest( section.Asteroid );
			}
		}

		session.AddMetaData( "StarSections", starSections );
		session.AddMetaData( "AsteroidSections", asteroidSections );
		session.AddMetaData( "TotalStars", starCount );
		session.AddMetaData( "TotalAsteroids", asteroidCount );

		lastScrollStopTime = Time.time;

		yield break;
	}

	protected override void GameComplete( )
	{
	}

	private void OnGate( )
	{
		Debug.DrawLine( ship.transform.position, ship.transform.position + Vector3.up, Color.white, 10.0f );

		var data = new
		{
			//	I don't actually use individual gates, I use a width which covers all gates and a single collider
			//	This gets us the position of the closest gate to us.
			ScreenPosition = ScreenPosition( sections[section].NearestGateCenter( ship.transform.position.x ) ).Serialize( ),
			RelativeTime = Time.time - lastScrollStopTime
		};

		if ( !sections[section].ValidPosition( ship.transform.position.x ) )
		{
			ship.Damage( );

			--Lives;
			Audio.PlaySFX( badGateAudio );

			session.AddEvent( "RedGateHit", data );
		}
		else
		{
			Audio.PlaySFX( goodGateAudio );

			session.AddEvent( "GreenGateHit", data );
		}
	}

	private void OnAsteroid( GameObject asteroid )
	{
		Lives = 0;
		Explode( );

		session.AddEvent( "AsteroidHit", new
			{
				ScreenPosition = ScreenPosition( asteroid.transform.position ).Serialize( ),
				RelativeTime = Time.time - lastScrollStopTime
			} );
	}

	private void Explode( )
	{
		var explosionInstance = Instantiate( explosion, ship.transform.position, ship.transform.rotation ) as GameObject;
		explosionInstance.transform.parent = transform;

		Destroy( ship.gameObject );
	}

	private void OnStar( GameObject star )
	{
		var collectionInstance = Instantiate( collection, star.transform.position, star.transform.rotation ) as GameObject;
		collectionInstance.transform.parent = transform;

		Destroy( star );
		Score += scorePerStar;

		session.AddEvent( "StarCollected", new
			{
				ScreenPosition = ScreenPosition( star.transform.position ).Serialize( ),
				RelativeTime = Time.time - lastScrollStopTime
			} );
	}

	private void Update( )
	{
		if ( Running && ship != null )
		{
			if ( ETCamera.Target == inputRoot )
			{
				targetX = Mathf.Clamp( ETCamera.LastHitPosition.x, -worldWidth, worldWidth );
				targetX = sections[section].CenterOnTrack( targetX );
			}

			ship.UpdateTarget( targetX );
			if ( !Scrolling && ship.transform.position.y > sectionExitPoint )
			{
				NextSection( );
			}

			pointCounter += speed * scorePerMeter * Time.deltaTime;
			while ( pointCounter > 1.0f )
			{
				++Score;
				pointCounter -= 1.0f;
			}
		}
	}

	private void NextSection( )
	{
		++section;

		if ( section > gates )
		{
			End( );
		}
		else
		{
			scrollJob = NMN.Job.Make( NextSectionImpl( ) );
			scrollJob.OnComplete += ( cancelled ) =>
				{
					scrollJob = null;
					lastScrollStopTime = Time.time;
				};
		}
	}

	private IEnumerator NextSectionImpl( )
	{
		yield return null;

		var current = worldRoot.transform.position;
		var target = worldRoot.transform.position - ( Vector3.up * sectionSeparation );

		for ( float time = 0.0f; time < scrollDuration; time += Time.deltaTime )
		{
			var t = NMN.Math.SmoothStep( time / scrollDuration );
			worldRoot.transform.position = Vector3.Lerp( current, target, t );
			yield return null;
		}

		worldRoot.transform.position = target;

		yield break;
	}

	private void OnDrawGizmos( )
	{
		Gizmos.DrawLine(	worldRoot.transform.position + Vector3.up + ( Vector3.right * worldWidth ),
							worldRoot.transform.position + Vector3.up - ( Vector3.right * worldWidth ) );

		Gizmos.DrawLine(	worldRoot.transform.position + ( Vector3.up * sectionExitPoint ) + ( Vector3.right * worldWidth ),
							worldRoot.transform.position + ( Vector3.up * sectionExitPoint ) - ( Vector3.right * worldWidth ) );

		if ( ship != null && section >= 0 && section < sections.Count )
		{
			sections[section].DrawValidBounds( ship.transform.position.x );
		}
	}
}
