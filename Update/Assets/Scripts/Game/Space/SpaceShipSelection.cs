﻿
using UnityEngine;

public class SpaceShipSelection : NMN.Behaviour
{
	[SerializeField] private GameObject instructions = null;
	[SerializeField] private GameObject shipSelection = null;

	[SerializeField] private Vector2 shipsRange;
	[SerializeField] private float shipOffset;
	[SerializeField] private SelectSpaceShip[] ships = null;

	[SerializeField] private float transitionTime = 0.5f;

	private int current = 0;
	private float offset = 0.0f;
	private float velocity = 0.0f;

	private SpaceGame game = null;

	public void Show( )
	{
		NGUITools.SetActive( instructions, false );
		NGUITools.SetActive( shipSelection, true );

		game = GTG.BaseGame.ActiveGame as SpaceGame;
	}

	public void CycleLeft( ) { Cycle( -1 ); }
	public void CycleRight( ) { Cycle( 1 ); }

	public void Spawn( )
	{
		if ( current < game.AvailableShips )
		{
			game.SpawnShip( current );
		}
	}

	private void Cycle( int delta )
	{
		ships[current].Highlight( false );

		current += delta;
		current = NMN.Math.Wrap( current, 0, ships.Length );

		offset += delta * shipOffset;

		ships[current].Highlight( current < game.AvailableShips );
	}

	private void Update( )
	{
		var oldOffset = offset;
		offset = Mathf.SmoothDamp( offset, 0.0f, ref velocity, transitionTime );
		var deltaOffset = offset - oldOffset;

		for ( int i = 0; i < ships.Length; ++i )
		{
			var ship = ships[i];

			var position = ship.transform.localPosition;
			position.x = NMN.Math.Wrap( position.x + deltaOffset, shipsRange.x, shipsRange.y );
			ship.transform.localPosition = position;
		}
	}
}
