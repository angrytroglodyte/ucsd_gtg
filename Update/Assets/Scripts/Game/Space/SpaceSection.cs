﻿
using UnityEngine;

using Random = System.Random;

public class SpaceSection : NMN.Behaviour
{
	public GameObject Star { get; private set; }
	public GameObject Asteroid { get; private set; }

	[SerializeField] private Transform[] gates = null;
	[SerializeField] private float gateWidth = 1.0f;
	[SerializeField] private Vector3 minSpawnBounds = Vector3.zero;
	[SerializeField] private Vector3 maxSpawnBounds = Vector3.zero;

	private float start = 0.0f;
	private float end = 0.0f;

	public void Setup( ResourcePool pool, int gateSize, int gateOffset, bool star, bool asteroid, Random generator )
	{
		var offset = generator.Next( -gateOffset, gateOffset + 1 );
		var start = Mathf.Clamp( (gates.Length / 2) + offset, 0, gates.Length - gateSize );
		var end = start + gateSize;

		for ( int i = 0; i < gates.Length; ++i )
		{
			var prefab = ( i >= start && i < end ) ? pool.RandomResource( "good_gate", generator ) : pool.RandomResource( "bad_gate", generator );
			var instance = Instantiate( prefab, gates[i].position, Quaternion.identity ) as GameObject;
			instance.transform.parent = gates[i];
		}

		this.start = gates[start].position.x - ( gateWidth * 0.5f );
		this.end = this.start + ( gateSize * gateWidth );

		if ( star )
		{
			var position = Vector3.zero;
			position.x = CenterOnTrack( Mathf.Lerp( minSpawnBounds.x, maxSpawnBounds.x, (float)generator.NextDouble( ) ) );
			position.y = Mathf.Lerp( minSpawnBounds.y, maxSpawnBounds.y, (float)generator.NextDouble( ) );

			var prefab = pool.RandomResource( "star", generator );
			var instance = Instantiate( prefab ) as GameObject;
			instance.transform.parent = transform;
			instance.transform.localPosition = position;

			Star = instance;
		}

		if ( asteroid )
		{
			var position = Vector3.zero;
			position.x = CenterOnTrack( Mathf.Lerp( minSpawnBounds.x, maxSpawnBounds.x, (float)generator.NextDouble( ) ) );
			position.y = Mathf.Lerp( minSpawnBounds.y, maxSpawnBounds.y, (float)generator.NextDouble( ) );

			var prefab = pool.RandomResource( "asteroid", generator );
			var instance = Instantiate( prefab ) as GameObject;
			instance.transform.parent = transform;
			instance.transform.localPosition = position;

			Asteroid = instance;
		}
	}

	public bool ValidPosition( float currentX )
	{
		return ( currentX >= start ) && ( currentX <= end );
	}

	public float CenterOnTrack( float targetX )
	{
		var left = gates[0].position.x;
		var offset = targetX - left;
		var snapped = gateWidth * Mathf.Round( offset / gateWidth );

		return left + snapped;
	}

	public Vector3 NearestGateCenter( float currentX )
	{
		var center = CenterOnTrack( currentX );
		var collider = GetComponent<BoxCollider>( );
		return collider.center + ( Vector3.right * center );
	}

	private void OnDrawGizmos( )
	{
		var collider = GetComponent<BoxCollider>( );
		if ( collider != null )
		{
			Gizmos.matrix = transform.localToWorldMatrix;
			Gizmos.color = Color.red;

			Gizmos.DrawSphere( collider.center + ( Vector3.right * start ), 0.1f );
			Gizmos.DrawSphere( collider.center + ( Vector3.right * end ), 0.1f );
			Gizmos.DrawLine( collider.center + ( Vector3.right * start ), collider.center + ( Vector3.right * end ) );

			Gizmos.DrawWireCube( Vector3.Lerp( minSpawnBounds, maxSpawnBounds, 0.5f ), maxSpawnBounds - minSpawnBounds );
		}
	}

	public void DrawValidBounds( float currentX )
	{
		Gizmos.color = Color.yellow;

		var center = new Vector3( Mathf.Lerp( start, end, 0.5f ), 5.0f, 0.1f );
		var size = new Vector3( end - start, 10.0f, 0.1f );

		Gizmos.DrawWireCube( center, size );

		Gizmos.color = ValidPosition( currentX ) ? Color.white : Color.red;
		Gizmos.matrix = transform.localToWorldMatrix;

		var collider = GetComponent<BoxCollider>( );
		if ( collider != null )
		{
			Gizmos.DrawSphere( collider.center + ( Vector3.right * currentX ), 0.1f );
		}
	}
}
