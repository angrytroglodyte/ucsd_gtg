﻿
using UnityEngine;

public class FlickerOffset : NMN.Behaviour
{
	private void Awake( )
	{
		var material = renderer.material;
		var current = material.GetVector( "_FlickerParams" );
		current.x = current.y = Random.value;
		material.SetVector( "_FlickerParams", current );

		enabled = false;
	}
}
