﻿
using System;
using UnityEngine;
using System.Collections.Generic;

using Random = System.Random;

public class ResourcePool : NMN.Behaviour
{
	[System.Serializable]
	public class Category
	{
		public string key = string.Empty;
		public GameObject[] options = null;
	}

	[SerializeField] private Category[] categories = null;

	private Dictionary<string, Category> lookup = new Dictionary<string, Category>( );

	public GameObject RandomResource( string key, Random generator )
	{
		Category category = null;
		if ( !lookup.TryGetValue( key, out category ) )
		{
			return null;
		}

		//	That's so Random
		var r0 = generator.NextDouble( );
		var r1 = generator.NextDouble( );
		var uniform = r0 + r1;
		var r = ( uniform > 1.0 ) ? ( 2.0 - uniform ) : uniform;
		var index = (int)Math.Floor( r * category.options.Length );

		return category.options[index];
	}

	public GameObject SpecificResource( string key, int index )
	{
		Category category = null;
		if ( !lookup.TryGetValue( key, out category ) )
		{
			return null;
		}

		return category.options[index];
	}

	private void Awake( )
	{
		foreach ( var category in categories )
		{
			lookup.Add( category.key, category );
		}
	}
}
