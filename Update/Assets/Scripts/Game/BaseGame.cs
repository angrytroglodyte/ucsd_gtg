﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

namespace GTG
{
	public abstract class BaseGame : NMN.Behaviour
	{
		public static event Action<BaseGame> OnGameReady;

		public event Action OnGameStart;
		public event Action OnGameComplete;
		public event Action OnShutDown;

		private static Action<BaseGame> OnLoadedCallback;

		public static void LoadGame( Game game, int level, Action<BaseGame> onComplete )
		{
			OnLoadedCallback = ( self ) =>
				{
					self.Generate( game, GTG.GameSave.ClampLevel( level ) );

					if ( onComplete != null )
					{
						onComplete( self );
					}
				};

			//NMN.SceneLoader.LoadAdditive( game.SceneName, ( root ) =>
			//	{
			//		// For some reason this isn't working anymore... =[

			//		var instance = root.GetComponent<BaseGame>( );
			//		instance.Generate( game, level );

			//		if ( onComplete != null )
			//		{
			//			onComplete( instance );
			//		}
			//	} );

			NMN.SceneLoader.LoadAdditive( game.SceneName );
			NMUILoader.LoadUI( game.SceneName + "_hud" );
		}

		public static GTG.BaseGame ActiveGame { get { return NMN.Tools.FindObjectOfType<GTG.BaseGame>( ); } }

		public GTG.Game Data { get; private set; }
		public int Level { get; private set; }

		public float Difficulty { get; private set; }
		public Random Generator { get; private set; }
		public float Duration { get; private set; }

		public bool Started { get; private set; }
		public bool Over { get; private set; }
		public bool Running { get { return Started && !Over; } }

		public float StartTime { get; private set; }
		public float ElapsedTime { get { return Started ? ( Time.time - StartTime ) : 0.0f; } }

		public virtual float Progress { get { return ElapsedTime / Duration; } }
		public bool Victory { get; protected set; }

		public int Score
		{
			get { return score; }
			protected set
			{
				var old = score;
				score = value;
				var delta = score - old;
				User.AwardPoints( delta );
			}
		}

		[SerializeField] protected WorldGenerator worldGenerator = null;
		[SerializeField] protected bool timedGame = true;
		[SerializeField] protected AnimationCurve difficultyCurve = AnimationCurve.EaseInOut( 0.0f, 0.0f, 50.0f, 1.0f );	//	Default curve, maxes out difficulty at level 50
		[SerializeField] protected AnimationCurve durationCurve = AnimationCurve.Linear( 0.0f, 30.0f, 1.0f, 30.0f );
		[SerializeField] protected AudioClip startGameSFX = null;
		[SerializeField] protected Camera gameCamera = null;

		protected GTG.Session session = null;
		protected GTG.GameSave save = null;

		private int score = 0;
		private List<KeyValuePair<float, Action>> alerts = new List<KeyValuePair<float, Action>>( );
		private List<NMN.Job> tasks = new List<NMN.Job>( );

		public void ShutDown( Action onShutdown )
		{
			if ( OnShutDown != null )
			{
				OnShutDown( );
			}

			CleanUp( );
			session.Dispose( );
			NMN.Job.Make( NMN.Tools.ConfirmDestroy( gameObject, onShutdown ) );
		}

		public Vector3 ScreenPosition( Vector3 worldPosition )
		{
			return gameCamera.WorldToScreenPoint( worldPosition );
		}

		protected void RegisterInterest( GameObject interest )
		{
			var onHover = GTG.OnHoverCallback.Get( interest );
			onHover.OnHoverEnter += ( hover ) =>
				{
					session.AddEvent( "EyeEnterObject", new
					{
						ScreenPosition = ScreenPosition( hover.transform.position ).Serialize( ),
						Tag = hover.tag
					} );
				};

			onHover.OnHoverExit += ( hover ) =>
				{
					session.AddEvent( "EyeExitObject", new
					{
						ScreenPosition = ScreenPosition( hover.transform.position ).Serialize( ),
						Tag = hover.tag
					} );
				};
		}

		protected abstract IEnumerator GenerateLevel( );
		protected abstract void GameComplete( );

		private void Awake( )
		{
			Started = false;
			StartTime = 0.0f;

			OnLoadedCallback( this );
			OnLoadedCallback = null;
		}

		private void Generate( Game game, int level )
		{
			Debug.Log( game.DisplayName + " : " + level );
			session = new Session( game.SceneName, level );

			Data = game;
			Level = level;
			save = GTG.GameSave.Load( Data.SceneName );

			Generator = new Random( level );
			Difficulty = difficultyCurve.Evaluate( (float)level );
			Duration = durationCurve.Evaluate( Difficulty );

			var job = NMN.Job.Make( worldGenerator.Generate( level, Generator ) );
			job.AddChild( GenerateLevel( ) );
			job.OnComplete += ( killed ) =>
				{
					if ( OnGameReady != null )
					{
						OnGameReady( this );
					}

					InstructionsPopup.Load( game, level, Run );
				};
		}

		private void Save( )
		{
			if ( Victory )
			{
				save.CompleteLevel( Level );
				save.Save( );
			}
		}

		protected void AddAlert( float t, Action<float> alert )
		{
			alerts.Add( new KeyValuePair<float, Action>( t, ( ) =>
				{
					alert( t );
				} ) );
		}

		private void Run( )
		{
			foreach ( var alert in alerts )
			{
				tasks.Add( Invoke( alert.Value, alert.Key ) );
			}

			Started = true;
			StartTime = Time.time;

			if ( timedGame )
			{
				tasks.Add( Invoke( End, Duration ) );
			}

			if ( OnGameStart != null )
			{
				OnGameStart( );
			}

			Audio.PlaySFX( startGameSFX );
			session.Start( );
		}

		private void CleanUp( )
		{
			foreach ( var task in tasks )
			{
				task.Kill( );
			}

			tasks.Clear( );
		}

		protected void End( )
		{
			if ( !Over )
			{
				CleanUp( );

				Over = true;
				Save( );
				GameComplete( );

				session.AddEvent( "End" );
				session.AddMetaData( "Score", Score );

				Invoke( ( ) =>
					{
						if ( OnGameComplete != null )
						{
							OnGameComplete( );
						}
					}, 1.5f );
			}
		}
	}
}
