﻿
using System;
using UnityEngine;
using System.Collections;

using Random = System.Random;

public class WorldGenerator : NMN.Behaviour
{
	public ResourcePool ActivePool { get; private set; }

	[SerializeField] private string[] resourcePools = null;
	[SerializeField] private GTG.BaseWorldNode worldRoot = null;
	[SerializeField] private int cycleRate = 3;

	public IEnumerator Generate( int level, Random generator )
	{
		yield return null;

		var poolID = ( level / cycleRate ) % resourcePools.Length;
		var poolPath = resourcePools[poolID];
		var poolPrefab = Resources.Load<GameObject>( poolPath );
		var poolInstance = NGUITools.AddChild( gameObject, poolPrefab );

		yield return null;

		ActivePool = poolInstance.GetComponent<ResourcePool>( );

		var job = NMN.Job.MakeNull( false );
		worldRoot.BuildTask( job, ActivePool, generator );

		job.Start( );

		while ( job.Running )
		{
			yield return null;
		}

		yield break;
	}
}
