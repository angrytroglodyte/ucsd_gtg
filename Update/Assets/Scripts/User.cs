﻿
using System;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class User : NMN.Singleton<User>
{
	public static event Action OnLoggedIn;

	public static event Action<int, int> OnPointsChanged;

	public static int Points { get { return Instance.data.points; } }
	public static string Username { get { return Instance.username; } }
	public static bool UseMouse { get { return !EyeTribeClient.IsActive || Instance.data.mouse; } set { Instance.data.mouse = value; Save( ); } }

	public static void Setup( string username )
	{
		Instance.username = username;
		Load( );

		if ( OnLoggedIn != null )
		{
			OnLoggedIn( );
		}
	}

	public static void Load( )
	{
#if !UNITY_WEBPLAYER
		var path = FilePath( );
		if ( File.Exists( path ) )
#else
		if ( PlayerPrefs.HasKey( username + "_save" ) )
#endif
		{
#if !UNITY_WEBPLAYER
			var data = File.ReadAllText( path );
#else
			var data = PlayerPrefs.GetString( username + "_save" );
#endif

			Instance.data = JsonConvert.DeserializeObject<Data>( data );
		}
		else
		{
			Instance.data = new Data( );
		}
	}

	public static void Save( )
	{
		var path = FilePath( );
		var data = JsonConvert.SerializeObject( Instance.data );
		Directory.CreateDirectory( Path.GetDirectoryName( path ) );

#if !UNITY_WEBPLAYER
		File.WriteAllText( path, data );
#else
		PlayerPrefs.SetString( username + "_save", data );
#endif
	}

	public static void AwardPoints( int points )
	{
		points = Mathf.Max( 0, points );
		Instance.data.points += points;
		Save( );

		if ( OnPointsChanged != null )
		{
			OnPointsChanged( Instance.data.points, points );
		}
	}

	private static string FilePath( )
	{
		var root = Path.Combine( Application.persistentDataPath, "Data" );
		return Path.Combine( root, Instance.username + ".dat" );
	}

	[Serializable]
	private class Data
	{
		public int points = 0;
		public bool mouse = false;
	}

	private Data data = null;
	private string username = string.Empty;

	[ContextMenu( "Test Award" )]
	public void TestAward( )
	{
		AwardPoints( UnityEngine.Random.Range( 10, 100 ) );
	}

	protected override void OnApplicationQuit( )
	{
		Save( );

		base.OnApplicationQuit( );
	}
}
