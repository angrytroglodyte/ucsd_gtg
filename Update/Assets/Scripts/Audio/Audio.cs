﻿
using System.IO;
using UnityEngine;

using Newtonsoft.Json;

[RequireComponent(typeof(AudioListener))]
public class Audio : NMN.Singleton<Audio>
{
	[JsonObject( MemberSerialization.OptIn )]
	public class Category
	{
		private static string FilePath( string category )
		{
			var root = Path.Combine( Application.persistentDataPath, "Data" );
			root = Path.Combine( root, "Audio" );
			return Path.Combine( root, category + ".dat" );
		}

		public float Volume
		{
			get { return volume; }
			set
			{
				if ( volume != value )
				{
					volume = value;
					Save( );
				}
			}
		}

		[JsonProperty] private string name = string.Empty;
		[JsonProperty] private float volume = 1.0f;

		private void Save( )
		{
			var path = FilePath( name );
			var data = JsonConvert.SerializeObject( this );
			Directory.CreateDirectory( Path.GetDirectoryName( path ) );

#if !UNITY_WEBPLAYER
			File.WriteAllText( path, data );
#else
			PlayerPrefs.SetString( name + "_save", data );
#endif
		}

		public static Category FromFile( string category )
		{
#if !UNITY_WEBPLAYER
			var path = FilePath( category );
			if ( File.Exists( path ) )
#else
			if ( PlayerPrefs.HasKey( category + "_save" ) )
#endif
			{
#if !UNITY_WEBPLAYER
				var data = File.ReadAllText( path );
#else
				var data = PlayerPrefs.GetString( category + "_save" );
#endif

				return JsonConvert.DeserializeObject<Category>( data );
			}
			else
			{
				var defaultCategory = new Category( );
				defaultCategory.name = category;
				return defaultCategory;
			}
		}
	}

	public static void PlayMusic( AudioClip clip )
	{
		Instance.nextMusic = clip;

		if ( Instance.latestMusic != null )
		{
			if ( !Instance.latestMusic.Stopping && Instance.latestMusic.Clip != Instance.nextMusic )
			{
				Instance.latestMusic.StopMusic( ( player ) =>
					{
						Destroy( player.gameObject );
						Instance.PlayMusicImpl( Instance.nextMusic );
					} );
			}
		}
		else if ( clip != null )
		{
			Instance.PlayMusicImpl( Instance.nextMusic );
		}
	}

	public static void PlaySFX( AudioClip clip )
	{
		GTG.AudioPlayer.SFX( Instance.sfx, clip, ( player ) =>
			{
				Destroy( player.gameObject );
			} );
	}

	public static Category Music { get { return Instance.music; } }
	public static Category SFX { get { return Instance.sfx; } }

	private Category music = null;
	private Category sfx = null;

	private GTG.AudioPlayer latestMusic = null;
	private AudioClip nextMusic = null;

	private void PlayMusicImpl( AudioClip clip )
	{
		if ( clip != null )
		{
			latestMusic = GTG.AudioPlayer.Music( music, clip );
		}
		else
		{
			latestMusic = null;
		}
	}

	protected override void OnSpawned( )
	{
		base.OnSpawned( );

		music = Category.FromFile( "music" );
		sfx = Category.FromFile( "sfx" );
	}
}
