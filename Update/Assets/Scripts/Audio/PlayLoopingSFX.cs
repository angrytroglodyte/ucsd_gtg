﻿
using UnityEngine;

public class PlayLoopingSFX : NMN.Behaviour
{
	[SerializeField] private AudioClip clip = null;

	private GTG.AudioPlayer player = null;

	private void Awake( )
	{
		player = GTG.AudioPlayer.Make( Audio.SFX, clip );
		player.Play( 0.0f, true );
	}

	private void OnDestroy( )
	{
		Destroy( player.gameObject );
	}

	private void OnEnable( )
	{
		player.Volume = 1.0f;
	}

	private void OnDisable( )
	{
		player.Volume = 0.0f;
	}
}

