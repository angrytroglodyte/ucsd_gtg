﻿
using UnityEngine;

public class SFXOnClick : NMN.Behaviour
{
	[SerializeField] private AudioClip clip = null;

	private void OnClick( )
	{
		Audio.PlaySFX( clip );
	}
}
