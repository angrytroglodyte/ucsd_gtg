﻿
using System;
using UnityEngine;
using System.Collections;

namespace GTG
{
	public class AudioPlayer : NMN.Behaviour
	{
		public static AudioPlayer Make( Audio.Category category, AudioClip clip )
		{
			var holder = new GameObject( clip.name, typeof( AudioSource ), typeof( AudioPlayer ) );
			var source = holder.GetComponent<AudioSource>( );
			var player = holder.GetComponent<AudioPlayer>( );

			player.category = category;
			player.clip = clip;
			player.source = source;

			holder.transform.parent = Audio.Instance.transform;

			return player;
		}

		public static AudioPlayer Music( Audio.Category category, AudioClip clip )
		{
			var player = Make( category, clip );
			player.PlayMusic( );
			return player;
		}

		public static AudioPlayer SFX( Audio.Category category, AudioClip clip, Action<AudioPlayer> onComplete )
		{
			var player = Make( category, clip );
			player.PlaySFX( onComplete );
			return player;
		}

		public AudioClip Clip { get { return clip; } }
		public bool Playing { get { return playing; } }
		public bool Stopping { get { return stopping; } }
		public float Volume { get { return volume; } set { volume = value; } }

		private AudioSource source = null;
		private AudioClip clip = null;
		private Audio.Category category = null;

		private float volume = 1.0f;
		private NMN.Job task = null;
		
		private bool playing = false;
		private bool stopping = false;

		public void PlayMusic( Action<AudioPlayer> fadeComplete = null )
		{
			Play( 0.0f, true );
			Fade( 1.0f, fadeComplete );
		}

		public void StopMusic( Action<AudioPlayer> fadeComplete = null )
		{
			playing = false;
			stopping = true;

			Fade( 0.0f, fadeComplete );
		}

		public void PlaySFX( Action<AudioPlayer> onComplete )
		{
			Play( 1.0f, false );

			if ( onComplete != null )
			{
				task = Invoke( ( ) =>
					{
						onComplete( this );
					}, clip.length + 1.0f );
			}
		}

		public void Play( float volume, bool loop )
		{
			this.playing = true;
			this.volume = volume;
			this.source.clip = clip;
			this.source.loop = loop;
			this.source.volume = volume * category.Volume;
			this.source.Play( );
		}

		private void Fade( float to, Action<AudioPlayer> onComplete )
		{
			if ( task != null )
			{
				task.Kill( );
			}

			task = NMN.Job.Make( FadeVolume( volume, to, 1.0f ) );

			if ( onComplete != null )
			{
				task.OnComplete += ( cancelled ) =>
				{
					onComplete( this );
				};
			}
		}

		private void Update( )
		{
			source.volume = volume * category.Volume;
		}

		private IEnumerator FadeVolume( float from, float to, float duration )
		{
			yield return null;

			volume = from;
			for ( var time = 0.0f; time < duration; time += Time.deltaTime )
			{
				var t = time / duration;
				volume = Mathf.Lerp( from, to, t );
				yield return null;
			}

			volume = to;

			yield break;
		}
	}
}
