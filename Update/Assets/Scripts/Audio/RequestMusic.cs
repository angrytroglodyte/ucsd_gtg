﻿
using UnityEngine;

public class RequestMusic : NMN.Behaviour
{
	[SerializeField] private AudioClip clip = null;

	private void Awake( )
	{
		Audio.PlayMusic( clip );
	}
}
