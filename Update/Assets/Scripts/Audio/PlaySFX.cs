﻿
using UnityEngine;

public class PlaySFX : NMN.Behaviour
{
	[SerializeField] private AudioClip clip = null;

	private void Awake( )
	{
		Audio.PlaySFX( clip );
	}
}
