﻿
using System;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GTG
{
	[Serializable]
	[JsonObject( MemberSerialization.OptIn )]
	public class ConfigData
	{
		private const float BaseCursorSize = 20.0f;

		public const string DataPath = "Data/config.txt";
		private const string SavePath = "Assets/StreamingAssets/" + DataPath;

		private static string DefaultAnalyticsPath
		{
			get
			{
				var path = Application.persistentDataPath;
				path = Path.Combine( path, "Data" );
				path = Path.Combine( path, "Analytics" );
				return path;
			}
		}
		
		public bool EmulateMovement { get { return emulate; } }
		public float CursorSize { get { return cursorSize; } }
		public float CursorScale { get { return CursorSize / BaseCursorSize; } }
		public string Username { get { return username; } }
		public string AnalyticsFolder { get { return string.IsNullOrEmpty( analyticsFolder ) ? DefaultAnalyticsPath : analyticsFolder; } }

		[JsonProperty]
		[SerializeField]
		private bool emulate = false;

		[JsonProperty]
		[SerializeField]
		private float cursorSize = BaseCursorSize;

		[JsonProperty]
		[SerializeField]
		private string username = "default_user";

		[JsonProperty]
		[SerializeField]
		private string analyticsFolder = string.Empty;

		public static ConfigData Load( )
		{
			var data = File.ReadAllText( Path.Combine( Application.streamingAssetsPath, DataPath ) );
			return JsonConvert.DeserializeObject<ConfigData>( data );
		}

#if UNITY_EDITOR && !UNITY_WEBPLAYER
		public static void Save( ConfigData data )
		{
			var currentDirectory = Directory.GetCurrentDirectory( );
			var path = Path.Combine( currentDirectory, SavePath );

			AssetDatabase.StartAssetEditing( );

			Directory.CreateDirectory( Path.GetDirectoryName( path ) );
			File.WriteAllText( path, JsonConvert.SerializeObject( data, Formatting.Indented ) );

			AssetDatabase.StopAssetEditing( );
			AssetDatabase.SaveAssets( );
			AssetDatabase.Refresh( );
		}
#endif
	}

	public class Config : NMN.Singleton<Config>
	{
		public static bool Emulate { get { return Instance.data.EmulateMovement; } }
		public static float CursorSize { get { return Instance.data.CursorSize; } }
		public static float CursorScale { get { return Instance.data.CursorScale; } }
		public static string Username { get { return Instance.data.Username; } }
		public static string AnalyticsFolder { get { return Instance.data.AnalyticsFolder; } }

		private ConfigData data = null;

		protected override void OnSpawned( )
		{
			base.OnSpawned( );

			data = ConfigData.Load( );
		}

		protected override void OnApplicationQuit( )	//	Don't run the base functionality.
		{
		}
	}
}

public class ConfigSerialiser : NMN.Behaviour
{
	[SerializeField] private GTG.ConfigData config = null;

	[ContextMenu( "Serialise" )]
	private void Serialise( )
	{
#if UNITY_EDITOR && !UNITY_WEBPLAYER
		GTG.ConfigData.Save( config );
#endif
	}

	[ContextMenu( "Deserialise" )]
	private void Deserialise( )
	{
#if UNITY_EDITOR && !UNITY_WEBPLAYER
		config = GTG.ConfigData.Load( );
#endif
	}
}