﻿
using System;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace GTG
{
	[Serializable]
	[JsonObject(MemberSerialization.OptIn)]
	public class Game
	{
		public const string DataPath = "Data/game_data";
		private const string FullPath = "Assets/Content/Data/Resources/" + DataPath + ".txt";

		public string SceneName { get { return sceneName; } }
		public string DisplayName { get { return displayName; } }
		public string Description { get { return description; } }
		public bool DebugOnly { get { return debugOnly; } }

		[JsonProperty]
		[SerializeField]
		private string sceneName = string.Empty;

		[JsonProperty]
		[SerializeField]
		private string displayName = string.Empty;
		
		[JsonProperty]
		[SerializeField]
		private string description = string.Empty;

		[JsonProperty]
		[SerializeField]
		private string splashScreen = string.Empty;

		[JsonProperty]
		[SerializeField]
		private string gameSelectionIcon = string.Empty;

		[JsonProperty]
		[SerializeField]
		private string levelSelectionIcon = string.Empty;

		[JsonProperty]
		[SerializeField]
		private bool debugOnly = false;

		public static List<Game> Load( )
		{
			var data = Resources.Load<TextAsset>( DataPath );
			return JsonConvert.DeserializeObject<List<Game>>( data.text );
		}

#if UNITY_EDITOR && !UNITY_WEBPLAYER
		public static void Save( List<Game> games )
		{
			var currentDirectory = Directory.GetCurrentDirectory( );
			var path = Path.Combine( currentDirectory, FullPath );

			AssetDatabase.StartAssetEditing( );

			Directory.CreateDirectory( Path.GetDirectoryName( path ) );
			File.WriteAllText( path, JsonConvert.SerializeObject( games, Formatting.Indented ) );

			AssetDatabase.StopAssetEditing( );
			AssetDatabase.SaveAssets( );
			AssetDatabase.Refresh( );
		}
#endif

		public void LoadSplashScreen( Action<Texture2D> onComplete )
		{
			NMN.Job.Make( LoadTexture( splashScreen, onComplete ) );
		}

		public void LoadGameSelectIcon( Action<Texture2D> onComplete )
		{
			NMN.Job.Make( LoadTexture( gameSelectionIcon, onComplete ) );
		}

		public void LoadLevelSelectIcon( Action<Texture2D> onComplete )
		{
			NMN.Job.Make( LoadTexture( levelSelectionIcon, onComplete ) );
		}

		private IEnumerator LoadTexture( string texture, Action<Texture2D> onComplete )
		{
			yield return null;
			
			var request = Resources.LoadAsync<Texture2D>( texture );
			yield return request;

			onComplete( request.asset as Texture2D );

			yield break;
		}
	}
}

public class GameData : NMN.Behaviour
{
	[SerializeField] private List<GTG.Game> games = null;

	[ContextMenu( "Serialise" )]
	private void Serialise( )
	{
#if UNITY_EDITOR && !UNITY_WEBPLAYER
		GTG.Game.Save( games );
#endif
	}

	[ContextMenu( "Deserialise" )]
	private void Deserialise( )
	{
#if UNITY_EDITOR && !UNITY_WEBPLAYER
		games = GTG.Game.Load( );
#endif
	}
}