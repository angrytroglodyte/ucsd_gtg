﻿
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace GTG
{
	[JsonObject(MemberSerialization.OptIn)]
	public class GameSave
	{
		public const int MaxLevels = 16;
		public static int ClampLevel( int requested ) { return Mathf.Min( requested, MaxLevels ); }

		public int LastCompleteLevel { get { return Mathf.Max( 0, lastCompleteLevel ); } }
		//public int AvailableLevels { get { return Mathf.Max( 15, availableLevels ); } }

		[JsonProperty] private string game = string.Empty;
		[JsonProperty] private int lastCompleteLevel = 0;

		public void CompleteLevel( int level )
		{
			lastCompleteLevel = Mathf.Max( ClampLevel( level ), LastCompleteLevel );
		}

		public void Save( )
		{
			var path = FilePath( game );
			var data = JsonConvert.SerializeObject( this );
			Directory.CreateDirectory( Path.GetDirectoryName( path ) );

#if !UNITY_WEBPLAYER
			File.WriteAllText( path, data );
#else
			PlayerPrefs.SetString( game + "_save", data );
#endif
		}

		public GameSave( string game ) { this.game = game; }

		private static string FilePath( string game )
		{
			var root = Path.Combine( Application.persistentDataPath, "Data" );
			root = Path.Combine( root, "Game" );
			root = Path.Combine( root, User.Username );
			return Path.Combine( root, game + ".dat" );
		}

		public static GameSave Load( string game )
		{
#if !UNITY_WEBPLAYER
			var path = FilePath( game );
			if ( File.Exists( path ) )
#else
			if ( PlayerPrefs.HasKey( game + "_save" ) )
#endif
			{
#if !UNITY_WEBPLAYER
				var data = File.ReadAllText( path );
#else
				var data = PlayerPrefs.GetString( game + "_save" );
#endif

				return JsonConvert.DeserializeObject<GameSave>( data );
			}
			else
			{
				return new GameSave( game );
			}
		}
	}
}
