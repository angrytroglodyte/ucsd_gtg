﻿
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ResourcePool))]
public class ResourcePoolEditor : Editor
{
	private SerializedProperty categories = null;

	private void OnEnable( )
	{
		categories = serializedObject.FindProperty( "categories" );
	}

	public override void OnInspectorGUI( )
	{
		serializedObject.Update();

		//base.OnInspectorGUI( );

		EditorGUILayout.Space( );
		EditorGUILayout.BeginVertical( "Box" );
		{
			++EditorGUI.indentLevel;

			EditorGUILayout.BeginHorizontal( );
			{
				categories.isExpanded = EditorGUILayout.Foldout( categories.isExpanded, "Categories" );

				GUILayout.FlexibleSpace( );
				if ( GUILayout.Button( "Add Category", EditorStyles.miniButton ) )
				{
					var index = categories.arraySize;
					categories.InsertArrayElementAtIndex( index );

					var category = categories.GetArrayElementAtIndex( index );
					category.FindPropertyRelative( "key" ).stringValue = "New Category";
				}
			}
			EditorGUILayout.EndHorizontal( );

			if ( categories.isExpanded )
			{
				for ( int i = 0; i < categories.arraySize; ++i )
				{
					var category = categories.GetArrayElementAtIndex( i );
					EditorGUILayout.BeginVertical( "Box" );
					{
						++EditorGUI.indentLevel;

						var key = category.FindPropertyRelative( "key" );

						EditorGUILayout.BeginHorizontal( );
						{
							category.isExpanded = EditorGUILayout.Foldout( category.isExpanded, key.stringValue );
							GUILayout.FlexibleSpace( );

							EditorGUI.BeginDisabledGroup( i == 0 );
							{
								if ( GUILayout.Button( "^", EditorStyles.miniButtonLeft ) )
								{
									categories.MoveArrayElement( i, i - 1 );
									Repaint( );
									break;
								}
							}
							EditorGUI.EndDisabledGroup( );

							EditorGUI.BeginDisabledGroup( i == ( categories.arraySize - 1 ) );
							{
								if ( GUILayout.Button( "v", EditorStyles.miniButtonMid ) )
								{
									categories.MoveArrayElement( i, i + 1 );
									Repaint( );
									break;
								}
							}
							EditorGUI.EndDisabledGroup( );

							if ( GUILayout.Button( "Remove", EditorStyles.miniButtonRight ) )
							{
								if ( EditorUtility.DisplayDialog( "Remove Category", "Are you sure you want to remove this category?\n" + key.stringValue, "Yes", "No" ) )
								{
									categories.DeleteArrayElementAtIndex( i );
									Repaint( );
									break;
								}
							}
						}
						EditorGUILayout.EndHorizontal( );

						if ( category.isExpanded )
						{
							EditorGUILayout.PropertyField( key );

							EditorGUILayout.BeginVertical( "Box" );
							{
								++EditorGUI.indentLevel;

								var options = category.FindPropertyRelative( "options" );

								EditorGUILayout.BeginHorizontal( );
								{
									options.isExpanded = EditorGUILayout.Foldout( options.isExpanded, "Options" );

									GUILayout.FlexibleSpace( );
									if ( GUILayout.Button( "Add", EditorStyles.miniButton, GUILayout.Width( 40.0f ) ) )
									{
										var index = options.arraySize;
										options.InsertArrayElementAtIndex( index );
										options.GetArrayElementAtIndex( index ).objectReferenceValue = null;
									}
								}
								EditorGUILayout.EndHorizontal( );

								if ( options.isExpanded )
								{
									for ( int j = 0; j < options.arraySize; ++j )
									{
										EditorGUILayout.BeginHorizontal( );
										{
											EditorGUILayout.PropertyField( options.GetArrayElementAtIndex( j ) );

											if ( GUILayout.Button( "x", EditorStyles.miniButton, GUILayout.Width( 25.0f ) ) )
											{
												options.DeleteArrayElementAtIndex( j );
												//Repaint( );
												break;
											}
										}
										EditorGUILayout.EndHorizontal( );
									}
								}

								--EditorGUI.indentLevel;
							}
							EditorGUILayout.EndVertical( );
						}

						--EditorGUI.indentLevel;
					}
					EditorGUILayout.EndVertical( );
				}
			}

			--EditorGUI.indentLevel;
		}
		EditorGUILayout.EndVertical( );

		serializedObject.ApplyModifiedProperties();
	}
}
