﻿
using System.IO;
using UnityEngine;
using UnityEditor;

using NMN.Extensions;
using Newtonsoft.Json;

namespace GTG.Editor
{
	public class MoleTerrainGenerator : ScriptableWizard
	{
		public enum Resolution
		{
			Small,
			Medium,
			Large
		}

		private static int[] resolutions =
		{
			512,
			1024,
			2096
		};

		private const string TerrainDataDirectory = "Assets/Content/Moles/Terrain/Data";
		private const string TerrainPrefabDirectory = "Assets/Content/Moles/Terrain/Prefabs";

		public string TerrainName = "New Terrain Data";

		public float width = 100.0f;
		public float depth = 100.0f;
		public float height = 10.0f;
		public Resolution heightmapResolution = Resolution.Small;
		public Resolution splatResolution = Resolution.Small;

		[Range( 1.0f, 10.0f )]
		public float tiling = 1.0f;

		[Range( 0.0f, 1.0f )]
		public float offset = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float scaling = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float rampUpRange = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float rampDownRange = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float center = 0.5f;

		[Range( 0.0f, 1.0f )]
		public float clearingSize = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float clearingEdgeSize = 0.0f;

		public Texture2D lowTexture = null;
		public Vector4 lowTextureTiling = new Vector4( 1, 1, 0, 0 );

		public Texture2D highTexture = null;
		public Vector4 highTextureTiling = new Vector4( 1, 1, 0, 0 );

		public Texture2D slopeXTexture = null;
		public Texture2D slopeYTexture = null;
		public Vector4 slopeTextureTiling = new Vector4( 1, 1, 0, 0 );		

		[Range( 0.0f, 1.0f )]
		public float lowTextureMax = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float highTextureMin = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float slopeMin = 0.0f;

		[Range( 0.0f, 1.0f )]
		public float slopeMax = 0.0f;

		private static TerrainData DataFromSelection( )
		{
			var gameObject = Selection.activeGameObject;
			if ( gameObject == null )
			{
				return null;
			}

			var terrain = gameObject.GetComponent<Terrain>( );
			if ( terrain == null )
			{
				return null;
			}

			var data = terrain.terrainData;
			if ( data == null )
			{
				return null;
			}

			return data;
		}

		private static Resolution SizeToResolution( int size )
		{
			if ( size <= resolutions[0] )
			{
				return Resolution.Small;
			}
			else if ( size <= resolutions[1] )
			{
				return Resolution.Medium;
			}
			else
			{
				return Resolution.Large;
			}
		}

		[MenuItem("GTG/Mole Whack/Terrain Generator")]
		public static void CreateWizard( MenuCommand command )
		{
			var self = ScriptableWizard.DisplayWizard<MoleTerrainGenerator>( "Mole Terrain Generator", "Generate", "Apply" );
			var data = DataFromSelection( );

			self.Deserialize( );

			if ( data != null )
			{
				self.width = data.size.x;
				self.depth = data.size.z;
				self.height = data.size.y;

				self.heightmapResolution = SizeToResolution( data.heightmapResolution - 1 );
				self.splatResolution = SizeToResolution( data.alphamapResolution );
			}
		}

		private void OnWizardCreate( )
		{
			var data = CreateTerrainAsset( );
			CreatePrefab( data );

			SetupTerrainData( data );

			Serialize( );
		}

		private void OnWizardOtherButton( )
		{
			var data = DataFromSelection( );
			if ( data != null )
			{
				SetupTerrainData( data );
			}

			Serialize( );
		}

		private string TerrainDataPath( )
		{
			return AssetDatabase.GenerateUniqueAssetPath( Path.Combine( TerrainDataDirectory, TerrainName + ".asset" ) );
		}

		private string TerrainPrefabPath( )
		{
			return AssetDatabase.GenerateUniqueAssetPath( Path.Combine( TerrainPrefabDirectory, TerrainName + ".prefab" ) );
		}

		private TerrainData CreateTerrainAsset( )
		{
			var terrainData = new TerrainData( );
			AssetDatabase.CreateAsset( terrainData, TerrainDataPath( ) );

			return terrainData;
		}

		private GameObject CreatePrefab( TerrainData data )
		{
			var gameObject = new GameObject( TerrainName );
			var terrain = gameObject.AddComponent<Terrain>( );
			var collider = gameObject.AddComponent<TerrainCollider>( );
			terrain.terrainData = data;
			collider.terrainData = data;

			var prefab = PrefabUtility.CreatePrefab( TerrainPrefabPath( ), gameObject, ReplacePrefabOptions.ReplaceNameBased );

			DestroyImmediate( gameObject );
			return prefab;
		}

		private SplatPrototype GetSplatPrototype( Texture2D texture, Vector4 tiling )
		{
			var splat = new SplatPrototype( );
			splat.texture = texture;
			splat.tileOffset = new Vector2( tiling.z, tiling.w );
			splat.tileSize = new Vector2( tiling.x, tiling.y );

			return splat;
		}

		private void SetupTerrainData( TerrainData data )
		{
			NMN.Editor.Tools.EditAssets( ( ) =>
				{
					data.heightmapResolution = resolutions[heightmapResolution.ToInt( )] + 1;
					data.alphamapResolution = resolutions[splatResolution.ToInt( )];

					data.size = new Vector3( width, height, depth );

					var prototypes = new SplatPrototype[4];
					prototypes[0] = GetSplatPrototype( lowTexture, lowTextureTiling );
					prototypes[1] = GetSplatPrototype( highTexture, highTextureTiling );
					prototypes[2] = GetSplatPrototype( slopeXTexture, slopeTextureTiling );
					prototypes[3] = GetSplatPrototype( slopeYTexture, slopeTextureTiling );

					data.splatPrototypes = prototypes;

					GenerateTerrain( data );
				} );

			//	Calculate texturing separately to allow heights to be applied first
			NMN.Editor.Tools.EditAssets( ( ) =>
				{
					GenerateSplats( data );
				} );
		}

		private void GenerateTerrain( TerrainData data )
		{
			float[,] heights = new float[data.heightmapResolution, data.heightmapResolution];
			float size = data.heightmapResolution;
			var center = new Vector2( 0.5f, this.center );
			for ( int y = 0; y < data.heightmapResolution; ++y )
			{
				for ( int x = 0; x < data.heightmapResolution; ++x )
				{
					var coord = new Vector2( x / size, y / size );
					var height = ( Mathf.PerlinNoise( coord.x * tiling, coord.y * tiling ) * scaling ) + offset;

					if ( coord.y <= rampUpRange )
					{
						var rampScaling = coord.y / rampUpRange;
						height *= NMN.Math.SmoothStep( rampScaling );
					}
					else if ( coord.y >= rampDownRange )
					{
						var rampScaling = ( coord.y - rampDownRange ) / ( 1.0f - rampDownRange );
						height *= NMN.Math.LinearStep( 0.5f, 1.0f, NMN.Math.SmoothStep( 1.0f - ( rampScaling* 0.5f ) ) );
					}

					var distanceFromCenter = ( coord - center ).magnitude;
					if ( distanceFromCenter < clearingSize )
					{
						height = 0.0f;
					}
					else if ( distanceFromCenter < ( clearingSize + clearingEdgeSize ) )
					{
						var distanceIntoEdge = distanceFromCenter - clearingSize;
						var ratioIntoEdge = distanceIntoEdge / clearingEdgeSize;
						height *= NMN.Math.SmoothStep( ratioIntoEdge );
					}

					heights[y, x] = height;
				}
			}

			data.SetHeights( 0, 0, heights );
		}

		private void GenerateSplats( TerrainData data )
		{
			var alphas = new float[data.alphamapResolution, data.alphamapResolution, 4];
			float inverseResolution = 1.0f / data.alphamapResolution;
			for ( int y = 0; y < data.alphamapResolution; ++y )
			{
				for ( int x = 0; x < data.alphamapResolution; ++x )
				{
					var coord = new Vector2( x * inverseResolution, y * inverseResolution );

					var steepness = data.GetSteepness( coord.y, coord.x );
					var actualHeight = data.GetInterpolatedHeight( coord.y, coord.x );
					var normal = data.GetInterpolatedNormal( coord.y, coord.x );

					var slope = steepness / 90.0f;
					var height = actualHeight / data.size.y;

					var lowAlpha = ( height < lowTextureMax ) ? 1.0f : NMN.Math.LinearStep( highTextureMin, lowTextureMax, height );
					var highAlpha = ( height > highTextureMin ) ? 1.0f : NMN.Math.LinearStep( lowTextureMax, highTextureMin, height );
					
					var slopeAlpha = NMN.Math.LinearStep( slopeMin, slopeMax, slope );
					lowAlpha *= 1.0f - slopeAlpha;
					highAlpha *= 1.0f - slopeAlpha;

					var normalTotal = Mathf.Abs( normal.x ) + Mathf.Abs( normal.z );
					var slopeX = NMN.Math.SmoothStep( 0.45f, 0.55f, Mathf.Abs( normal.x ) / normalTotal );
					var slopeY = 1.0f - slopeX;

					alphas[x, y, 0] = lowAlpha;
					alphas[x, y, 1] = highAlpha;
					alphas[x, y, 2] = slopeAlpha * slopeX;
					alphas[x, y, 3] = slopeAlpha * slopeY;
				}
			}

			data.SetAlphamaps( 0, 0, alphas );
		}

		private void SetVector4( string key, Vector4 v4 )
		{
			EditorPrefs.SetString( key, v4.x + " " + v4.y + " " + v4.z + " " + v4.w );
		}

		private Vector4 GetVector4( string key, Vector4 value )
		{
			var str = EditorPrefs.GetString( key, null );
			if ( str == null )
			{
				return value;
			}

			var values = str.Split( ' ' );
			if ( values.Length != 4 )
			{
				return value;
			}

			float.TryParse( values[0], out value.x );
			float.TryParse( values[1], out value.y );
			float.TryParse( values[2], out value.z );
			float.TryParse( values[3], out value.w );

			return value;
		}

		private void Serialize( )
		{
			EditorPrefs.SetString( "MoleTerrain - Name", TerrainName );

			EditorPrefs.SetFloat( "MoleTerrain - Width", width );
			EditorPrefs.SetFloat( "MoleTerrain - Depth", depth );
			EditorPrefs.SetFloat( "MoleTerrain - Height", height );

			EditorPrefs.SetInt( "MoleTerrain - Heightmap Resolution", heightmapResolution.ToInt( ) );
			EditorPrefs.SetInt( "MoleTerrain - Splat Resolution", splatResolution.ToInt( ) );

			EditorPrefs.SetFloat( "MoleTerrain - Tiling", tiling );
			EditorPrefs.SetFloat( "MoleTerrain - Offset", offset );
			EditorPrefs.SetFloat( "MoleTerrain - Scaling", scaling );
			EditorPrefs.SetFloat( "MoleTerrain - Ramp Up", rampUpRange );
			EditorPrefs.SetFloat( "MoleTerrain - Ramp Down", rampDownRange );
			EditorPrefs.SetFloat( "MoleTerrain - Center", center );
			EditorPrefs.SetFloat( "MoleTerrain - Clearing", clearingSize );
			EditorPrefs.SetFloat( "MoleTerrain - Clearing Edge", clearingEdgeSize );

			EditorPrefs.SetString( "MoleTerrain - Low Texture", NGUIEditorTools.ObjectToGUID( lowTexture ) );
			SetVector4( "MoleTerrain - Low Texture Tiling", lowTextureTiling );

			EditorPrefs.SetString( "MoleTerrain - High Texture", NGUIEditorTools.ObjectToGUID( highTexture ) );
			SetVector4( "MoleTerrain - High Texture Tiling", highTextureTiling );

			EditorPrefs.SetString( "MoleTerrain - Slope X Texture", NGUIEditorTools.ObjectToGUID( slopeXTexture ) );
			EditorPrefs.SetString( "MoleTerrain - Slope Y Texture", NGUIEditorTools.ObjectToGUID( slopeYTexture ) );
			SetVector4( "MoleTerrain - Slope Texture Tiling", slopeTextureTiling );

			EditorPrefs.SetFloat( "MoleTerrain - Low Texture Max", lowTextureMax );
			EditorPrefs.SetFloat( "MoleTerrain - High Texture Min", highTextureMin );
			EditorPrefs.SetFloat( "MoleTerrain - Slope Min", slopeMin );
			EditorPrefs.SetFloat( "MoleTerrain - Slope Max", slopeMax );
		}

		private void Deserialize( )
		{
			TerrainName = EditorPrefs.GetString( "MoleTerrain - Name", TerrainName );

			width = EditorPrefs.GetFloat( "MoleTerrain - Width", width );
			depth = EditorPrefs.GetFloat( "MoleTerrain - Depth", depth );
			height = EditorPrefs.GetFloat( "MoleTerrain - Height", height );

			heightmapResolution = EditorPrefs.GetInt( "MoleTerrain - Heightmap Resolution", heightmapResolution.ToInt( ) ).ToEnum<Resolution>( );
			splatResolution = EditorPrefs.GetInt( "MoleTerrain - Splat Resolution", splatResolution.ToInt( ) ).ToEnum<Resolution>( );

			tiling = EditorPrefs.GetFloat( "MoleTerrain - Tiling", tiling );
			offset = EditorPrefs.GetFloat( "MoleTerrain - Offset", offset );
			scaling = EditorPrefs.GetFloat( "MoleTerrain - Scaling", scaling );
			rampUpRange = EditorPrefs.GetFloat( "MoleTerrain - Ramp Up", rampUpRange );
			rampDownRange = EditorPrefs.GetFloat( "MoleTerrain - Ramp Down", rampDownRange );
			center = EditorPrefs.GetFloat( "MoleTerrain - Center", center );
			clearingSize = EditorPrefs.GetFloat( "MoleTerrain - Clearing", clearingSize );
			clearingEdgeSize = EditorPrefs.GetFloat( "MoleTerrain - Clearing Edge", clearingEdgeSize );

			var lowGUID = EditorPrefs.GetString( "MoleTerrain - Low Texture", null );
			lowTexture = ( lowGUID == null ) ? null : NGUIEditorTools.GUIDToObject<Texture2D>( lowGUID );

			lowTextureTiling = GetVector4( "MoleTerrain - Low Texture Tiling", lowTextureTiling );

			var highGUID = EditorPrefs.GetString( "MoleTerrain - High Texture", null );
			highTexture = ( highGUID == null ) ? null : NGUIEditorTools.GUIDToObject<Texture2D>( highGUID );

			highTextureTiling = GetVector4( "MoleTerrain - High Texture Tiling", highTextureTiling );

			var slopeXGUID = EditorPrefs.GetString( "MoleTerrain - Slope X Texture", null );
			slopeXTexture = ( slopeXGUID == null ) ? null : NGUIEditorTools.GUIDToObject<Texture2D>( slopeXGUID );

			var slopeYGUID = EditorPrefs.GetString( "MoleTerrain - Slope Y Texture", null );
			slopeYTexture = ( slopeYGUID == null ) ? null : NGUIEditorTools.GUIDToObject<Texture2D>( slopeYGUID );

			slopeTextureTiling = GetVector4( "MoleTerrain - Slope Texture Tiling", slopeTextureTiling );

			lowTextureMax = EditorPrefs.GetFloat( "MoleTerrain - Low Texture Max", lowTextureMax );
			highTextureMin = EditorPrefs.GetFloat( "MoleTerrain - High Texture Min", highTextureMin );
			slopeMin = EditorPrefs.GetFloat( "MoleTerrain - Slope Min", slopeMin );
			slopeMax = EditorPrefs.GetFloat( "MoleTerrain - Slope Max", slopeMax );
		}
	}
}
