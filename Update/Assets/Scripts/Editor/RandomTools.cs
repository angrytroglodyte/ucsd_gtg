﻿
using UnityEditor;
using UnityEngine;

namespace GTG.Tools
{
	public static class RandomTools
	{
		[MenuItem( "GTG/Mole Whack/Snap To Terrain" )]
		public static void SnapToTerrain( )
		{
			Transform[] transforms = Selection.GetTransforms( SelectionMode.ExcludePrefab | SelectionMode.OnlyUserModifiable );

			if ( transforms.Length > 0 )
			{
				foreach ( Transform transform in transforms )
				{
					RaycastHit hit;
					if ( Physics.Raycast( transform.position + Vector3.up, Vector3.down, out hit ) )
					{
						transform.position = hit.point;
						transform.rotation = Quaternion.LookRotation( transform.forward, hit.normal );
					}
				}
			}
		}

		[MenuItem( "GTG/Mole Whack/Random Orientation" )]
		public static void RandomRotation( )
		{
			Transform[] transforms = Selection.GetTransforms( SelectionMode.ExcludePrefab | SelectionMode.OnlyUserModifiable );

			if ( transforms.Length > 0 )
			{
				foreach ( Transform transform in transforms )
				{
					Vector3 randomized = Random.onUnitSphere;
					randomized = new Vector3( randomized.x, 0F, randomized.z );
					transform.rotation = Quaternion.LookRotation( randomized );
				}
			}
		}

		[MenuItem( "GTG/Butterfly Bob/Switch To Winter" )]
		public static void SwitchToWinter( )
		{
			var transforms = Selection.GetTransforms( SelectionMode.ExcludePrefab | SelectionMode.OnlyUserModifiable | SelectionMode.Deep );

			foreach ( var transform in transforms )
			{
				var sprite = transform.GetComponent<tk2dSprite>( );
				if ( sprite != null )
				{
					var atlas = sprite.Collection;
					var path = AssetDatabase.GetAssetPath( atlas.gameObject );
					path = path.Replace( "Summer", "Winter" ).Replace( "summer", "winter" );

					var newAtlasContainer = AssetDatabase.LoadAssetAtPath( path, typeof(GameObject) ) as GameObject;
					var newAtlas = newAtlasContainer.GetComponent<tk2dSpriteCollectionData>( );

					var instance = sprite.CurrentSprite;
					var newInstance = newAtlas.GetSpriteDefinition( instance.name );
					if ( newInstance != null )
					{
						sprite.SetSprite( newAtlas, newInstance.name );
						EditorUtility.SetDirty( sprite );
					}
					else
					{
						Debug.LogError( "WINTER ATLAS DOES NOT CONTAIN: " + instance.name, sprite );
					}
				}
			}
		}
	}
}
