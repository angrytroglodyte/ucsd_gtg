﻿
using System;
using UnityEngine;

public class CountdownTimer : NMN.BaseUI
{
	public static event Action OnCountdownStart;

	public static void Load( GTG.Game game, Action onClose )
	{
		NMUILoader.LoadUI<CountdownTimer>( game.SceneName + "_countdown", ( popup ) =>
		{
			popup.onClose = onClose;

			if ( OnCountdownStart != null )
			{
				OnCountdownStart( );
			}
		} );
	}

	[SerializeField] private int time = 3;
	[SerializeField] private UILabel label = null;

	private Action onClose = null;
	private int current = 0;
	private NMN.Job task = null;

	public override void OnSpawn( UICamera camera )
	{
		base.OnSpawn( camera );

		current = time;

		task = InvokeRepeating( Countdown, 1.0f, 1.0f );
		UpdateLabel( );
	}

	private void Countdown( )
	{
		if ( current > 0 )
		{
			--current;
			UpdateLabel( );
		}
		else
		{
			onClose( );
			Close( );
			task.Kill( );
		}
	}

	private void UpdateLabel( )
	{
		label.text = current > 0 ? current.ToString( ) : "GO!";
	}
}
