﻿
using System;
using UnityEngine;

public class BasicResultsPopup : NMN.BaseUI
{
	public static void Load( GTG.BaseGame game )
	{
		NMUILoader.LoadUI<BasicResultsPopup>( game.Data.SceneName + "_results", ( popup ) =>
		{
			popup.Populate( game );
		} );
	}

	[SerializeField] private GameObject container = null;
	[SerializeField] private UILabel score = null;
	[SerializeField] private GameObject retryButton = null;
	[SerializeField] private GameObject nextButton = null;

	private GTG.BaseGame game = null;

	public void RetryLevel( )
	{
		LoadingScreenUI.Load( game.Data, game.Level, false, ( ui ) =>
			{
				game.ShutDown( ( ) =>
					{
						Close( );
						ui.LoadLevel( );
					} );
			} );
	}

	public void NextLevel( )
	{
		LoadingScreenUI.Load( game.Data, game.Level + 1, false, ( ui ) =>
		{
			game.ShutDown( ( ) =>
			{
				Close( );
				ui.LoadLevel( );
			} );
		} );
	}

	public void LevelSelect( )
	{
		LevelSelectionUI.Load( game.Data, ( ui ) =>
		{
			ui.OnIntroComplete += ( ui2 ) =>
				{
					game.ShutDown( null );
					Close( );
				};
		} );
	}

	protected virtual void Populate( GTG.BaseGame game )
	{
		this.game = game;

		if ( score != null )
		{
			score.text = game.Score.ToString( );
		}

		NGUITools.SetActive( container, true );

		NGUITools.SetActive( nextButton, game.Victory );
		NGUITools.SetActive( retryButton, !game.Victory );
	}
}
