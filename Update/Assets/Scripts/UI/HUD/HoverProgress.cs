﻿
using System;
using UnityEngine;

public class HoverProgress : NMN.Behaviour
{
	[SerializeField] private UISprite sprite = null;
	[SerializeField] private float timeRequired = 1.0f;
	[SerializeField] private UITweener tween = null;
	[SerializeField] private string spriteNameFormat = "spinner_{0}";
	[SerializeField] private int minSpriteID = 0;
	[SerializeField] private int maxSpriteID = 0;

	private float timeRemaining = 0.0f;

	private GTG.MoleBase mole = null;
	private BoxCollider focalPoint = null;
	private Camera sourceCamera = null;
	private Camera destinationCamera = null;

	[ContextMenu("Begin")]
	public void Begin( GTG.MoleBase mole, Camera sourceCamera, Camera destinationCamera )
	{
		timeRemaining = timeRequired;

		this.mole = mole;
		this.sourceCamera = sourceCamera;
		this.destinationCamera = destinationCamera;
		this.focalPoint = mole.Focus.GetComponent<BoxCollider>( );

		this.mole.OnLookStop += OnLookStop;

		tween.PlayForward( );

		LateUpdate( );
	}

	[ContextMenu( "Cancel" )]
	public void End( )
	{
		if ( enabled )
		{
			mole.OnLookStop -= OnLookStop;

			enabled = false;
			tween.PlayReverse( );
		}
	}

	public void TweenComplete( )
	{
		if ( tween.direction == AnimationOrTween.Direction.Reverse )
		{
			Destroy( gameObject );
		}
	}

	private void OnLookStop( GTG.MoleBase mole )
	{
		End( );
	}

	private void Update( )
	{
		if ( timeRemaining > 0.0f )
		{
			timeRemaining -= Time.deltaTime;

			var progress = NMN.Math.SmoothStep( timeRequired, 0.0f, timeRemaining );
			var id = Mathf.FloorToInt( Mathf.Lerp( minSpriteID, maxSpriteID, progress ) );
			sprite.spriteName = string.Format( spriteNameFormat, id );

			if ( timeRemaining < 0.0f )
			{
				//	Complete
				mole.Whack( );
				End( );

				sprite.spriteName = string.Format( spriteNameFormat, maxSpriteID );
			}
		}
	}

	private void LateUpdate( )
	{
		var position = mole.Focus.transform.TransformPoint( focalPoint.center );
		var uiPosition = destinationCamera.ViewportToWorldPoint( sourceCamera.WorldToViewportPoint( position ) );
		uiPosition.z = 0.0f;

		transform.position = uiPosition;
	}
}
