﻿
using UnityEngine;

public class ShroomResults : BasicResultsPopup
{
	[SerializeField] private UILabel shrooms = null;

	protected override void Populate( GTG.BaseGame game )
	{
		base.Populate( game );

		var shroomGame = game as ShroomGame;
		shrooms.text = shroomGame.ShroomsHit.ToString( );
	}
}
