﻿
using System;
using UnityEngine;

public class SpaceHUD : GTG.BaseHUD
{
	[SerializeField] private UIProgressBar progress = null;
	[SerializeField] private UISprite[] activeLives = null;

	private SpaceGame game = null;

	protected override void OnGameReady( GTG.BaseGame baseGame )
	{
		this.game = baseGame as SpaceGame;

		this.progress.value = game.Progress;

		UpdateLives( );
		game.OnLivesChanged += ( lives ) =>
			{
				UpdateLives( );
			};
	}

	private void UpdateLives( )
	{
		for ( int i = 0; i < activeLives.Length; ++i )
		{
			activeLives[i].enabled = ( game.Lives >= ( i + 1 ) );
		}
	}

	private void Update( )
	{
		if ( game != null )
		{
			progress.value = game.Progress;
		}
	}
}
