﻿
using System;
using UnityEngine;

public class SpaceWizardsHUD : GTG.BaseHUD
{
	[SerializeField] private UILabel planes = null;
	[SerializeField] private UISprite[] activeLives = null;

	private SpaceWizardsGame game = null;

	protected override void OnGameReady( GTG.BaseGame baseGame )
	{
		this.game = baseGame as SpaceWizardsGame;

		UpdateLives( );
		game.OnLivesChanged += ( lives ) =>
		{
			UpdateLives( );
		};

		UpdatePlanes( );
		game.OnEnemyDeath += ( remaining ) =>
		{
			UpdatePlanes( );
		};
	}

	private void UpdatePlanes( )
	{
		planes.text = game.RemainingEnemies.ToString( );
	}

	private void UpdateLives( )
	{
		for ( int i = 0; i < activeLives.Length; ++i )
		{
			activeLives[i].enabled = ( game.Lives >= ( i + 1 ) );
		}
	}
}
