﻿
using System;
using UnityEngine;

public class ButterflyBobHUD : GTG.BaseHUD
{
	[SerializeField] private UILabel starsRemaining = null;
	[SerializeField] private UISprite[] activeLives = null;

	private ButterflyGame game = null;

	protected override void OnGameReady(GTG.BaseGame baseGame)
	{
		this.game = baseGame as ButterflyGame;

		UpdateLives( );
		game.OnLivesChanged += ( lives ) =>
			{
				UpdateLives( );
			};

		UpdateStars( );
		game.OnStarCollected += ( ) =>
			{
				UpdateStars( );
			};
	}

	private void UpdateStars( )
	{
		starsRemaining.text = game.RemainingStars.ToString( );
	}

	private void UpdateLives( )
	{
		for ( int i = 0; i < activeLives.Length; ++i )
		{
			activeLives[i].enabled = ( game.Lives >= ( i + 1 ) );
		}
	}
}
