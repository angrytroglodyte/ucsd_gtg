﻿
using System;
using UnityEngine;

public class MoleWhackHUD : GTG.BaseHUD
{
	[SerializeField] private GameObject hoverProgressPrefab = null;
	[SerializeField] private UISprite[] activeLives = null;
	[SerializeField] private UILabel molesRemaining = null;

	private MoleGame game = null;
	private new UICamera camera = null;

	public override void OnSpawn( UICamera camera )
	{
		base.OnSpawn( camera );

		this.camera = camera;
	}

	protected override void OnGameReady( GTG.BaseGame baseGame )
	{
		this.game = baseGame as MoleGame;

		UpdateLives( );
		OnMoleWhacked( 0 );

		game.OnLivesChanged += OnLivesChanged;
		game.OnMoleWhacked += OnMoleWhacked;
		game.OnCountdownMole += OnCountdownMole;
	}

	private void OnLivesChanged( int lived )
	{
		UpdateLives( );
	}

	private void OnMoleWhacked( int hit )
	{
		molesRemaining.text = hit.ToString( );
	}

	private void OnCountdownMole( GTG.MoleBase mole )
	{
		var hoverInstance = NGUITools.AddChild( gameObject, hoverProgressPrefab );
		var progress = hoverInstance.GetComponent<HoverProgress>( );
		progress.Begin( mole, game.SceneCamera, camera.cachedCamera );
	}

	protected override void OnGameComplete()
	{
		game.OnLivesChanged -= OnLivesChanged;
		game.OnCountdownMole -= OnCountdownMole;

		base.OnGameComplete( );
	}

	private void UpdateLives( )
	{
		for ( int i = 0; i < activeLives.Length; ++i )
		{
			activeLives[i].enabled = ( game.Lives >= ( i + 1 ) );
		}
	}
}
