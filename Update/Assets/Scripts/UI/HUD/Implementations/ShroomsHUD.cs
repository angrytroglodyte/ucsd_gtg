﻿
using System;
using UnityEngine;

public class ShroomsHUD : GTG.BaseHUD
{
	[SerializeField] private UILabel remaining = null;
	[SerializeField] private UILabel timeRemaining = null;
	[SerializeField] private UITweener blackout = null;

	private ShroomGame game = null;
	private float blackoutDuration = 0.0f;

	public void OnBlackoutTweenComplete( )
	{
		if ( blackout.direction == AnimationOrTween.Direction.Forward )
		{
			Invoke( ( ) =>
				{
					blackout.PlayReverse( );
				}, blackoutDuration );
		}
		else
		{
			NGUITools.SetActive( blackout.gameObject, false );
		}
	}

	protected override void OnGameReady( GTG.BaseGame baseGame )
	{
		this.game = baseGame as ShroomGame;

		UpdateRemaining( );

		game.OnShroomPopped += ( ) =>
			{
				UpdateRemaining( );
			};

		game.OnBlackout += ( duration ) =>
			{
				NGUITools.SetActive( blackout.gameObject, true );
				blackout.PlayForward( );
				blackoutDuration = duration;
			};
	}

	private void UpdateRemaining( )
	{
		remaining.text = game.ShroomsRemaining.ToString( );
	}

	private void Update( )
	{
		if ( game != null )
		{
			var ratio = game.Progress;
			var duration = game.Duration;
			var timeRemaining = duration * ( 1.0f - ratio );

			var minutes = Mathf.FloorToInt( timeRemaining / 60.0f );
			var seconds = Mathf.RoundToInt( timeRemaining - ( minutes * 60.0f ) );
			var text = ( ( minutes > 0 ) ? minutes.ToString( ) : string.Empty ) + ":" + seconds.ToString( );

			this.timeRemaining.text = text;
		}
	}
}
