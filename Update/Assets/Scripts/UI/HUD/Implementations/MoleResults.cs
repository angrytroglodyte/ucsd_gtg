﻿
using UnityEngine;

public class MoleResults : BasicResultsPopup
{
	[SerializeField] private UILabel moles = null;

	protected override void Populate( GTG.BaseGame game )
	{
		base.Populate( game );

		var moleGame = game as MoleGame;
		moles.text = moleGame.MolesHit.ToString( );
	}
}
