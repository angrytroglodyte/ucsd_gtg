﻿
using System;
using UnityEngine;

public class PauseMenuPopup : NMN.BaseUI
{
	public static event Action OnPause;
	public static event Action OnResume;

	public static void Load( GTG.BaseGame game )
	{
		NMUILoader.LoadUI<PauseMenuPopup>( game.Data.SceneName + "_pause", ( ui ) =>
			{
				ui.game = game;
			} );
	}

	[SerializeField] private UIToggle musicToggle = null;
	[SerializeField] private UIToggle sfxToggle = null;

	private GTG.BaseGame game = null;

	public void ToggleMusic( )
	{
		Audio.Music.Volume = musicToggle.value ? 1.0f : 0.0f;
	}

	public void ToggleSFX( )
	{
		Audio.SFX.Volume = sfxToggle.value ? 1.0f : 0.0f;
	}

	public void Retry( )
	{
		LoadingScreenUI.Load( game.Data, game.Level, false, ( ui ) =>
		{
			game.ShutDown( ( ) =>
			{
				Close( );
				ui.LoadLevel( );
			} );
		} );
	}

	public void MainMenu( )
	{
		LevelSelectionUI.Load( game.Data, ( ui ) =>
		{
			ui.OnIntroComplete += ( ui2 ) =>
			{
				game.ShutDown( null );
				Close( );
			};
		} );
	}

	private void Awake( )
	{
		Time.timeScale = 0.0f;

		musicToggle.value = ( Audio.Music.Volume > 0.5f );
		sfxToggle.value = ( Audio.SFX.Volume > 0.5f );

		if ( OnPause != null )
		{
			OnPause( );
		}
	}

	private void OnDestroy( )
	{
		if ( OnResume != null )
		{
			OnResume( );
		}

		Time.timeScale = 1.0f;
	}
}
