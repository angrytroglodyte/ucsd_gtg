﻿
using UnityEngine;

public class PauseGame : NMN.Behaviour
{
	private GTG.BaseGame game = null;

	public void OnClick( )
	{
		PauseMenuPopup.Load( game );
	}

	private void OnGameReady( GTG.BaseGame game )
	{
		this.game = game;
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += OnGameReady;
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
