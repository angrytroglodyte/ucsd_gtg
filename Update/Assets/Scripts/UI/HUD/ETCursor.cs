﻿
using UnityEngine;

public class ETCursor : NMN.Behaviour
{
	[SerializeField] private UIWidget widget = null;

	private UICamera destCamera = null;

	private void Awake( )
	{
		destCamera = NMUILoader.Camera;

		widget.width = widget.height = (int)GTG.Config.CursorSize;
	}

	private void Update( )
	{
		if ( !ETCamera.Available || !ETCamera.LastPositionValid )
		{
			widget.enabled = false;
			Screen.showCursor = true;
			return;
		}

		widget.enabled = true;
		Screen.showCursor = false;

		var screenPoint = ETCamera.LastInputPosition;
		var uiPosition = destCamera.cachedCamera.ScreenToWorldPoint( screenPoint );

		var position = transform.position;
		position.x = uiPosition.x;
		position.y = uiPosition.y;
		transform.position = position;
	}

	//	This used to be an OnDestroy.
	//	Then OnDestroy stopped being called for some reason.
	private void OnDisable( )
	{
		Screen.showCursor = true;
	}
}
