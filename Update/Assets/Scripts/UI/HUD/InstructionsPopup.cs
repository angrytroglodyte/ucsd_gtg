﻿
using System;
using UnityEngine;

public class InstructionsPopup : NMN.BaseUI
{
	public static void Load( GTG.Game game, int level, Action onClose )
	{
		NMUILoader.LoadUI<InstructionsPopup>( game.SceneName + "_instructions", ( popup ) =>
			{
				popup.game = game;
				popup.onClose = onClose;
				popup.levelText.text = String.Format( popup.levelText.text, level );
			} );
	}

	[SerializeField] private UILabel levelText = null;

	private GTG.Game game = null;
	private Action onClose = null;

	public void Exit( )
	{
		Close( );
		CountdownTimer.Load( game, onClose );
	}
}
