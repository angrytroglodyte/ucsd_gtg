﻿
using System;
using UnityEngine;

namespace GTG
{
	public abstract class BaseHUD : NMN.BaseUI
	{
		private GTG.BaseGame baseGame = null;

		protected abstract void OnGameReady( GTG.BaseGame baseGame );
		
		protected virtual void OnGameComplete( )
		{
			BasicResultsPopup.Load( baseGame );
			NGUITools.SetActive( gameObject, false );
		}

		private void GameReady( GTG.BaseGame baseGame )
		{
			this.baseGame = baseGame;
			this.baseGame.OnShutDown += Shutdown;

			baseGame.OnGameComplete += OnGameComplete;

			OnGameReady( baseGame );
		}

		private void Shutdown( )
		{
			Close( );
		}

		private void Awake( )
		{
			GTG.BaseGame.OnGameReady += GameReady;
		}

		private void OnDestroy( )
		{
			GTG.BaseGame.OnGameReady -= GameReady;
		}
	}
}
