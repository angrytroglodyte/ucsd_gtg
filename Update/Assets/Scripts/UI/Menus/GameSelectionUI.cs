﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSelectionUI : NMN.BaseUI
{
	private static Action<GameSelectionUI> onLoadingComplete = null;

	public static void Load( Action<GameSelectionUI> onComplete )
	{
		onLoadingComplete = onComplete;
		NMUILoader.LoadUI( "game_selection_ui" );
	}

	[SerializeField] private UIScrollView scrollView = null;
	[SerializeField] private UIGrid buttonRoot = null;
	[SerializeField] private GameObject gameButtonPrefab = null;

	public void Settings( )
	{
		SettingsUI.Load( );
	}

	public void Exit( )
	{
		Application.Quit( );
	}

	private void Awake( )
	{
		var data = GTG.Game.Load( );

		NMN.Job.Make( Populate( data ) ).OnComplete += ( killed ) =>
			{
				onLoadingComplete( this );
				onLoadingComplete = null;
			};
	}

	private IEnumerator Populate( List<GTG.Game> games )
	{
		yield return null;

		scrollView.ResetPosition( );

		foreach ( var game in games )
		{
#if !UNITY_EDITOR
			if ( !game.DebugOnly )
#endif
			{
				Texture2D texture = null;
				game.LoadGameSelectIcon( ( logo ) => { texture = logo; } );

				while ( texture == null )
				{
					yield return null;
				}

				var instance = NGUITools.AddChild( buttonRoot.gameObject, gameButtonPrefab );
				var button = instance.GetComponent<GameButton>( );
				button.Display( game, texture );

				button.OnSelected += LoadLevelSelect;
			}
		}

		buttonRoot.Reposition( );
		scrollView.ResetPosition( );

		yield break;
	}

	private void LoadLevelSelect( GTG.Game game )
	{
		LevelSelectionUI.Load( game, ( ui ) =>
			{
				ui.OnIntroComplete += ( ui2 ) =>
					{
						Close( );
					};
			} );
	}
}
