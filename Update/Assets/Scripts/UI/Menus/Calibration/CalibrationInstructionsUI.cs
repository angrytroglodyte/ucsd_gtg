﻿
using System;
using UnityEngine;

public class CalibrationInstructionsUI : NMN.BaseUI
{
	public static void Load( CalibrationUI calibration )
	{
		NMUILoader.LoadUI<CalibrationInstructionsUI>( "calibration_instructions_ui", ( self ) =>
		{
			self.calibration = calibration;
			self.intro.Run( );
		} );
	}

	[SerializeField] private NMN.NGUI.BaseTween intro = null;
	[SerializeField] private NMN.NGUI.BaseTween outro = null;

	private CalibrationUI calibration = null;

	public void Exit( )
	{
		outro.OnComplete += ( tween ) =>
		{
			calibration.Calibrate( );
			Close( );
		};

		outro.Run( );
	}
}
