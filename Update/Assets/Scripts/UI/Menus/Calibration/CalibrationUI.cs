﻿
using System;
using UnityEngine;

public class CalibrationUI : NMN.BaseUI
{
	public static void Load( )
	{
		NMUILoader.LoadUI<CalibrationUI>( "calibration_ui", ( self ) =>
		{
				self.Enter( );
		} );
	}

    //[SerializeField] private CalibrationGame calibration = null;
    //[SerializeField] private CalibCamera calibration;
    [SerializeField] private NMN.NGUI.BaseTween intro = null;
	[SerializeField] private Vector3[] startPositions = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outros = null;

	public void Recalibrate( )
	{
		//CalibrationInstructionsUI.Load( this );
	}

	public void Calibrate( )
	{
	/*	calibration.Run( ( ) =>
			{
				CalibrationResultsUI.Load( calibration, this );
			} );*/

	}

	public void Exit( )
	{
		Outro( );
	}

	private void Enter( )
	{
		var startPosition = startPositions[UnityEngine.Random.Range( 0, startPositions.Length )];
		intro.target.transform.localPosition = startPosition;

		intro.OnComplete += ( tween ) =>
		{
			//calibration.Setup( ( ) =>
			//	{
			//		CalibrationInstructionsUI.Load( this );
			//	} );
		};

		intro.Run( );
	}

	private void Outro( )
	{
		var outro = outros[UnityEngine.Random.Range( 0, outros.Length )];
		outro.OnComplete += ( tween ) =>
		{
			Close( );
		};

		outro.Run( );
	}
}
