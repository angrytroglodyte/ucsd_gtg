﻿
using System;
using UnityEngine;
using System.Collections.Generic;

public class CalibrationResultsUI : NMN.BaseUI
{
	public static void Load( CalibrationGame calibration, CalibrationUI root )
	{
		NMUILoader.LoadUI<CalibrationResultsUI>( "calibration_results_ui", ( self ) =>
		{
			self.calibration = calibration;
			self.root = root;
			self.Open( );
		} );
	}

	[SerializeField] private UILabel qualityLabel = null;

	[SerializeField] private List<float> errorThresholds = new List<float>{ 0.5f, 1.0f, 1.5f, float.MaxValue };
	[SerializeField] private string[] qualityLevels = new string[] { "perfect", "good", "poor", "invalid" };

	[SerializeField] private NMN.NGUI.BaseTween intro = null;
	[SerializeField] private NMN.NGUI.BaseTween outro = null;

	private CalibrationGame calibration = null;
	private CalibrationUI root = null;

	public void Recalibrate( )
	{
		Outro( ( ) =>
		{
			root.Recalibrate( );
		} );
	}

	public void Exit( )
	{
		Outro( ( ) =>
			{
				root.Exit( );
			} );
	}

	private void Open( )
	{
		intro.Run( );

		if ( calibration.Success )
		{
			var index = errorThresholds.FindIndex( ( error ) =>
			{
				return calibration.Error <= error;
			} );

			qualityLabel.text = qualityLevels[index];
		}
		else
		{
			qualityLabel.text = qualityLevels[qualityLevels.Length-1];
		}
	}

	private void Outro( Action onComplete = null )
	{
		outro.OnComplete += ( tween ) =>
		{
			if ( onComplete != null )
			{
				onComplete( );
			}

			Close( );
		};

		outro.Run( );
	}
}
