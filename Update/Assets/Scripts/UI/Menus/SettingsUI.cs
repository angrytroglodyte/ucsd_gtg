﻿
using System;
using System.Linq;
using UnityEngine;

public class SettingsUI : NMN.BaseUI
{
	public static void Load( )
	{
		NMUILoader.LoadUI( "settings_ui" );
	}

	[SerializeField] private UIButton recalibrateButton = null;
	[SerializeField] private UIButton cursorButton = null;

	[SerializeField] private UIToggle musicToggle = null;
	[SerializeField] private UIToggle sfxToggle = null;
	[SerializeField] private UIToggle cursorToggle = null;

	[SerializeField] private NMN.NGUI.BaseTween[] introTweens = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outroTweens = null;

	public void ToggleMusic( )
	{
		Audio.Music.Volume = musicToggle.value ? 1.0f : 0.0f;
	}

	public void ToggleSFX( )
	{
		Audio.SFX.Volume = sfxToggle.value ? 1.0f : 0.0f;
	}

	public void ToggleCursor( )
	{
		User.UseMouse = !cursorToggle.value;
		recalibrateButton.isEnabled = !User.UseMouse;
	}

	public void Recalibrate( )
	{
        CalibrationUI.Load( );
        //Application.LoadLevel("calib_scene");
	}

	public void Exit( )
	{
		if ( enabled )
		{
			enabled = false;

			foreach ( var tween in outroTweens )
			{
				tween.Run( );
			}

			outroTweens.Last( ).OnComplete += ( tween ) =>
				{
					Close( );
				};
		}
	}

	private void Awake( )
	{
		recalibrateButton.isEnabled = EyeTribeClient.IsActive && !User.UseMouse;
		cursorButton.isEnabled = EyeTribeClient.IsActive;

		musicToggle.value = ( Audio.Music.Volume > 0.5f );
		sfxToggle.value = ( Audio.SFX.Volume > 0.5f );
		cursorToggle.value = !User.UseMouse;

		foreach ( var tween in introTweens )
		{
			tween.Run( );
		}
	}
}
