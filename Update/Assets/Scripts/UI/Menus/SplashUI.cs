﻿
using System;
using System.Linq;
using UnityEngine;
using System.Collections;

public class SplashUI : NMN.BaseUI
{
	[SerializeField] private NMN.NGUI.BaseTween[] introTweens = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outroTweensA = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outroTweensB = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outroTweensC = null;

	private void Awake( )
	{
		NMN.Job.Make( WaitForEyeTribe( ) ).OnComplete += ( cancelled ) =>
			{
				Intro( );
			};
	}

	private IEnumerator WaitForEyeTribe( )
	{
		yield return null;

		while ( !EyeTribeClient.IsInitialised )
		{
			yield return null;
		}

		yield break;
	}

	private void Intro( )
	{
		foreach ( var tween in introTweens )
		{
			tween.Run( );
		}

		introTweens.Last( ).OnComplete += ( intro ) =>
			{
				NMN.Job.Make( NMN.Job.Delay( 2.0f ) ).OnComplete += ( cancelled ) =>
					{
						foreach ( var tween in outroTweensA )
						{
							tween.Run( );
						}

						outroTweensA.Last( ).OnComplete += OnMonkeyClosed;
					};
			};
	}

	private void OnMonkeyClosed( NMN.NGUI.BaseTween finalTween )
	{
		LoginUI.Open( ( ) =>
			{
				foreach ( var tween in outroTweensB )
				{
					tween.Run( );
				}

				outroTweensB.Last( ).OnComplete += ( outroB ) =>
				{
					GameSelectionUI.Load( ( ui ) =>
					{
						foreach ( var tween in outroTweensC )
						{
							tween.Run( );
						}

						outroTweensC.Last( ).OnComplete += ( outroC ) =>
						{
							Close( );
						};
					} );
				};
			} );
	}
}
