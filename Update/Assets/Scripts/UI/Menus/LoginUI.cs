﻿
using System;
using System.Linq;
using UnityEngine;

public class LoginUI : NMN.BaseUI
{
	public static void Open( Action onComplete )
	{
		NMUILoader.LoadUI<LoginUI>( "login_ui", ( ui ) =>
			{
				ui.onComplete = onComplete;
			} );
	}

	[SerializeField] private UILabel username = null;
	[SerializeField] private NMN.NGUI.BaseTween[] introTweens = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outroTweens = null;

	private Action onComplete = null;

	public void PlayAsUser( )
	{
		if ( enabled )
		{
			User.Setup( GTG.Config.Username );
			Outro( );
		}
	}

	public void PlayAsGuest( )
	{
		if ( enabled )
		{
			User.Setup( Guid.NewGuid( ).ToString( ) );
			Outro( );
		}
	}

	private void Awake( )
	{
		username.text = string.Format( username.text, GTG.Config.Username );

		foreach ( var tween in introTweens )
		{
			tween.Run( );
		}
	}

	private void Outro( )
	{
		enabled = false;

		foreach ( var tween in outroTweens )
		{
			tween.Run( );
		}

		outroTweens.Last( ).OnComplete += ( tween ) =>
			{
				onComplete( );
				Close( );
			};

	}
}
