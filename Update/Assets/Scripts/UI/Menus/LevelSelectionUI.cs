﻿
using System;
using UnityEngine;

public class LevelSelectionUI : NMN.BaseUI
{
	public event Action<LevelSelectionUI> OnIntroComplete;

	private static GTG.Game targetGame = null;

	public static void Load( GTG.Game game, Action<LevelSelectionUI> onComplete )
	{
		targetGame = game;

		NMUILoader.LoadUI<LevelSelectionUI>( "level_selection_ui", ( self ) =>
			{
				self.game.LoadLevelSelectIcon( ( texture ) =>
					{
						self.logo.mainTexture = texture;
						self.Enter( );

						if ( onComplete != null )
						{
							onComplete( self );
						}
					} );
			} );
	}

	[SerializeField] private GameObject buttonPrefab = null;
	[SerializeField] private UITexture logo = null;
	[SerializeField] private UIScrollView scrollView = null;
	[SerializeField] private UIGrid buttonsRoot = null;
	[SerializeField] private NMN.NGUI.BaseTween intro = null;
	[SerializeField] private Vector3[] startPositions = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outros = null;

	private GTG.Game game = null;
	private GTG.GameSave save = null;

	public void Exit( )
	{
		GameSelectionUI.Load( ( ui ) =>
		{
			Outro( );
		} );
	}

	private void Enter( )
	{
		var startPosition = startPositions[UnityEngine.Random.Range( 0, startPositions.Length )];
		intro.target.transform.localPosition = startPosition;

		intro.OnComplete += ( tween ) =>
			{
				if ( OnIntroComplete != null )
				{
					OnIntroComplete( this );
				}
			};

		intro.Run( );
	}

	private void Awake( )
	{
		game = targetGame;
		targetGame = null;

		save = GTG.GameSave.Load( game.SceneName );
	}

	private void Start( )
	{
		scrollView.ResetPosition( );

		for ( int i = 0; i < GTG.GameSave.MaxLevels; ++i )
		{
			var level = i + 1;

			var completed = level <= ( save.LastCompleteLevel );
			var locked = level > ( save.LastCompleteLevel + 1 );

			var buttonInstance = NGUITools.AddChild( buttonsRoot.gameObject, buttonPrefab );

			var button = buttonInstance.GetComponent<LevelButton>( );
			button.Display( level, completed, locked );

			button.OnSelected += OnLevelSelected;
		}

		buttonsRoot.Reposition( );
		scrollView.ResetPosition( );
	}

	private void OnLevelSelected( int level )
	{
		LoadingScreenUI.Load( game, level, true, ( ui ) =>
			{
				Outro( );
			} );
	}

	private void Outro( )
	{
		var outro = outros[UnityEngine.Random.Range( 0, outros.Length )];
		outro.OnComplete += ( tween ) =>
		{
			Close( );
		};

		outro.Run( );
	}
}
