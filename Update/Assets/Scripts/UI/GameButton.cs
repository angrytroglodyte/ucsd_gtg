﻿
using System;
using UnityEngine;

public class GameButton : NMN.Behaviour
{
	public event Action<GTG.Game> OnSelected;

	[SerializeField] private UITexture logo = null;

	private GTG.Game game = null;

	public void Display( GTG.Game data, Texture2D logo )
	{
		game = data;
		this.logo.mainTexture = logo;
	}

	public void OnClick( )
	{
		if ( OnSelected != null )
		{
			OnSelected( game );
		}
	}
}
