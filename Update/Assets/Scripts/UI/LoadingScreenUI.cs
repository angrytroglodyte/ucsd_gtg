﻿
using System;
using UnityEngine;

public class LoadingScreenUI : NMN.BaseUI
{
	private static GTG.Game targetGame = null;
	private static int targetLevel = 0;
	private static Action<LoadingScreenUI> onCompleteCallback = null;
	private static bool shouldAutoLoad = false;
	private static bool loading = false;

	public static void Load( GTG.Game game, int level, bool autoLoad, Action<LoadingScreenUI> onComplete )
	{
		if ( !loading )
		{
			targetGame = game;
			targetLevel = level;
			onCompleteCallback = onComplete;
			shouldAutoLoad = autoLoad;
			loading = true;

			NMUILoader.LoadUI( "loading_screen_ui" );
		}
	}

	[SerializeField] private UITexture splash = null;
	[SerializeField] private NMN.NGUI.BaseTween intro = null;
	[SerializeField] private Vector3[] startPositions = null;
	[SerializeField] private NMN.NGUI.BaseTween[] outros = null;

	public void LoadLevel( )
	{
		GTG.BaseGame.LoadGame( targetGame, targetLevel, null );

		onCompleteCallback = null;
		targetGame = null;
		targetLevel = 0;
	}

	private void Enter( )
	{
		var startPosition = startPositions[UnityEngine.Random.Range( 0, startPositions.Length )];
		intro.target.transform.localPosition = startPosition;

		intro.OnComplete += ( tween ) =>
		{
			if ( onCompleteCallback != null )
			{
				onCompleteCallback( this );
			}

			if ( shouldAutoLoad )
			{
				LoadLevel( );
			}
		};

		intro.Run( );
	}

	private void GameLoaded( GTG.BaseGame game )
	{
		Outro( );
	}

	private void Outro( )
	{
		var outro = outros[UnityEngine.Random.Range( 0, outros.Length )];
		outro.OnComplete += ( tween ) =>
		{
			Close( );
		};

		outro.Run( );
	}

	private void Awake( )
	{
		GTG.BaseGame.OnGameReady += GameLoaded;
		OnClose += ( self ) =>
			{
				loading = false;
			};

		targetGame.LoadSplashScreen( ( texture ) =>
			{
				splash.mainTexture = texture;

				Enter( );
			} );
	}

	private void OnDestroy( )
	{
		GTG.BaseGame.OnGameReady -= GameLoaded;
	}
}
