﻿
using UnityEngine;

public class SyncUserPoints : NMN.Behaviour
{
	[SerializeField] private UILabel pointsLabel = null;

	private void Awake( )
	{
		pointsLabel.text = User.Points.ToString( );
		User.OnPointsChanged += OnPointsChanged;
	}

	private void OnDestroy( )
	{
		User.OnPointsChanged -= OnPointsChanged;
	}

	private void OnPointsChanged( int points, int delta )
	{
		pointsLabel.text = points.ToString( );
	}
}
