﻿
using UnityEngine;

public class SyncUserEarnings : NMN.Behaviour
{
	[SerializeField]
	private UILabel pointsLabel = null;

	private void Awake( )
	{
		var cents = TimeTracking.Cents;
		pointsLabel.text = string.Format( "{0}.{1:00}", cents / 100, cents % 100 );
	}
}
