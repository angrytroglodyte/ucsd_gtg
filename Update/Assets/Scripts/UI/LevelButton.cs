﻿
using System;
using UnityEngine;

public class LevelButton : NMN.Behaviour
{
	public event Action<int> OnSelected;

	[SerializeField] private UILabel text = null;
	[SerializeField] private GameObject locked = null;
	[SerializeField] private GameObject unlocked = null;
	[SerializeField] private GameObject complete = null;

	private int level = 0;

	public void Display( int level, bool completed, bool locked )
	{
		this.level = level;
		text.text = level.ToString( );

		NGUITools.SetActive( this.locked, locked );
		
		if ( !locked )
		{
			NGUITools.SetActive( this.unlocked, !completed );
			NGUITools.SetActive( this.complete, completed );
		}
	}

	public void OnClick( )
	{
		if ( OnSelected != null )
		{
			OnSelected( level );
		}
	}
}
