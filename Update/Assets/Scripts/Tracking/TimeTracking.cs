﻿
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

using Newtonsoft.Json;

public class TimeTracking : NMN.Singleton<TimeTracking>
{
	[JsonObject( MemberSerialization.OptIn )]
	public class Day
	{
		[JsonProperty] public DateTime date = DateTime.Today;
		[JsonProperty] public double minutesPlayed = 0.0;

		public int Minutes { get { return (int)minutesPlayed; } }
		public int Blocks { get { return Mathf.Min( Instance.maxBlocksPerDay, Minutes / Instance.minutesPerBlock ); } }

		public static Day Make( DateTime date )
		{
			var day = new Day( );
			day.date = date;
			return day;
		}
	}

	[JsonObject( MemberSerialization.OptIn )]
	public class Week
	{
		[JsonProperty] public DateTime first;
		[JsonProperty] public DateTime last;
		[JsonProperty] public List<Day> days = null;

		public int Blocks
		{
			get { return Mathf.Min( Instance.maxBlocksPerWeek, days.Aggregate( 0, ( total, day ) => total + day.Blocks ) ); }
		}

		public bool Contains( DateTime date )
		{
			return ( date >= first ) && ( date <= last );
		}

		public static Week Make( DateTime firstDay )
		{
			var week = new Week( );
			week.first = firstDay;
			week.last = firstDay.AddDays( 6 );
			week.days = new List<Day>( );

			return week;
		}
	}

	public static int Blocks
	{
		get
		{
			return Instance.weeks.Aggregate( 0, ( total, week ) => total + week.Blocks );
		}
	}

	public static int Cents { get { return Instance.centsPerBlock * Blocks; } }
	public static Day Today { get { return Instance.CurrentDay; } }

	private Day CurrentDay
	{
		get
		{
			var today = DateTime.Today;
			var week = weeks.Count > 0 ? weeks[weeks.Count - 1] : null;

			//	Very, very first week
			if ( week == null )
			{
				week = Week.Make( today );
				weeks.Add( week );
			}
			else if ( today < week.first )
			{
				throw new System.ArgumentOutOfRangeException( "We've travelled back in time... that's typically a bad sign." );
			}
			else
			{
				while ( !week.Contains( today ) )
				{
					//	Today is not in the latest week!
					//	Keep adding new weeks until it is.

					week = Week.Make( week.last.AddDays( 1 ) );
					weeks.Add( week );
				}
			}

			//	week is now a valid week which contains today... or it's null and bad things have happened.
			//	Assuming latest day, so the end of the list should be today!
			var day = week.days.LastOrDefault( stored => stored.date == today );
			if ( day == null )
			{
				//	No data stored for today =[
				//	So make some!
				day = Day.Make( today );
				week.days.Add( day );
			}

			//	We found it!
			return day;
		}
	}

	[SerializeField] private int minutesPerBlock = 30;
	[SerializeField] private int centsPerBlock = 400;
	[SerializeField] private int maxBlocksPerDay = 2;
	[SerializeField] private int maxBlocksPerWeek = 10;

	private DateTime gameStart = DateTime.UtcNow;
	private List<Week> weeks = null;
	private string username = string.Empty;

	private void OnStartTracking( )
	{
		gameStart = DateTime.UtcNow;
	}

	private void OnStopTracking( )
	{
		var elapsed = DateTime.UtcNow - gameStart;
		CurrentDay.minutesPlayed += elapsed.TotalMinutes;

		Debug.Log( "LOGGED TIME: " + elapsed.TotalMinutes );
		Save( );
	}

	private void OnGameReady( GTG.BaseGame game )
	{
		game.OnGameStart += OnStartTracking;
		game.OnGameComplete += OnStopTracking;
	}

	private void OnLoggedIn( )
	{
		username = User.Username;

		Load( );
	}

	private string FilePath( )
	{
		var path = Path.Combine( GTG.Config.AnalyticsFolder, username );
		return Path.Combine( path, "time.json" );
	}

	private void Save( )
	{
		var path = FilePath( );
		var data = JsonConvert.SerializeObject( weeks, Formatting.Indented );

		Directory.CreateDirectory( Path.GetDirectoryName( path ) );
		File.WriteAllText( path, data );
	}

	private void Load( )
	{
		var path = FilePath( );
		if ( File.Exists( path ) )
		{
			weeks = JsonConvert.DeserializeObject<List<Week>>( File.ReadAllText( path ) );
		}
		else
		{
			weeks = new List<Week>( );
		}
	}

	protected override void Awake()
	{
		base.Awake( );
		GTG.BaseGame.OnGameReady += OnGameReady;
		User.OnLoggedIn += OnLoggedIn;
		PauseMenuPopup.OnResume += OnStartTracking;
		PauseMenuPopup.OnPause += OnStopTracking;
	}

	private void OnDestroy( )
	{
		if ( weeks != null )
		{
			Save( );
		}

		PauseMenuPopup.OnPause -= OnStopTracking;
		PauseMenuPopup.OnResume -= OnStartTracking;
		User.OnLoggedIn -= OnLoggedIn;
		GTG.BaseGame.OnGameReady -= OnGameReady;
	}
}
