﻿
using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace GTG
{
	[JsonObject( MemberSerialization.OptIn )]
	public class Session : IDisposable
	{
		public class Event
		{
			public double time = 0.0f;
			public string key = string.Empty;
			public object data = null;
		}

		public string Game { get { return game; } }

		[JsonProperty] private string game = string.Empty;
		[JsonProperty] private int level = 0;
		[JsonProperty("date")] private DateTime start;

		private bool running = false;
		private bool disposed = false;

		[JsonProperty] private Dictionary<string, object> metadata = new Dictionary<string, object>( );
		[JsonProperty] private List<Event> events = new List<Event>( );

		public Session( string game, int level )
		{
			this.game = game;
			this.level = level;
		}

		public void AddMetaData( string key, object data = null )
		{
			if ( disposed )
			{
				throw new ObjectDisposedException( "Session", game );
			}

			metadata[key] = data;
		}

		public void AddEvent( string key, object data = null )
		{
			if ( disposed )
			{
				throw new ObjectDisposedException( "Session", game );
			}

			if ( !running )
			{
				throw new InvalidOperationException( string.Format( "Session not running: {0}, {1}", game, key ) );
			}

			var e = new Event( );
			e.time = ( DateTime.UtcNow - start ).TotalSeconds;
			e.key = key;
			e.data = data;

			events.Add( e );
		}

		public void Start( )
		{
			start = DateTime.UtcNow;
			running = true;

			AddEvent( "Start" );
		}

		public void Dispose( )
		{
			disposed = true;

			Analytics.Write( this );
		}
	}

	public static class Analytics
	{
		private static string UniquePath( string game )
		{
			var path = Config.AnalyticsFolder;
			path = Path.Combine( path, User.Username );
			path = Path.Combine( path, game );

			var file = string.Empty;
			var i = 0;
			do
			{
				file = Path.Combine( path, "session_" + ( i++ ) + ".json" );
			}
			while ( File.Exists( file ) );

			return file;
		}

		public static void Write( Session session )
		{
			var path = UniquePath( session.Game );
			var data = JsonConvert.SerializeObject( session, Formatting.Indented );

			Directory.CreateDirectory( Path.GetDirectoryName( path ) );
			File.WriteAllText( path, data );
		}
	}
}

public static class VectorSerializationHelper
{
	public static object Serialize( this Vector3 self )
	{
		return new { x = self.x, y = self.y, z = self.z };
	}
}