﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Invoker : MonoBehaviour {

    static List<Action> invocations = new List<Action>();

    static Invoker instance;
    static object lockItem = new object();
    void Awake()
    {
        instance = this;
    }

    static public void InvokeOnMainThread(Action act)
    {
        lock (lockItem)
            invocations.Add(act);
    }

    void Update()
    {
        lock (lockItem)
        {
            for (int i = 0; i < invocations.Count; i++) invocations[i]();
            invocations.Clear();
        }
    }
}
