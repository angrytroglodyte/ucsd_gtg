﻿Shader "GTG/Bob/God Rays"
{
	Properties
	{
		_MainTex ("Particle Texture", 2D) = "white" {}
		_FlickerParams ("Flicker Offset (XY), Scaling (ZW)", Vector) = (0,0,5,1)
	}

	Category
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	
		Blend SrcAlpha One
		AlphaTest Greater .01
		ColorMask RGB
		Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
	
		// ---- Fragment program cards
		SubShader
		{
			Pass {
		
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma multi_compile_particles

				#include "UnityCG.cginc"
			
				struct appdata_t
				{
					float4 vertex : POSITION;
					fixed4 color : COLOR;
					half2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : POSITION;
					fixed4 color : COLOR;
					half2 texcoord : TEXCOORD0;
					half2 flicker : TEXCOORD1;
				};
			
				sampler2D _MainTex;
				float4 _MainTex_ST;
				half4 _FlickerParams;

				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
					o.color = v.color;
					o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);

					const half2 timeScaling = half2(1,0.5);
					o.flicker = ( o.texcoord.yy * _FlickerParams.zw ) + _FlickerParams.xy + ( _Time.yy * timeScaling );
					return o;
				}
			
				fixed4 frag (v2f i) : COLOR
				{				
					const half pi = 3.1415;
					half2 flicker = 0.9 + ( sin( i.flicker * pi ) * 0.1 );
					return i.color * flicker.x * flicker.y * tex2D(_MainTex, i.texcoord);
				}
				ENDCG 
			}
		}
	}
}
