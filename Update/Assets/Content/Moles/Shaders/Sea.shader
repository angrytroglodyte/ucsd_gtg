﻿Shader "GTG/Sea"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_MainTexB ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Offset ("Scroll Offset", Vector) = (1,1,1,1)
		_Scroll ("Scroll Speed", Vector) = (1,1,1,1)
	}

	SubShader
	{
		Tags {"Queue"="Transparent-1" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert alpha

		sampler2D _MainTex;
		sampler2D _MainTexB;
		fixed4 _Color;
		float4 _Offset;
		float4 _Scroll;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_MainTexB;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			const float pi = 3.1415;
			float offset = sin( ( _Time.xx + _Offset.xy ) * pi ) * _Scroll.xy;
			float offsetB = sin( ( _Time.xx + _Offset.zw ) * pi ) * _Scroll.zw;

			fixed4 c = lerp( tex2D(_MainTex, IN.uv_MainTex + offset), tex2D(_MainTexB, IN.uv_MainTexB + offsetB), 0.5 ) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		ENDCG
	}
}
