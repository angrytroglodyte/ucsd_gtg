Shader "NMN/Split/tk2d/LitPremulVertexColor" 
{
	Properties 
	{
	    _MainTex ("Base (RGB)", 2D) = "white" {}
	    _AlphaTex ("Alpha (RGB)", 2D) = "black" {}
	}
	
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		ZWrite Off Blend One OneMinusSrcAlpha Cull Off Fog { Mode Off }
		LOD 110

		CGPROGRAM
		#pragma surface surf Lambert alpha
		struct Input {
			float2 uv_MainTex;
			fixed4 color : COLOR;
		};
		sampler2D _MainTex;
		sampler2D _AlphaTex;
		void surf(Input IN, inout SurfaceOutput o)
		{
			fixed4 mainColor = fixed4( tex2D(_MainTex, IN.uv_MainTex).rgb, tex2D(_AlphaTex, IN.uv_MainTex).g ) * IN.color;
			o.Albedo = mainColor.rgb;
			o.Alpha = mainColor.a;
		}
		ENDCG		
	}

	Fallback "NMN/Split/tk2d/PremulVertexColor", 1
}
