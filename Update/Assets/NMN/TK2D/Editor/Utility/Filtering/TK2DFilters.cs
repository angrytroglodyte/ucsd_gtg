﻿
using System.IO;
using UnityEngine;
using UnityEditor;

namespace NMN.Editor
{
	public class MaterialFromSpriteCollectionFilter : FilterBase<Material>
	{
		public override bool IsValid( UnityEngine.Object selected )
		{
			var gameObject = selected as GameObject;
			if ( gameObject == null )
			{
				return false;
			}
			
			return gameObject.GetComponent<tk2dSpriteCollection>( ) != null;
		}
		
		public override Material[] Convert( UnityEngine.Object selected )
		{
			return (selected as GameObject).GetComponent<tk2dSpriteCollection>( ).spriteCollection.materials;
		}
	}
}