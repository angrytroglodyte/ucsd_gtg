﻿
using System.IO;
using UnityEngine;
using UnityEditor;

namespace NMN.Editor
{
	public class MaterialFromUIAtlasFilter : FilterBase<Material>
	{
		public override bool IsValid( UnityEngine.Object selected )
		{
			var gameObject = selected as GameObject;
			if ( gameObject == null )
			{
				return false;
			}

			return gameObject.GetComponent<UIAtlas>( ) != null;
		}
		
		public override Material[] Convert( UnityEngine.Object selected )
		{
			return new [] { (selected as GameObject).GetComponent<UIAtlas>( ).spriteMaterial };
		}
	}
}