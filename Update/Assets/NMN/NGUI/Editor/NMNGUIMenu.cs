﻿
using UnityEngine;
using UnityEditor;
using System.Linq;

public static partial class NMMenu
{
	#region Create Tools

	[MenuItem( "NMN/Create/2D UI", true, RootID + 15 )]
	private static bool Create2DUIValidator( )
	{
		if ( UIRoot.list.Count == 0 || UICamera.list.size == 0 ) return true;
		foreach ( UICamera c in UICamera.list )
			if ( NGUITools.GetActive( c ) && c.camera.isOrthoGraphic )
				return false;
		return true;
	}

	[MenuItem( "NMN/Create/2D UI", false, RootID + 15 )]
	public static void Create2DUI( )
	{
		var root = UICreateNewUIWizard.CreateNewUI( UICreateNewUIWizard.CameraType.Simple2D );
		root.AddComponent<NMUILoader>( );
	}

	[MenuItem( "NMN/Create/3D UI", true, RootID + 15 )]
	private static bool Create3DUIValidator( )
	{
		if ( UIRoot.list.Count == 0 || UICamera.list.size == 0 ) return true;
		foreach ( UICamera c in UICamera.list )
			if ( NGUITools.GetActive( c ) && !c.camera.isOrthoGraphic )
				return false;
		return true;
	}

	[MenuItem( "NMN/Create/3D UI", false, RootID + 15 )]
	public static void Create3DUI( )
	{
		var root = UICreateNewUIWizard.CreateNewUI( UICreateNewUIWizard.CameraType.Advanced3D );
		root.AddComponent<NMUILoader>( );
	}

	#endregion
}
