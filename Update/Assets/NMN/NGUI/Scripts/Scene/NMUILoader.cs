﻿
using System;
using UnityEngine;
using System.Collections.Generic;

public class NMUILoader : NMN.Singleton<NMUILoader>
{
	public static void LoadUI<T>( string scene, Action<T> onComplete ) where T : NMN.BaseUI
	{
		LoadUI( scene, ( ui ) =>
			{
				onComplete( ui as T );
			} );
	}

	public static void LoadUI( string scene, Action<NMN.BaseUI> onComplete = null )
	{
		if ( !Instance.loading )
		{
			Instance.loading = true;

			NMN.SceneLoader.LoadAdditive( scene, ( ui ) =>
				{
					Instance.loading = false;
					Instance.AddUI( ui, onComplete );
				} );
		}
	}

	public static UICamera Camera { get { return Instance.camera; } }

	[SerializeField] private new UICamera camera = null;
	[SerializeField] private NMN.BaseUI initialUI = null;

	private bool loading = false;
	private List<NMN.BaseUI> loadedUI = new List<NMN.BaseUI>( );
	private List<UIPanel> panels = new List<UIPanel>( );

	private void AddUI( GameObject uiContainer, Action<NMN.BaseUI> onComplete )
	{
		var transform = uiContainer.transform;
		transform.parent = camera.transform;
		transform.localScale = Vector3.one;

		var uiRoot = uiContainer.GetComponent<NMN.BaseUI>( );
		uiRoot.OnClose += RemoveUI;

		loadedUI.Add( uiRoot );
		panels.Clear( );

		loadedUI.Sort( ( lhs, rhs ) =>
			{
				return lhs.Priority.CompareTo( rhs.Priority );
			} );

		foreach ( var ui in loadedUI )
		{
			var containedPanels = ui.GetComponentsInChildren<UIPanel>( );
			Array.Sort( containedPanels, ( lhs, rhs ) =>
				{
					return lhs.depth.CompareTo( rhs.depth );
				} );

			panels.AddRange( containedPanels );
		}

		var nextDepth = 0;	// UIPanel.nextUnusedDepth;
		foreach ( var panel in panels )
		{
			panel.depth = nextDepth++;
		}

		uiRoot.OnSpawn( camera );

		if ( onComplete != null )
		{
			onComplete( uiRoot );
		}
	}

	private void RemoveUI( NMN.BaseUI ui )
	{
		loadedUI.Remove( ui );
	}

	protected override void OnSpawned( )
	{
		base.OnSpawned( );

		if ( initialUI != null )
		{
			AddUI( initialUI.gameObject, null );
		}
	}
}