﻿
using System;
using UnityEngine;

namespace NMN
{
	public abstract class BaseUI : Behaviour
	{
		public event Action<BaseUI> OnClose;

		public int Priority { get { return priority; } }

		[SerializeField] private int priority = 0;

		public virtual void OnSpawn( UICamera camera ) { }

		public void Close( )
		{
			if ( OnClose != null )
			{
				OnClose( this );
			}

			Destroy( gameObject );
		}
	}
}
