﻿
using UnityEngine;
using System.Collections;

public class NMDestroyCallback : NMN.Behaviour
{
    public void Destroy( )
    {
        NGUITools.SetActive( gameObject, false );
        Destroy( gameObject );
    }
}
