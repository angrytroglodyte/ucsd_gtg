﻿
using UnityEngine;

public class NMEventEcho : NMN.Behaviour
{
	void OnHover( bool isOver )
	{
		Debug.Log( "OnHover( " + isOver + " )", this );
	}

	void OnPress( bool pressed )
	{
		Debug.Log( "OnPress( " + pressed + " )", this );
	}

	void OnClick( )
	{
		Debug.Log( "OnClick( )", this );
	}

	void OnDoubleClick( )
	{
		Debug.Log( "OnDoubleClick( )", this );
	}

	void OnSelect( bool selected )
	{
		Debug.Log( "OnSelect( " + selected + " )", this );
	}

	void OnDrag( Vector2 delta )
	{
		Debug.Log( "OnDrag( " + delta + " )", this );
	}

	void OnDrop( GameObject go )
	{
		Debug.Log( "OnDrop( " + go + " )", this );
	}

	void OnSubmit( )
	{
		Debug.Log( "OnSubmit( )", this );
	}

	void OnScroll( float delta )
	{
		Debug.Log( "OnScroll( " + delta + " )", this );
	}
}
