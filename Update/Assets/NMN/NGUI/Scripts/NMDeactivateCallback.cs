﻿
using UnityEngine;
using System.Collections;

public class NMDeactivateCallback : NMN.Behaviour
{
    public void Deactivate( )
    {
        NGUITools.SetActive( gameObject, false );
    }
}
