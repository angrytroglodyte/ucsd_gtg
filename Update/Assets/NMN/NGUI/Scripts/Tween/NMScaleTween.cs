﻿
using UnityEngine;

public class NMScaleTween : NMN.NGUI.BaseTween
{
	public Vector3 to = Vector3.one;

	public override void Run( )
	{
		var tween = TweenScale.Begin( target, duration, to );
		Configure( tween );
	}
}
