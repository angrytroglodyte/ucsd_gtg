﻿
using System;
using UnityEngine;

namespace NMN.NGUI
{
	public abstract class BaseTween : NMN.Behaviour
	{
		public event Action<BaseTween> OnComplete;

		public GameObject target = null;
		public UITweener.Method method = UITweener.Method.Linear;
		public UITweener.Style style = UITweener.Style.Once;
		public AnimationCurve animationCurve = new AnimationCurve( new Keyframe( 0f, 0f, 0f, 1f ), new Keyframe( 1f, 1f, 1f, 0f ) );
		public bool ignoreTimeScale = true;
		public float delay = 0f;
		public float duration = 1f;
		public bool steeperCurves = false;
		public int tweenGroup = 0;

		public abstract void Run( );

		protected void Configure( UITweener tween )
		{
			tween.method = method;
			tween.style = style;
			tween.animationCurve = animationCurve;
			tween.ignoreTimeScale = ignoreTimeScale;
			tween.delay = delay;
			tween.duration = duration;
			tween.steeperCurves = steeperCurves;
			tween.tweenGroup = tweenGroup;

			tween.AddOnFinished( Complete );

			tween.PlayForward( );
		}

		private void Complete( )
		{
			if ( OnComplete != null )
			{
				OnComplete( this );
			}

			OnComplete = null;
		}
	}
}
