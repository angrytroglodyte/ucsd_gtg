﻿
using UnityEngine;

public class NMPositionTween : NMN.NGUI.BaseTween
{
	public Vector3 to = Vector3.zero;

	public override void Run( )
	{
		var tween = TweenPosition.Begin( target, duration, to );
		Configure( tween );
	}
}
