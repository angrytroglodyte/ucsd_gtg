
using System;

namespace NMN.Extensions
{
	public static class EnumExtensions
	{
		//  Y'know what I hate? No "where T : enum" filter in C#
		public static int ToInt<T>( this T value ) where T : struct, IComparable
		{
			return Convert.ToInt32( (object)value );
		}
		
		public static T ToEnum<T>( this int value ) where T : struct, IComparable
		{
			return (T)(object)value;
		}
		
		public static T ToEnum<T>( this string text) where T : struct, IComparable
		{
			return (T)Enum.Parse(typeof(T), text);
		}
		
		public static bool HasFlags<T>( this T self, T flags ) where T : struct, IComparable
		{
			int selfInt = self.ToInt( );
			int flagInt = flags.ToInt();
			
			return ( selfInt & flagInt ) != 0;
		}
		
		public static T AddFlag<T>(this T self, T flag) where T : struct, IComparable
		{
			return (self.ToInt() | flag.ToInt()).ToEnum<T>();
		}
		
		public static T RemoveFlag<T>(this T self, T flag) where T : struct, IComparable
		{
			return (self.ToInt() & ~flag.ToInt()).ToEnum<T>();
		}
		
		public static T SetFlag<T>(this T self, T flag, bool value) where T : struct, IComparable
		{
			return value ? self.AddFlag(flag) : self.RemoveFlag(flag);
		}
	}
}
