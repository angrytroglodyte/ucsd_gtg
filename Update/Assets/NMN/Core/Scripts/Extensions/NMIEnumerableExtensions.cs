﻿
using System;
using System.Linq;
using System.Collections.Generic;

namespace NMN.Extensions
{
	public static class IEnumerableExtensions
	{
		public static IEnumerable<T> Shuffle<T>( this IEnumerable<T> self )
		{
			return self.Shuffle( new Random( ) );
		}

		public static IEnumerable<T> Shuffle<T>( this IEnumerable<T> self, Random generator )
		{
			var list = self.ToList( );
			for ( int i = 0; i < list.Count; ++i )
			{
				int j = generator.Next( i, list.Count );
				yield return list[j];
				list[j] = list[i];
			}
		}
	}
}
