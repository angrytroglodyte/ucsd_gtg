
using System;
using System.Collections;
using UnityEngine;

namespace NMN.Extensions
{
	public static class AnimationCurveExtensions
	{
		public static IEnumerator Run( this AnimationCurve curve, Action<float> update, float startTime, float endTime, bool ignoreTimescale = false )
		{
			yield return null;

			float time = startTime;
			float end = curve.Evaluate( endTime );

			while ( time < endTime )
			{
				update( curve.Evaluate( time ) );

				time += ignoreTimescale ? NMN.RealTime.deltaTime : Time.deltaTime;

				yield return null;
			}

			update( end );

			yield break;
		}

		public static IEnumerator Run( this AnimationCurve curve, Action<float> update, bool ignoreTimescale = false )
		{
			Keyframe end = curve[curve.length - 1];

			var job = NMN.Job.Make( curve.Run( update, 0.0f, end.time, ignoreTimescale ) );
			while ( job.Running )
			{
				yield return null;
			}

			yield break;
		}
	}
}
