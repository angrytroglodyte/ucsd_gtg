
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NMN
{
	public class Job
	{
		public event Action<bool> OnComplete;
		
		public bool Running { get { return (parent != null) ? (parent.Running && running) : running; } private set { running = value; } }
		public bool Paused { get; private set; }
		
		private IEnumerator coroutine = null;
		private Job parent = null;
		private bool running = false;
		private bool killed = false;
		private Queue<Job> children = null;
		
		public static Job Make( IEnumerator coroutine ) { return new Job( coroutine ); }
		public static Job Make( IEnumerator coroutine, bool autoRun ) { return new Job( coroutine, autoRun ); }
		public static Job MakeNull( ) { return Make( NullTask( ) ); }
		public static Job MakeNull( bool autoRun ) { return Make( NullTask( ), autoRun ); }

		public static IEnumerator Delay( float time )
		{
			yield return new WaitForSeconds( time );
			yield break;
		}
		
		public Job( IEnumerator coroutine ) : this( coroutine, true ) {}
		
		public Job( IEnumerator coroutine, bool autoRun )
		{
			this.coroutine = coroutine;
			
			if ( autoRun )
			{
				Start( );
			}
		}

		public Job AddChild( IEnumerator coroutine )
		{
			var job = new Job( coroutine, false );
			return AddChild( job );
		}

		public Job AddChild( Job child )
		{
			if ( children == null )
			{
				children = new Queue<Job>( );
			}
			
			children.Enqueue( child );
			child.parent = this;
			return child;
		}
		
		public void Start( )
		{
			Running = true;
			JobManager.StartTask( DoWork( ) );
		}
		
		public IEnumerator StartAsCoroutine( )
		{
			Running = true;
			yield return JobManager.StartTask( DoWork( ) );
		}
		
		public void Pause( )
		{
			Paused = true;
		}
		
		public void Resume( )
		{
			Paused = false;
		}
		
		public void Kill( )
		{
			killed = true;
			Running = false;
			Paused = false;
		}
		
		private static IEnumerator NullTask( )
		{
			yield break;
		}
		
		private IEnumerator DoWork( )
		{
			yield return null;
			
			while ( Running )
			{
				if ( Paused )
				{
					yield return null;
				}
				else
				{
					if ( coroutine.MoveNext( ) )
					{
						yield return coroutine.Current;
					}
					else
					{
						if ( children != null )
						{
							yield return JobManager.StartTask( RunChildJobs( ) );
						}
						
						Running = false;
					}
				}
			}
			
			if ( OnComplete != null )
			{
				OnComplete( killed );
				OnComplete = null;
			}
		}
		
		private IEnumerator RunChildJobs( )
		{
			if ( children != null && children.Count > 0 )
			{
				do
				{
					var job = children.Dequeue( );
					yield return JobManager.StartTask( job.StartAsCoroutine( ) );
				}
				while ( children.Count > 0 && Running );
			}
		}
	}

	public class JobManager : Singleton<JobManager>
	{
		public static Coroutine StartTask( IEnumerator task )
		{
			return Instance.StartCoroutine( task );
		}
	}
}
