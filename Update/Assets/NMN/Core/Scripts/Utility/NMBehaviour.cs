
using System;
using UnityEngine;
using System.Collections;

namespace NMN
{
	public abstract class Behaviour : MonoBehaviour
	{
		/// <summary>
		/// Gets the first instance of a component of type T attached to this Game Object.
		/// Overwrites the built in Unity version, as for some reason this seems to be faster.
		/// </summary>
		/// <returns>The first instance of a component of type T attached to this Game Object.</returns>
		/// <typeparam name="T">The component type to retrieve.</typeparam>
		public new T GetComponent<T>( ) where T : Component
		{
			return NMN.Tools.GetComponent<T>( gameObject );
		}

		public T AddComponent<T>( ) where T : Component
		{
			return NMN.Tools.AddComponent<T>( gameObject );
		}

		public T GetOrAddComponent<T>( ) where T : Component
		{
			return NMN.Tools.GetOrAddComponent<T>( gameObject );
		}

		public NMN.Job Invoke( Action target, float delay )
		{
			var job = NMN.Job.Make( NMN.Job.Delay( delay ) );
			job.OnComplete += ( cancelled ) =>
				{
					if ( !cancelled )
					{
						target( );
					}
				};

			return job;
		}

		public NMN.Job InvokeRepeating( Action target, float delay, float frequency )
		{
			var job = NMN.Job.Make( NMN.Job.Delay( delay ) );
			job.AddChild( InvokeRepeatingImpl( target, frequency ) );
			return job;
		}

		private IEnumerator InvokeRepeatingImpl( Action target, float frequency )
		{
			var wait = new WaitForSeconds( frequency );
			for ( ; ; )
			{
				target( );
				yield return wait;
			}
		}
	}
}
