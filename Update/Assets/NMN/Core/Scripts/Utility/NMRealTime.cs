
using UnityEngine;

namespace NMN
{
	public class RealTime : Singleton<RealTime>
	{
		private const float MaxDelta = 1.0f;

		public static float time { get; private set; }
		public static float deltaTime { get; private set; }

		protected override void OnSpawned ()
		{
			time = Time.realtimeSinceStartup;
			deltaTime = 0.0f;
		}

		private void Update( )
		{
			var old = time;
			time = Time.realtimeSinceStartup;
			deltaTime = Mathf.Clamp( time - old, 0.0f, MaxDelta );
		}
	}
}
