
using System;
using UnityEngine;

namespace NMN
{
	public class Math
	{
		public static float LinearStep( float min, float max, float current )
		{
			return Mathf.Clamp01( ( current - min ) / ( max - min ) );
		}

		public static float SmoothStep( float t )
		{
			return t * t * ( 3.0f - ( 2.0f * t ) );
		}

		public static float SmoothStep( float min, float max, float current )
		{
			return SmoothStep( LinearStep( min, max, current ) );
		}

		public static float ScalarProjection( Vector3 a, Vector3 b )
		{
			return Vector3.Dot( a, b ) / b.magnitude;
		}

		public static Vector3 Project( Vector3 a, Vector3 b )
		{
			return b * ( Vector3.Dot( a, b ) / Vector3.Dot( b, b ) );
		}

		public static int Wrap( int current, int min, int max )
		{
			int range = max - min;
			while ( current >= max ) { current -= range; }
			while ( current < min ) { current += range; }
			return current;
		}

		public static float Wrap( float current, float min, float max )
		{
			float range = max - min;
			while ( current > max ) { current -= range; }
			while ( current < min ) { current += range; }
			return current;
		}
	}
}

