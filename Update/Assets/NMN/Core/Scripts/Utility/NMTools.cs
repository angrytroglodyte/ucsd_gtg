
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NMN
{
	public static class Tools
	{
		public static GameObject AddChild( Transform parent )
		{
			var child = new GameObject( "New Child" );

			var transform = child.transform;
			transform.parent = parent;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			transform.localScale = Vector3.one;

			return child;
		}

		public static GameObject AddChild( GameObject parent )
		{
			return AddChild( parent.transform );
		}

		public static T GetComponent<T>( GameObject go ) where T : Component
		{
			return (T)go.GetComponent( typeof( T ) );
		}

		public static T AddComponent<T>( GameObject go ) where T : Component
		{
			return (T)go.AddComponent( typeof( T ) );
		}

		public static T GetOrAddComponent<T>( GameObject go ) where T : Component
		{
			var t = GetComponent<T>( go );
			if ( t == null )
			{
				t = AddComponent<T>( go );
			}

			return t;
		}

		public static T FindObjectOfType<T>( ) where T : UnityEngine.Object
		{
			return (T)GameObject.FindObjectOfType( typeof(T) );
		}

		public static IEnumerable<T> EnumValues<T>( )
		{
			return (T[])Enum.GetValues( typeof( T ) );
		}

		static public IEnumerator ConfirmDestroy( GameObject target, Action onComplete = null )
		{
			yield return null;
			
			GameObject.Destroy( target );
			while ( target != null )
			{
				yield return null;
			}

			yield return null;
			
			if ( onComplete != null )
			{
				onComplete( );
			}
			
			yield break;
		}
	}
}
