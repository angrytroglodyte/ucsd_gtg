
using UnityEngine;
using System.Collections;

namespace NMN
{
	public class Singleton<T> : Behaviour where T : Singleton<T>
	{
		public static T Instance { get { Initialiser.Bind( ); return Initialiser.Instance; } }

		protected static class Initialiser
		{
			public static T Instance = null;

			static Initialiser( )
			{
				Instance = NMN.Tools.FindObjectOfType<T>( );
				if ( Instance == null )
				{
					var gameObject = new GameObject( );
					Instance = gameObject.AddComponent<T>( );
				}

				Instance.transform.parent = NMGlobalContainer.Container;
				Instance.name = typeof(T).FullName;
				Instance.OnSpawned( );
			}

			public static void Bind( )
			{
			}

			public static void Unbind( )
			{
				Instance = null;
			}
		}

		protected virtual void OnSpawned( ) { }

		protected virtual void Awake( )
		{
			Initialiser.Bind( );
		}

		protected virtual void OnApplicationQuit( )
		{
			Initialiser.Unbind( );
		}
	}
}