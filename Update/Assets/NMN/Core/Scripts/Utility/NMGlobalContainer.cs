
using UnityEngine;
using System.Collections;

[AddComponentMenu("NMN/Core/Utility/Global Container")]
public class NMGlobalContainer : NMN.Behaviour
{
	public static NMGlobalContainer Instance { get { Initialiser.Cache( ); return Initialiser.Instance; } }
	public static Transform Container { get { return Instance.transform; } }

	private static class Initialiser
	{
		public static NMGlobalContainer Instance = null;

		static Initialiser( )
		{
			Instance = NMN.Tools.FindObjectOfType<NMGlobalContainer>( );
			if ( Instance == null )
			{
				var gameObject = new GameObject( );
				Instance = gameObject.AddComponent<NMGlobalContainer>( );
			}
		}

		public static void Cache( )
		{
		}

		public static void Destroy( )
		{
			Instance = null;
		}
	}

	private void Awake( )
	{
		Initialiser.Cache( );
		name = "_globals";
	}

	private void OnApplicationQuit( )
	{
		Initialiser.Destroy( );
	}
}
