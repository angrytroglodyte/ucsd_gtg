
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NMN
{
	public class CachedBehaviour : Behaviour
	{
		private Dictionary<Type,Component> cache = new Dictionary<Type, Component>( );

		public void CacheType<T>( ) where T : Component
		{
			var type = typeof(T);
			cache[type] = GetComponent(type);
		}

		public T Get<T>( ) where T : Component
		{
			var type = typeof(T);
			Component output = null;
			if ( !cache.TryGetValue( type, out output ) )
			{
				output = GetComponent(type);
			}

			return (T)output;
		}
	}

	public class CachedBehaviour<T> : CachedBehaviour where T : Component
	{
		public void Cache( )
		{
			CacheType<T>( );
		}
	}
}
