﻿
using UnityEngine;

public class NMScaleShuriken : NMN.Behaviour
{
	[SerializeField] private ParticleSystem[] systems = null;

	private void Start( )
	{
		var scale = Mathf.Max( transform.localScale.x, transform.localScale.y, transform.localScale.z );

		foreach ( var system in systems )
		{
			system.startSpeed *= scale;
			system.startSize *= scale;
		}
	}
}
