﻿
using NMN;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NMN
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        private struct SceneLoadRequest
        {
            public bool additive;
            public string name;
            public string targetObject;
            public Action<GameObject> callback;
        }

        private Queue<SceneLoadRequest> requests = new Queue<SceneLoadRequest>( );
        private Job loader = null;

        public static void Load( string scene )
        {
            Load( scene, string.Empty, null );
        }

        public static void LoadAdditive( string scene )
        {
            LoadAdditive( scene, string.Empty, null );
        }

        public static void Load( string scene, Action<GameObject> callback )
        {
            Load( scene, scene, callback );
        }

        public static void LoadAdditive( string scene, Action<GameObject> callback )
        {
            LoadAdditive( scene, scene, callback );
        }

        public static void Load( string scene, string targetObject, Action<GameObject> callback )
        {
            Load( false, scene, targetObject, callback );
        }

        public static void LoadAdditive( string scene, string targetObject, Action<GameObject> callback )
        {
            Load( true, scene, targetObject, callback );
        }

        public static void Load( bool additive, string scene, string targetObject, Action<GameObject> callback )
        {
            var request = new SceneLoadRequest( ) { additive = additive, name = scene, targetObject = targetObject, callback = callback };
            Instance.requests.Enqueue( request );
            StartTask( );
        }

        private static void StartTask( )
        {
            if ( Instance.loader == null )
            {
                Instance.loader = Job.Make( LoaderTask( ) );
                Instance.loader.OnComplete += ( killed ) =>
                {
                    Instance.loader = null;

                    if ( Instance.requests.Count > 0 )
                    {
                        //  We must have added a new scene to load while the task was shutting down
                        //  Start it back up again
                        StartTask( );
                    }
                };
            }
        }

        private static IEnumerator LoaderTask( )
        {
            yield return null;

            while ( Instance.requests.Count > 0 )
            {
                var request = Instance.requests.Dequeue( );

                if ( request.additive )
                {
                    yield return Application.LoadLevelAdditiveAsync( request.name );
                }
                else
                {
                    yield return Application.LoadLevelAsync( request.name );
                }

                if ( !( request.callback == null || string.IsNullOrEmpty( request.targetObject ) ) )
                {
                    yield return null;

                    GameObject root = null;
                    for ( ; root == null; root = GameObject.Find( request.targetObject ) )
                    {
                        yield return null;
                    }

					yield return null;

                    request.callback( root );
                }
            }

            yield break;
        }
    }
}
