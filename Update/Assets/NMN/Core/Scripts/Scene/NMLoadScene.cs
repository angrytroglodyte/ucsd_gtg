﻿
using UnityEngine;

public class NMLoadScene : NMN.Behaviour
{
    public string scene = string.Empty;
    public bool additive = false;

    public void Load( )
    {
        NMN.SceneLoader.Load( additive, scene, string.Empty, null );
    }
}
