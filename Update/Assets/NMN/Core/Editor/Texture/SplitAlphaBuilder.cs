
using System.IO;
using UnityEngine;
using UnityEditor;

using NMN.Extensions;

namespace NMN.Editor.TextureTools
{
	public static class SplitAlphaBuilder
	{
		public static void Build( Texture2D texture )
		{
			Build( new Texture2D[] { texture } );
		}

		public static void Build( Texture2D[] textures )
		{
			EditorUtility.DisplayProgressBar( "Generating Alpha...", string.Empty, 0.0f );

			for ( int i = 0; i < textures.Length; ++i )
			{
				var min = (float)i / (float)textures.Length;
				var max = (float)(i+1) / (float)textures.Length;
				Split( textures[i], min, max );
			}

			EditorUtility.ClearProgressBar( );
		}

		private static void Split( Texture2D texture, float progressMin, float progressMax )
		{
			EditorUtility.DisplayProgressBar( "Generating Alpha from " + texture.name, "Caching source data.", progressMin );

			var rootPath = AssetDatabase.GetAssetPath( texture );
			var directory = FileSystem.ParentFolder( rootPath );
			var outputPath = Path.Combine( directory, texture.name + "_alpha.png" );

			var importer = TextureImporter.GetAtPath( rootPath ) as TextureImporter;
			var settings = new TextureImporterSettings( );
			importer.ReadTextureSettings( settings );

			var buildTarget = EditorUserBuildSettings.selectedBuildTargetGroup.ImporterString( );
			var hasPlatformOverride = false;
			var platformSize = 0;
			var platformFormat = TextureImporterFormat.AutomaticTruecolor;
			var platformCompression = 0;

			EditorUtility.DisplayProgressBar( "Generating Alpha from " + texture.name, "Setting up copy.", Mathf.Lerp( progressMin, progressMax, 0.25f ) );

			Editor.Tools.EditAssets( ( ) =>
			{
				var prefered = new TextureImporterSettings( );
				prefered.grayscaleToAlpha = false;
				prefered.lightmap = false;
				prefered.maxTextureSize = 4096;
				prefered.mipmapEnabled = false;
				prefered.normalMap = false;
				prefered.npotScale = TextureImporterNPOTScale.None;
				prefered.readable = true;
				prefered.textureFormat = TextureImporterFormat.AutomaticTruecolor;
				importer.SetTextureSettings( prefered );

				hasPlatformOverride = importer.GetPlatformTextureSettings( buildTarget, out platformSize, out platformFormat, out platformCompression );

				if ( hasPlatformOverride )
				{
					importer.ClearPlatformTextureSettings( buildTarget );
				}

				AssetDatabase.ImportAsset( rootPath, ImportAssetOptions.ForceUpdate );
				EditorUtility.SetDirty( texture );
			} );

			EditorUtility.DisplayProgressBar( "Generating Alpha from " + texture.name, "Cloning.", Mathf.Lerp( progressMin, progressMax, 0.5f ) );

			Editor.Tools.EditAssets( ( ) =>
			{
				var pixels = texture.GetPixels( );
				for ( int i = 0; i < pixels.Length; ++i )
				{
					pixels[i].r = pixels[i].g = pixels[i].b = pixels[i].a;
					pixels[i].a = 1.0f;
				}

				var alpha = new Texture2D(texture.width, texture.height, TextureFormat.RGB24, false, false);
				alpha.SetPixels( pixels );

				File.WriteAllBytes( outputPath, alpha.EncodeToPNG( ) );
			} );

			EditorUtility.DisplayProgressBar( "Generating Alpha from " + texture.name, "Re-apply original settings.", Mathf.Lerp( progressMin, progressMax, 0.75f ) );
			Editor.Tools.EditAssets( ( ) =>
			{
				var alphaImporter = TextureImporter.GetAtPath( FileSystem.FromSystemPath( outputPath ) ) as TextureImporter;

				importer.SetTextureSettings( settings );
				alphaImporter.SetTextureSettings( settings );
				
				if ( hasPlatformOverride )
				{
					importer.SetPlatformTextureSettings( buildTarget, platformSize, platformFormat, platformCompression );
				}

				foreach ( var target in NMN.Tools.EnumValues<BuildTargetGroup>( ) )
				{
					var targetName = target.ImporterString( );

					var targetSize = 0;
					var targetFormat = TextureImporterFormat.AutomaticTruecolor;
					var targetQuality = 0;
					bool overriden = importer.GetPlatformTextureSettings( targetName, out targetSize, out targetFormat, out targetQuality );
					if ( overriden )
					{
						alphaImporter.SetPlatformTextureSettings( targetName, targetSize, targetFormat, targetQuality );
					}
				}

				AssetDatabase.ImportAsset( rootPath, ImportAssetOptions.ForceUpdate );
				AssetDatabase.ImportAsset( FileSystem.FromSystemPath( outputPath ), ImportAssetOptions.ForceUpdate );
				EditorUtility.SetDirty( texture );
			} );

			EditorUtility.UnloadUnusedAssets( );

			EditorUtility.DisplayProgressBar( "Generating Alpha from " + texture.name, "Complete.", progressMax );
		}
	}
}