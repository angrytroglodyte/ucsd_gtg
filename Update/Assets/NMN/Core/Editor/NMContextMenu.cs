
using UnityEngine;
using UnityEditor;
using System.Linq;

using NMN.Editor.TextureTools;
using NMN.Editor.MaterialTools;

public static class NMContextMenu
{
	public const int RootID = 1000;
	
#region Transform Tools

	[MenuItem("CONTEXT/Transform/Reset Local Position", true, RootID)]
	public static bool ResetPositionValidator( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			return transform.localPosition != Vector3.zero;
		}

		return false;
	}

	[MenuItem("CONTEXT/Transform/Reset Local Position", false, RootID)]
	public static void ResetPosition( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			transform.localPosition = Vector3.zero;
		}
	}

	[MenuItem("CONTEXT/Transform/Reset Local Rotation", true, RootID)]
	public static bool ResetRotationValidator( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			return transform.localRotation.eulerAngles != Vector3.zero;
		}
		
		return false;
	}

	[MenuItem("CONTEXT/Transform/Reset Local Rotation", false, RootID)]
	public static void ResetRotation( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			transform.localRotation = Quaternion.identity;
		}
	}

	[MenuItem("CONTEXT/Transform/Reset Local Scale", true, RootID)]
	public static bool ResetScaleValidator( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			return transform.localScale != Vector3.one;
		}
		
		return false;
	}

	[MenuItem("CONTEXT/Transform/Reset Local Scale", false, RootID)]
	public static void ResetScale( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			transform.localScale = Vector3.one;
		}
	}

	[MenuItem("CONTEXT/Transform/Add Child", false, RootID+15)]
	public static void AddChild( MenuCommand command )
	{
		var transform = command.context as Transform;
		if ( transform != null )
		{
			NMN.Tools.AddChild( transform );
		}
	}

#endregion
}
