
using System;
using UnityEngine;
using UnityEditor;

namespace NMN.Editor
{
	public static class Tools
	{
		public static void EditAssets( Action edit )
		{
			AssetDatabase.StartAssetEditing( );
			{
				edit( );
			}
			AssetDatabase.StopAssetEditing( );
			AssetDatabase.SaveAssets( );
			AssetDatabase.Refresh( );
		}
	}
}
	