
using System.IO;
using UnityEngine;
using UnityEditor;

namespace NMN.Editor
{
	public class MaterialFromTexture2DFilter : FilterBase<Material>
	{
		public override bool IsValid( UnityEngine.Object selected )
		{
			return selected is Texture2D;
		}

		public override Material[] Convert( UnityEngine.Object selected )
		{
			var texture = selected as Texture2D;
			var material = new Material( Shader.Find( "Diffuse" ) );

			material.mainTextureOffset = Vector2.zero;
			material.mainTextureScale = Vector2.one;
			material.mainTexture = texture;

			var directory = Path.GetDirectoryName( AssetDatabase.GetAssetPath( texture ) );

			Editor.Tools.EditAssets( ( ) =>
			{
				AssetDatabase.CreateAsset( material, directory + "/" + texture.name + ".mat" );
			} );

			return new [] { material };
		}
	}
}