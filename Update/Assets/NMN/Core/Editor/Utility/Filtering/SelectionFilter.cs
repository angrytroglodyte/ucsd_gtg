
using System;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;

namespace NMN.Editor
{
	public abstract class FilterBase<T> where T : UnityEngine.Object
	{
		public abstract bool IsValid( UnityEngine.Object selected );
		public abstract T[] Convert( UnityEngine.Object selected );
	}

	public abstract class SimpleFilter<T> : FilterBase<T> where T : UnityEngine.Object
	{
		public override bool IsValid( UnityEngine.Object selected )
		{
			return selected is T;
		}

		public override T[] Convert( UnityEngine.Object selected )
		{
			return new [] { selected as T };
		}
	}

	public static class SelectionFilter<T> where T : UnityEngine.Object
	{
		private static List<FilterBase<T>> filters = GatherFilters( );

		private static List<FilterBase<T>> GatherFilters( )
		{
			var filters = new List<FilterBase<T>>( );

			var assembly = Assembly.GetExecutingAssembly( );
			var baseType = typeof(FilterBase<T>);

			foreach ( var type in assembly.GetTypes( ) )
			{
				if ( !type.IsClass || type.IsAbstract || !type.IsSubclassOf(baseType) )
				{
					continue;
				}

				var filter = (FilterBase<T>)Activator.CreateInstance( type );
				filters.Add( filter );
			}

			return filters;
		}

		public static bool All( )
		{
			return Selection.activeObject == null ? false : All( Selection.objects );
		}

		public static bool All( IEnumerable<UnityEngine.Object> collection )
		{
			return collection.All( Check );
		}

		public static bool Any( )
		{
			return Selection.activeObject == null ? false : Any( Selection.objects );
		}

		public static bool Any( IEnumerable<UnityEngine.Object> collection )
		{
			return collection.Any( Check );
		}

		public static IEnumerable<T> Convert( )
		{
			return Selection.activeObject == null ? null : Convert( Selection.objects );
		}

		public static IEnumerable<T> Convert( IEnumerable<UnityEngine.Object> collection )
		{
			var list = new List<T>( );
			foreach ( var entry in collection )
			{
				var filtered = ConvertEntry( entry );
				if ( filtered != null )
				{
					list.AddRange( filtered );
				}
			}

			return list;
		}

		private static bool Check( UnityEngine.Object entry )
		{
			return filters.Any( ( filter ) =>
			{
				return filter.IsValid( entry );
			} );
		}

		private static T[] ConvertEntry( UnityEngine.Object entry )
		{
			foreach ( var filter in filters )
			{
				if ( filter.IsValid( entry ) )
				{
					return filter.Convert( entry );
				}
			}

			return null;
		}
	}
}
