
using UnityEngine;

namespace NMN.Editor
{
	public class GameObjectFilter : SimpleFilter<GameObject> { }

	public class TextureFilter : SimpleFilter<Texture> { }
	public class Texture2DFilter : SimpleFilter<Texture2D> { }
	public class Texture3DFilter : SimpleFilter<Texture3D> { }
	public class CubemapFilter : SimpleFilter<Cubemap> { }
	public class RenderTextureFilter : SimpleFilter<RenderTexture> { }
	public class MovieTextureFilter : SimpleFilter<MovieTexture> { }
	public class WebCamTextureFilter : SimpleFilter<WebCamTexture> { }

	public class SpriteFilter : SimpleFilter<Sprite> { }

	public class FontFilter : SimpleFilter<Font> { }

	public class MaterialFilter : SimpleFilter<Material> { }
	public class ProceduralMaterialFilter : SimpleFilter<ProceduralMaterial> { }

	public class ShaderFilter : SimpleFilter<Shader> { }

	public class MeshFilter : SimpleFilter<Mesh> { }

	public class AudioClipFilter : SimpleFilter<AudioClip> { }

	public class PhysicsMaterialFilter : SimpleFilter<PhysicMaterial> { }
	public class Physics2DMaterialFilter : SimpleFilter<PhysicsMaterial2D> { }

	public class TextAssetFilter : SimpleFilter<TextAsset> { }
}
