
using System.IO;
using UnityEngine;
using UnityEditor;

namespace NMN
{
	public static class FileSystem
	{
		public static string ParentFolder( string path )
		{
			return new FileInfo( path ).DirectoryName;
		}

		public static string ToSystemPath( string path )
		{
			return new FileInfo( path ).FullName;
		}

		public static string FromSystemPath( string path )
		{
			path = path.Replace( '\\', '/' );
			var currentDirectoty = Directory.GetCurrentDirectory( ).Replace( '\\', '/' );
			return path.StartsWith( currentDirectoty ) ? path.Remove( 0, currentDirectoty.Length + 1 ) : path;
		}

		public static void CloneFile( string path, string clonedName )
		{
			var info = new FileInfo( path );
			path = info.FullName;
			var directory = info.DirectoryName;
			var extension = info.Extension;

			var output = Path.Combine( directory, clonedName + extension );
			File.Copy( path, output, true );
		}
	}
}