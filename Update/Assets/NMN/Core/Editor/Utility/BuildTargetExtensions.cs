
using UnityEngine;
using UnityEditor;

namespace NMN.Extensions
{
	public static class BuildTargetGroupExtension
	{
		public static string ImporterString( this BuildTargetGroup self )
		{
			string[] platforms = new string[]
			{
				"UNKNOWN",			//	Unknown
				"Standalone",		//	Standalone
				"Web",				//	WebPlayer
				"UNKNOWN - WII",	//	Wii
				"iPhone",			//	iPhone
				"UNKNOWN - PS3",	//	PS3
				"UNKNOWN - 360",	//	XBOX360
				"Android",			//	Android
				"UNKOWN",
				"UNKNOWN - GLES",	//	GLESEmu
				"UNKOWN",
				"UNKNOWN - NaCl",	//	NaCl
				"FlashPlayer",		//	FlashPlayer
				"UNKOWN",
				"UNKNOWN - Metro",
				"UNKNOWN - WP8",
				"UNKNOWN - BB10",
				"UNKNOWN - Tizen"
			};
			
			return platforms[(int)self];
		}
	}
}
