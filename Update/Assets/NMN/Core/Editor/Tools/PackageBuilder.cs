
using System.IO;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;

namespace NMN.Editor
{
	public class PackageBuilder : EditorWindow
	{
		private static string RootDirectory = "Assets/NMN";
		private static string PrefsPrefix = "NMN.Editor.PackageBuilder-";

		private static string PrefsKey( string key )
		{
			return PrefsPrefix + key;
		}

		private Vector2 scroll = Vector2.zero;
		private SortedDictionary<string, string> libraries = new SortedDictionary<string, string>( );

		private bool AllSelected
		{
			get
			{
				return libraries.All( (library) =>
				{
					return EditorPrefs.GetBool( PrefsKey( library.Key ), false ); 
				} );
			}

			set
			{
				foreach ( var library in libraries )
				{
					EditorPrefs.SetBool( PrefsKey( library.Key ), value );
				}
			}
		}

		private bool AnySelected
		{
			get
			{
				return libraries.Any( (library) =>
				{
					return EditorPrefs.GetBool( PrefsKey( library.Key ), false ); 
				} );
			}
		}

		public static void Create( )
		{
			GetWindow<PackageBuilder>( false, "Package Builder", true ).Initialise( );
		}

		private void Initialise( )
		{
			Refresh( );
			Show( );
		}

		private void OnGUI( )
		{
			var refresh = false;
			var build = false;

			scroll = EditorGUILayout.BeginScrollView( scroll );
			{
				EditorGUILayout.Space( );

				var oldAll = AllSelected;
				var all = EditorGUILayout.ToggleLeft( "Select All", oldAll );
				if ( oldAll != all )
				{
					AllSelected = all;
				}

				EditorGUILayout.BeginVertical( "Box" );
				{
					if ( libraries.Count > 0 )
					{
						foreach ( var library in libraries )
						{
							EditorGUILayout.BeginHorizontal( );
							{
								var active = EditorPrefs.GetBool( PrefsKey( library.Key ), false );
								active = EditorGUILayout.ToggleLeft( library.Key, active );
								EditorPrefs.SetBool( PrefsKey( library.Key ), active );

								EditorGUILayout.LabelField( library.Value );
								GUILayout.FlexibleSpace( );
							}
							EditorGUILayout.EndHorizontal( );
						}
					}
					else
					{
						EditorGUILayout.LabelField( "No libraries detected. Try Refreshing the list." );
					}
				}
				EditorGUILayout.EndVertical( );

				EditorGUILayout.BeginHorizontal( );
				{
					GUILayout.FlexibleSpace( );

					refresh = GUILayout.Button( "Refresh", EditorStyles.miniButtonLeft, GUILayout.Width( 100.0f ) );

					EditorGUI.BeginDisabledGroup( !AnySelected );
					{
						build = GUILayout.Button( "Build", EditorStyles.miniButtonRight, GUILayout.Width( 100.0f ) );
					}
					EditorGUI.EndDisabledGroup( );
				}
				EditorGUILayout.EndHorizontal( );
			}
			EditorGUILayout.EndScrollView( );

			if ( refresh )
			{
				Refresh( );
			}

			if ( build )
			{
				Build( );
			}
		}

		private void Refresh( )
		{
			libraries.Clear( );

			var path = FileSystem.ToSystemPath( RootDirectory );
			var directories = Directory.GetDirectories( path );
			foreach ( var directory in directories )
			{
				var name = new DirectoryInfo( directory ).Name;
				var assetPath = FileSystem.FromSystemPath( directory );

				libraries.Add( name, assetPath );
			}

			Repaint( );
		}

		private void Build( )
		{
			var lastDirectory = EditorPrefs.GetString( PrefsKey( "Output" ), "Assets" );
			var directory = EditorUtility.SaveFolderPanel( "Select Output Directory", lastDirectory, string.Empty );
			if ( directory != lastDirectory )
			{
				EditorPrefs.SetString( PrefsKey( "Output" ), directory );
			}

			foreach ( var library in libraries )
			{
				if ( EditorPrefs.GetBool( PrefsKey( library.Key ) ) )
				{
					var path = Path.Combine( directory, library.Key + ".unitypackage" );
					AssetDatabase.ExportPackage( library.Value, path, ExportPackageOptions.Recurse | ExportPackageOptions.Interactive );
				}
			}
		}
	}
}
