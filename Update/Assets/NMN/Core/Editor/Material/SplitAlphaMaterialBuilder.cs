
using System.IO;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;

using NMN.Extensions;
using NMN.Editor.TextureTools;

namespace NMN.Editor.MaterialTools
{
	public static class SplitAlphaMaterialBuilder
	{
		private const string DefaultShader = "NMN/Split/Unlit/Transparent Colored";

		public static void Build( Material[] materials )
		{
			EditorUtility.DisplayProgressBar( "Splitting Alpha...", string.Empty, 0.0f );

			for ( int i = 0; i < materials.Length; ++i )
			{
				var material = materials[i];
				if ( material != null )
				{
					var progress = (float)i / (float)(materials.Length - 1);
					EditorUtility.DisplayProgressBar( "Splitting Textures", material.name, progress );

					Split( material );
				}
			}

			EditorUtility.ClearProgressBar( );
		}

		private static void Split( Material material )
		{
			var texture = material.mainTexture as Texture2D;
			if ( texture == null )
			{
				return;
			}

			if ( !material.shader.name.StartsWith( "NMN/Split/" ) )
			{
				var shader = Shader.Find( "NMN/Split/" + material.shader.name );
				if ( shader == null )
				{
					shader = Shader.Find( DefaultShader );
				}

				Editor.Tools.EditAssets( ( ) =>
				{
					material.shader = shader;

					EditorUtility.SetDirty( material );
				} );
			}

			SplitAlphaBuilder.Build( texture );

			var directory = Path.GetDirectoryName( AssetDatabase.GetAssetPath( texture ) );
			var alphaPath = directory + "/" + texture.name + "_alpha.png";
			var alpha = AssetDatabase.LoadAssetAtPath( alphaPath, typeof(Texture2D) ) as Texture2D;

			Editor.Tools.EditAssets( ( ) =>
			{
				material.SetTextureOffset( "_AlphaTex", material.mainTextureOffset );
				material.SetTextureScale( "_AlphaTex", material.mainTextureScale );
				material.SetTexture( "_AlphaTex", alpha );

				EditorUtility.SetDirty( material );
			} );
		}
	}
}
