
using UnityEngine;
using UnityEditor;
using System.Linq;

using NMN.Editor.TextureTools;
using NMN.Editor.MaterialTools;

public static partial class NMMenu
{
	public const int RootID = -1000;

#region Texture Tools

	[MenuItem("NMN/Texture/Split Alpha", true, RootID)]
	[MenuItem("Assets/NMN/Texture/Split Alpha", true, RootID)]
	public static bool SplitAlphaValidator( )
	{
		return NMN.Editor.SelectionFilter<Texture2D>.All( );
	}

	[MenuItem("NMN/Texture/Split Alpha", false, RootID)]
	[MenuItem("Assets/NMN/Texture/Split Alpha", false, RootID)]
	public static void SplitAlpha( )
	{
		SplitAlphaBuilder.Build( NMN.Editor.SelectionFilter<Texture2D>.Convert( ).ToArray( ) );
	}

#endregion

#region Material Tools

	[MenuItem("NMN/Material/Generate Split Material", true, RootID)]
	[MenuItem("Assets/NMN/Material/Generate Split Material", true, RootID)]
	public static bool GenerateSplitMaterialValidator( )
	{
		return NMN.Editor.SelectionFilter<Material>.All( );
	}

	[MenuItem("NMN/Material/Generate Split Material", false, RootID)]
	[MenuItem("Assets/NMN/Material/Generate Split Material", false, RootID)]
	public static void GenerateSplitMaterial( )
	{
		SplitAlphaMaterialBuilder.Build( NMN.Editor.SelectionFilter<Material>.Convert( ).ToArray( ) );
	}

#endregion

#region GameObject tools

	[MenuItem("NMN/Game Object/Add Child", true, RootID)]
	public static bool AddChildValidator( )
	{
		return NMN.Editor.SelectionFilter<GameObject>.All( );
	}
	
	[MenuItem("NMN/Game Object/Add Child", false, RootID)]
	public static void AddChild( )
	{
		foreach ( var gameObject in NMN.Editor.SelectionFilter<GameObject>.Convert( ) )
		{
			var prefabType = PrefabUtility.GetPrefabType( gameObject );
			if (	prefabType == PrefabType.None
			    ||	prefabType == PrefabType.PrefabInstance )
			{
				NMN.Tools.AddChild( gameObject );
			}
		}
	}

#endregion

#region Core Tools

	[MenuItem("NMN/Package Builder", false, RootID+15)]
	public static void BuildPackages( )
	{
		NMN.Editor.PackageBuilder.Create( );
	}

#endregion
}
